import React, {Component} from 'react';
import PropTypes from 'prop-types';
import { observer } from 'mobx-react';
import { observable, action, extendObservable } from 'mobx';
import Base from './base';
import _ from 'lodash';
import Dropzone from 'react-dropzone-component';
import SectionsStore from 'stores/SectionsStore';
import StepConditionData from 'classes/step-condition-data';
import WorkflowStore from 'stores/WorkflowStore';
import WorkflowItem from 'classes/workflow-item';
import Cookies from 'cookies';
import ReactDOMServer from 'react-dom/server';

@observer
export default class Condition extends Component {
    constructor(props) {
        super(props);
        this.data = new StepConditionData(props.item.data.toJS());
    }
    static propTypes = {
        item: PropTypes.object.isRequired,
        onSave: PropTypes.func
    };

    handleSave = () => {
        if(this.props.onSave) {
            let data = this.data.toJS();
            this.props.onSave(this.beforeSave(data));
        }
    };

    @action beforeSave(data) {
        // convert some values to int
        // data.wait_for_yes = !!data.wait_for_yes; //to bool
        // data.wait_for_value = data.wait_for_value ? parseInt(data.wait_for_value) : 0;
        // data.answered_to = data.answered_to ? parseInt(data.answered_to) : 0;
        return data;
    }

    @action handleTypeChange = (e) => {
        let newData = {};
        newData[e.target.name] = e.target.value;
        this.data.setData(newData);
    };

    @action handleChange = (e) => {
        let newData = {};
        let name = e.target.name;
        let multiple = false;
        let value = null;

        if (e.target.name.substr(-2) === "[]") {
            name = e.target.name.substr(-2);
            multiple = true;
        }
        if (e.target.type === "checkbox" || e.target.type === "radio") {
            if (e.target.checked) value = e.target.value;
        } else if (e.target.type === "select-multiple" || e.target.type === "select" && e.target.multiple === true) {
            value = this.getSelectedOptions(e.target);
        } else {
            value = e.target.value;
        }
        if (multiple) {
            if (newData[name] && _.isArray(newData[name]))
                newData[name].push(value);
            else
                newData[name] = [ value ];
        } else {
            newData[name] = value;
        }

        this.data.setData(newData);
    };
    getSelectedOptions(select) {
        if (typeof select.selectedOptions !== "undefined") {
            return _.map(e.target.selectedOptions, option => option.value);
        } else {
            let result = [];
            let options = select && select.options;
            let opt, i, iLen;

            for (i = 0, iLen = options.length; i < iLen; i++) {
                opt = options[i];

                if (opt.selected) {
                    result.push(opt.value || opt.text);
                }
            }
            return result;
        }
    }

    @action componentWillReceiveProps(nextProps) {
        if (nextProps.item.id !== this.props.item.id) {
            // extendObservable(this.data, nextProps.item.data.toJS());
            // this.data.setData(nextProps.item.data.toJS());
            this.data = new StepConditionData(nextProps.item.data.toJS());
        }
    };

    getMessagesSteps(firstMessage) {
        let queue = [firstMessage],
            cursor = 0,
            result = [],
            message;

        while(cursor < queue.length) {
            message = queue[cursor];
            if (message.type === WorkflowItem.TYPE_ACTION &&
                message.data.get('type') === 'send_message') {
                result.push(message);
            }
            queue.push(...message.next);
            cursor++;
        }

        return result;
    }
    getTypeContent(data) {
        let tags = [];
        if (!data.type) return null;

        if (data.type === 'user_field') {
            let user_fields = WorkflowStore.getUserFields();
            this.setUserFieldsTags(tags, data, user_fields);
        } else if (data.type === 'task_answer') {
            this.setTaskAnswerTags(tags, data);
        } else {
            return <div key="condition" className="form-group next">Неизвестный тип условия {data.type}</div>
        }
        tags.push(
            <div key="condition" className="form-group next">
                <div className="select-wrap">
                    <select className="form-control" name="condition" value={data.condition} onChange={this.handleChange}>
                        <option value="">-- Выберите условие --</option>
                        <option value="eq">=</option>
                        <option value="lt">&lt;</option>
                        <option value="gt">&gt;</option>
                        <option value="lt_eq">&lt;=</option>
                        <option value="gt_eq">&gt;=</option>
                        <option value="not_empty">Не пусто</option>
                        <option value="empty">Пусто</option>
                        <optgroup label="Для строк">
                            <option value="starts_with">Начинается с</option>
                            <option value="ends_with">Заканчивается на</option>
                            <option value="contains">Содержит</option>
                        </optgroup>
                    </select>
                </div>
            </div>
        );

        if (data.condition !== 'not_empty' && data.condition !== 'empty')
            tags.push(
                <div key="condition_value" className="form-group next">
                    <input type="text" className="form-control" name="condition_value" value={data.condition_value} onChange={this.handleChange} />
                </div>
            );
        return tags;
    }
    setTaskAnswerTags(tags, data) {
        let section_selected,
            assignmnet_selected;

        tags.push(<div key="cond_sections" className="form-group next">
            <div className="display-flex">
                <div className="select-wrap number-width">
                    <select className="form-control" name="section_id" value={data.section_id || ''} onChange={this.handleChange}>
                        <option value=""/>
                        {SectionsStore.items.map((item) => {
                            if (item.id === data.section_id)
                                section_selected = item;
                            return <option key={item.id} value={item.id}>{item.name}</option>
                        })}
                    </select>
                </div>
            </div>
        </div>);

        if (section_selected) {
            tags.push(<div key="cond_assignmnets" className="form-group next">
                <div className="display-flex">
                    <div className="select-wrap number-width">
                        <select className="form-control" name="assignment_id" value={data.assignment_id || ''} onChange={this.handleChange}>
                            <option value=""/>
                            {section_selected.assignments.map((item) => {
                                if (item.id == data.assignment_id)
                                    assignmnet_selected = item;
                                return <option key={item.id} value={item.id}>{item.name}</option>
                            })}
                        </select>
                    </div>
                </div>
            </div>);
        }
        if (assignmnet_selected) {
            tags.push(<div key="cond_tasks" className="form-group next">
                <div className="display-flex">
                    <div className="select-wrap number-width">
                        <select className="form-control" name="task_id" value={data.task_id || ''} onChange={this.handleChange}>
                            <option value=""/>
                            {assignmnet_selected.tasks.map((item) => {
                                let name = item.name ? item.name : 'id: ' + item.id;
                                return <option key={item.id} value={item.id}>{name}</option>
                            })}
                        </select>
                    </div>
                </div>
            </div>);
        }
    }
    setUserFieldsTags(tags, data, user_fields) {
        let user_field_options = _.map(
            user_fields,
            (field) => <option key={field.field_name} value={field.field_name}>{field.name}</option>
        );

        tags.push(
            <div key="user_field" className="form-group next">
                <div className="select-wrap">
                    <select className="form-control" name="field_name" value={data.field_name}
                            onChange={this.handleChange}>
                        <option value="">-- Выберите поле --</option>
                        {user_field_options}
                    </select>
                </div>
            </div>
        );
    }
    handleDeleteFile = (e) => {
        e.preventDefault();
        this.props.item.deleteFile()
            .then(this.updateItemData)
            .catch(function(e) {
                console.error(e);
            });
    };
    updateItemData = () => {
        this.data.setData(this.props.item.data.toJS());
    };

    render() {
        const {onSave, ...otherProps} = this.props;
        const data = this.data;

        let emptySelect = !data.type ? <option value="">Выбери условие</option> : null;
        return <Base title="Условие" onSave={this.handleSave} {...otherProps}>
            <div className="row wf-settings delay">
                <div className="col-sm-12">
                    <div className="form-group">
                        <div className="select-wrap number-width">
                            <select className="form-control" name="type" value={data.type} onChange={this.handleTypeChange}>
                                {emptySelect}
                                <option value="user_field">Пользовательское поле</option>
                                <option value="task_answer">Ответ на вопрос</option>
                            </select>
                        </div>
                    </div>

                    {this.getTypeContent(data)}
                </div>
            </div>
        </Base>
    }
}