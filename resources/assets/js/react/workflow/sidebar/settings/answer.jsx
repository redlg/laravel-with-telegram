import React, {Component} from 'react';
import PropTypes from 'prop-types';
import Base from './base';
import { toJS, observable, action } from 'mobx';
import { observer } from 'mobx-react';
import SectionsStore from '../../../stores/SectionsStore';

@observer
export default class Answer extends Component {
    constructor(props) {
        super(props);

        this.data = Object.assign(observable({
            section_id: 0,
            assignment_id: 0,
            task_id: 0,
        }), toJS(props.item.data));
    }
    static propTypes = {
        item: PropTypes.object.isRequired,
        onSave: PropTypes.func
    };

    handleSave = () => {
        if(this.props.onSave) {
            this.props.onSave(toJS(this.data));
        }
    };

    handleChange = action((e) => {
        this.data[e.target.name] = parseInt(e.target.value);
    });

    @action
    componentWillReceiveProps(nextProps) {
        if (nextProps.item.id !== this.props.item.id) {
            this.data.section_id = nextProps.item.data.get('section_id');
            this.data.assignment_id = nextProps.item.data.get('assignment_id');
            this.data.task_id = nextProps.item.data.get('task_id');
        }
    };

    render() {
        const {onSave, ...otherProps} = this.props;
        let sections, section_selected,
            assignmnets, assignmnet_selected,
            tasks;
        sections = <div className="form-group">
            <div className="display-flex">
                <div className="select-wrap number-width">
                    <select className="form-control" name="section_id" value={this.data.section_id || ''} onChange={this.handleChange}>
                        <option value=""/>
                        {SectionsStore.items.map((item) => {
                            if (item.id === this.data.section_id)
                                section_selected = item;
                            return <option key={item.id} value={item.id}>{item.name}</option>
                        })}
                    </select>
                </div>
            </div>
        </div>;

        if (section_selected) {
            assignmnets = <div className="form-group">
                <div className="display-flex">
                    <div className="select-wrap number-width">
                        <select className="form-control" name="assignment_id" value={this.data.assignment_id || ''} onChange={this.handleChange}>
                            <option value=""/>
                            {section_selected.assignments.map((item) => {
                                if (item.id == this.data.assignment_id)
                                    assignmnet_selected = item;
                                return <option key={item.id} value={item.id}>{item.name}</option>
                            })}
                        </select>
                    </div>
                </div>
            </div>;
        }
        if (assignmnet_selected) {
            tasks = <div className="form-group">
                <div className="display-flex">
                    <div className="select-wrap number-width">
                        <select className="form-control" name="task_id" value={this.data.task_id || ''} onChange={this.handleChange}>
                            <option value=""/>
                            {assignmnet_selected.tasks.map((item) => {
                                let name = item.name ? item.name : 'id: ' + item.id;
                                return <option key={item.id} value={item.id}>{name}</option>
                            })}
                        </select>
                    </div>
                </div>
            </div>;
        }

        return <Base title="Ответ от пользователя" onSave={this.handleSave} {...otherProps}>
            <div className="row wf-settings delay">
                <div className="col-sm-12">
                    {sections}
                    { assignmnets }
                    { tasks }
                </div>
            </div>
        </Base>
    }
}