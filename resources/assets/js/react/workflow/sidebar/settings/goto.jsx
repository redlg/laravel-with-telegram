import React, {Component} from 'react';
import PropTypes from 'prop-types';
import Base from './base';
import { toJS, observable, action } from 'mobx';
import { observer } from 'mobx-react';
import WorkflowStore from '../../../stores/WorkflowStore';

@observer
export default class Goto extends Component {
    constructor(props) {
        super(props);

        this.data = Object.assign(observable({
            target: '',
        }), toJS(props.item.data));
    }
    static propTypes = {
        item: PropTypes.object.isRequired,
        onSave: PropTypes.func
    };

    handleSave = () => {
        if(this.props.onSave) {
            this.props.onSave(toJS(this.data));
        }
    };

    handleChange = action((e) => {
        this.data[e.target.name] = e.target.value;
    });

    @action
    componentWillReceiveProps(nextProps) {
        if (nextProps.item.id !== this.props.item.id) {
            this.data.target = nextProps.item.data.get('target');
        }
    };

    render() {
        const {onSave, ...otherProps} = this.props;
        let workflows = WorkflowStore.items.map((item) => <option key={item.id} value={item.id}>{item.name}</option>);

        return <Base title="Переход к сценарию" onSave={this.handleSave} {...otherProps}>
            <div className="row wf-settings delay">
                <div className="col-sm-12">
                    <div className="form-group">
                        <div className="display-flex">
                            <div className="select-wrap number-width">
                                <select className="form-control" name="target" value={this.data.target || ''} onChange={this.handleChange}>
                                    <option value=""/>
                                    {workflows}
                                </select>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </Base>
    }
}