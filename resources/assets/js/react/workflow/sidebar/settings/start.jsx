import React, {Component} from 'react';
import PropTypes from 'prop-types';
import Base from './base';

export default class Start extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: Object.assign(
                { delay_unit: 'days' },
                props.item.data.toJS()
            )
        };
    }
    static propTypes = {
        item: PropTypes.object.isRequired,
        onSave: PropTypes.func
    };

    handleSave = () => {
        if(this.props.onSave) {
            this.props.onSave(this.state.data);
        }
    };

    handleChange = (e) => {
        let newData = {};
        newData[e.target.name] = e.target.value;
        this.setState({
            data: Object.assign({}, this.state.data, newData)
        });
    };

    componentWillReceiveProps(nextProps) {
        if (nextProps.item.id !== this.props.item.id) {
            this.setState({
                data: nextProps.item.data.toJS()
            });
        }
    };

    render() {
        const {onSave, ...otherProps} = this.props;
        const selectedUnit = this.state.data.delay_unit || 'days';
        const value = this.state.data.value || '';

        return <Base title="Событие" onSave={this.handleSave} {...otherProps}>
            <div className="row wf-settings delay">
                <div className="col-sm-12">
                    <div className="form-group">
                        Пользователь добавил бота
                    </div>
                </div>
            </div>
        </Base>
    }
}