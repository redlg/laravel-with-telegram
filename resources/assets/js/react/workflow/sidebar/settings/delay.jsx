import React, {Component} from 'react';
import PropTypes from 'prop-types';
import Base from './base';

export default class Delay extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: Object.assign(
                { delay_unit: 'days' },
                props.item.data.toJS()
            )
        };
    }
    static propTypes = {
        item: PropTypes.object.isRequired,
        onSave: PropTypes.func
    };

    handleSave = () => {
        if(this.props.onSave) {
            this.props.onSave(this.state.data);
        }
    };

    handleChange = (e) => {
        let newData = {};
        newData[e.target.name] = e.target.value;
        this.setState({
            data: Object.assign({}, this.state.data, newData)
        });
    };

    componentWillReceiveProps(nextProps) {
        if (nextProps.item.id !== this.props.item.id) {
            this.setState({
                data: nextProps.item.data.toJS()
            });
        }
    };

    render() {
        const {onSave, ...otherProps} = this.props;
        const selectedUnit = this.state.data.delay_unit || 'days';
        const value = this.state.data.value || '';

        return <Base title="Задержка" onSave={this.handleSave} {...otherProps}>
            <div className="row wf-settings delay">
                <div className="col-sm-12">
                    <div className="form-group">
                        <div className="display-flex">
                            <input className="form-control number pull-left mr10" type="text" name="value" value={value} onChange={this.handleChange}/>
                            <div className="select-wrap number-width">
                                <select className="form-control" name="delay_unit" value={selectedUnit} onChange={this.handleChange}>
                                    <option value="minutes">минут</option>
                                    <option value="hours">часов</option>
                                    <option value="days">дней</option>
                                </select>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </Base>
    }
}