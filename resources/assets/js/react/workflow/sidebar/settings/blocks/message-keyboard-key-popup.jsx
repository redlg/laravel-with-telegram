import React, { Component } from 'react';
import Popup from 'react-popup'
import { toJS, action, observable } from 'mobx';
import { observer } from 'mobx-react';
import _ from 'lodash';
import IconClose from 'react-icons/lib/fa/close';

@observer
export default class MessageKeyboardKeyPopup extends Component {
    constructor(props) {
        super(props);
        this.data = Object.assign(observable({
            id: 0,
            text: '',
            action: '',
            action_target: '',
        }), props.item);
    }

    componentWillReceiveProps(nextProps) {
        this.data = observable(nextProps.item);
    }
    @action
    onChange = (e) => {
        this.props.onChange(e.target.name, e.target.value);
        this.data[e.target.name] = e.target.value;
    };
    render() {
        return <div className="container-fluid">
            <div className="row">
                <div className="col-xs-12">
                    <label htmlFor="popup-message-key-text" className="control-label">Текст кнопки</label>
                    <input type="text" className="form-control" name="text" value={this.data.text} onChange={this.onChange} />
                </div>
                <div className="col-xs-12">
                    <label htmlFor="popup-message-key-text" className="control-label">Действие</label>
                    <div className="checkbox">
                        <label>
                            <input type="radio" name="action" value="continue" checked={this.data.action == 'continue'} onChange={this.onChange} /> Следующий шаг
                        </label>
                    </div>
                    <div className="checkbox">
                        <label>
                            <input type="radio" name="action" value="delay" checked={this.data.action == 'delay'} onChange={this.onChange} /> Отложить
                        </label>
                    </div>
                </div>
            </div>
        </div>;
    }
}
