import React, { Component } from 'react';
import Popup from 'react-popup'
import { toJS, action, observable } from 'mobx';
import { observer } from 'mobx-react';
import _ from 'lodash';
import IconClose from 'react-icons/lib/fa/close';
import MessageKeyboardKeyPopup from './message-keyboard-key-popup';

@observer
export default class MessageKeyboard extends Component {
    @action
    handleAddClick = () => {
        let max = 0;
        this.props.data.keyboard_keys.forEach(function(key){
            if (key.id > max) max = key.id;
        });
        this.props.data.keyboard_keys.push({
            id: max + 1,
            text: 'кнопка',
            action: 'continue',
        });
    };

    @action
    handleDelete = (key) => {
        this.props.data.keyboard_keys.remove(key);
    };

    render() {
        let { data } = this.props;
        return <div key="message_keyboard_group" className="form-group next highlight">
            <div className="row">
                <div key="message_keyboard_show" className="col-xs-12">
                    <label>
                        <input type="checkbox" name="keyboard_show" value="1" checked={data.keyboard_show == "1"} onChange={this.props.onChange || _.noop}/>
                        показать клавиатуру
                    </label>
                </div>
                { data.keyboard_show == "1" && data.keyboard_keys &&
                <div key="message_keyboard_keys" className="col-xs-12">
                    <div className="select-wrap">
                        {data.keyboard_keys.map((key) => <MessageKeyboardKey key={key.id} item={key} onDelete={this.handleDelete} />)}
                    </div>
                </div> }
                { data.keyboard_show == "1" && <button type="button" className="btn" onClick={this.handleAddClick}>Добавить</button>}
            </div>
        </div>;
    }
}

@observer
export class MessageKeyboardKey extends Component {
    @observable data = {
        text: '',
        url: '',
    };
    constructor(props) {
        super(props);
    }
    handleClick = () => {
        let keyData = toJS(this.props.item);
        let onChange = (name, value) => {
            keyData[name] = value;
        };
        Popup.create({
            closeOnOutsideClick: true,
            content: <MessageKeyboardKeyPopup onChange={onChange} item={this.props.item}/>,
            buttons: {
                left: [
                    {
                        text: 'Сохранить',
                        action: action(() => {
                            this.props.item.text = keyData.text;
                            this.props.item.action = keyData.action;
                            this.props.item.action_target = keyData.action_target;
                            Popup.close();
                        })
                    }
                ],
                right: [{
                    text: 'Удалить',
                    action: () => {
                        this.handleDelete();
                        Popup.close();
                    }
                }]
            }
        });
    };

    handleDelete = (e) => {
        e && (e.preventDefault() || e.stopPropagation());
        this.props.onDelete && this.props.onDelete(this.props.item);
    };

    render() {
        const {item} = this.props;

        return <span className="message-keyboard--key" onClick={this.handleClick}>
            <a href="#" onClick={this.handleDelete} className="message-keyboard--key-delete"><IconClose /></a>
            {item.text}
        </span>
    }
}