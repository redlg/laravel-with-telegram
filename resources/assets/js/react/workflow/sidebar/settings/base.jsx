import React, {Component} from 'react';
import PropTypes from 'prop-types';
import Button from '../../../button';
import WorkflowStore from '../../../stores/WorkflowStore';

export default class Base extends Component {
    static propTypes = {
        title: PropTypes.string,
        onSave: PropTypes.func,
        onCancel: PropTypes.func,
        item: PropTypes.object.isRequired
    };
    static defaultProps = {
        title: 'Заголовок'
    };
    handleFormSave = (e) => {
        e.preventDefault();
        if (this.props.onSave) {
            this.props.onSave(e);
        }
        WorkflowStore.deselectStep();
    };
    handleCancel = (e)=> {
        e.preventDefault();
        if (this.props.onCancel) {
            this.props.onCancel(e);
        }
        WorkflowStore.deselectStep();
    };
    render() {
        return <div>
            <div className="sidebar-content-top">
                <h3>{this.props.title}</h3>
            </div>
            <form onSubmit={this.handleFormSave} method="post" encType="multipart/form-data">
                <div className="sidebar-content">
                    {this.props.children}
                </div>
                <div className="sidebar-content-bottom">
                    <Button onClick={this.handleFormSave}>Сохранить</Button>
                    <a href="#" className="cancel" onClick={this.handleCancel}>Отмена</a>
                </div>
            </form>
        </div>;
    }
}