import React, {Component} from 'react';
import PropTypes from 'prop-types';
import { observer } from 'mobx-react';
import { toJS, observable, action, extendObservable } from 'mobx';
import Base from './base';
import _ from 'lodash';
import Dropzone from 'react-dropzone-component';
import SectionsStore from 'stores/SectionsStore';
import AssignmentsStore from 'stores/AssignmentsStore';
import StepActionData from 'classes/step-action-data';
import WorkflowItem from 'classes/workflow-item';
import MessageKeyboard from './blocks/message-keyboard';
import Cookies from 'cookies';
import ReactDOMServer from 'react-dom/server';

@observer
export default class Action extends Component {
    constructor(props) {
        super(props);
        this.data = new StepActionData(toJS(props.item.data));
        console.log(this.data);
    }
    static propTypes = {
        item: PropTypes.object.isRequired,
        onSave: PropTypes.func
    };

    handleSave = () => {
        if(this.props.onSave) {
            let data = this.data.toJS();
            this.props.onSave(this.beforeSave(data));
        }
    };

    @action beforeSave(data) {
        // convert some values to int
        data.save_answers = !!data.save_answers; //to bool
        data.keyboard_show = !!data.keyboard_show; //to bool
        data.save_answer_to = data.save_answer_to ? parseInt(data.save_answer_to) : 0;
        return data;
    }

    @action handleTypeChange = (e) => {
        let newData = {};
        newData[e.target.name] = e.target.value;
        this.data.setData(newData);
    };

    @action handleChange = (e) => {
        let newData = {};
        let name = e.target.name;
        let multiple = false;
        let value = null;

        if (e.target.name.substr(-2) === "[]") {
            name = e.target.name.substr(-2);
            multiple = true;
        }
        if (name === "message_type") {
            newData.file_id = null;
            newData.file_url = null;
        }
        if (e.target.type === "checkbox" || e.target.type === "radio") {
            if (e.target.checked) value = e.target.value;
        } else if (e.target.type === "select" && e.target.multiple === true) {
            value = this.getSelectedOptions(e.target);
        } else {
            value = e.target.value;
        }
        if (multiple) {
            if (newData[name] && _.isArray(newData[name]))
                newData[name].push(value);
            else
                newData[name] = [ value ];
        } else {
            newData[name] = value;
        }

        this.data.setData(newData);
    };
    getSelectedOptions(select) {
        if (typeof select.selectedOptions !== "undefined") {
            return _.map(e.target.selectedOptions, option => option.value);
        } else {
            let result = [];
            let options = select && select.options;
            let opt, i, iLen;

            for (i = 0, iLen = options.length; i < iLen; i++) {
                opt = options[i];

                if (opt.selected) {
                    result.push(opt.value || opt.text);
                }
            }
            return result;
        }
    }

    @action componentWillReceiveProps(nextProps) {
        if (nextProps.item.id !== this.props.item.id) {
            // extendObservable(this.data, nextProps.item.data.toJS());
            // this.data.setData(nextProps.item.data.toJS());
            this.data = new StepActionData(nextProps.item.data.toJS());
        }
    };

    getTypeContent(data) {
        if (data.type === "send_message") {
            let tags = [];
            tags.push(
                <div key="message_type" className="form-group next">
                    <label htmlFor="message_type" className="control-label">Что отправить?</label>
                    <div className="select-wrap">
                        <select id="message_type" className="form-control" name="message_type" value={data.message_type} onChange={this.handleChange}>
                            {!data.message_type ? <option value="">Выбери тип сообщения</option> : null}
                            <option value="assignment_field">Поле упражнения</option>
                            <option value="text">Текст</option>
                            <option value="image">Картинку</option>
                            <option value="video">Видео</option>
                            <option value="sticker">Стикер</option>
                        </select>
                    </div>
                </div>,
                this.getSendMessageType(data),
                <MessageKeyboard key="keyboard" data={data} onChange={this.handleChange} />
            );

            return tags;
        } else if (data.type === "update_custom_field") {
            return <div key="message_value" className="form-group next">Не реализовано (update_custom_field)</div>;
        } else if (data.type === "send_notification") {
            return <div key="message_value" className="form-group next">Не реализовано (send_notification)</div>;
        }
        return null;
    }
    handleDeleteFile = (e) => {
        e.preventDefault();
        this.props.item.deleteFile()
            .then(this.updateItemData)
            .catch(function(e) {
                console.error(e);
            });
    };
    updateItemData = () => {
        this.data.setData(this.props.item.data.toJS());
    };
    getSendMessageType = (data) => {
        let sendMessageType;
        if (data.message_type === "assignment_field") {
            let sections, selected_section, assignments, selected_assignment, fields;

            sections = SectionsStore.items.map(function(section) {
                if (section.id == data.section_id) {
                    selected_section = section;
                }
                return <option key={'section_' + section.id} value={section.id}>{section.name}</option>;
            });
            if (selected_section) {
                assignments = selected_section.assignments.map(function(assignment) {
                    if (assignment.id == data.assignment_id) {
                        selected_assignment = assignment;
                    }
                    return <option key={'assignment_' + assignment.id} value={assignment.id}>{assignment.name}</option>;
                });
            }
            if(selected_assignment) {
                fields = _.map(AssignmentsStore.fields, (field) => <option key={field.code} value={field.code}>{field.label}</option>);
            }
            sendMessageType = <div>
                <label htmlFor="section_id" className="control-label">Раздел</label>
                <div className="select-wrap">
                    <select className="form-control" id="section_id" name="section_id" value={data.section_id} onChange={this.handleTypeChange}>
                        <option value="">-- Из какого раздела --</option>
                        {sections}
                    </select>
                </div>

                {selected_section && <label htmlFor="assignment_id" className="control-label">Упражнение</label> }
                {selected_section && <div className="select-wrap">
                        <select className="form-control" id="assignment_id" name="assignment_id" value={data.assignment_id} onChange={this.handleTypeChange}>
                            <option value="">-- Из какого упражнения --</option>
                            {assignments}
                        </select>
                </div>}

                {selected_assignment && <label htmlFor="assignment_field" className="control-label">Поле</label> }
                {selected_assignment && <div className="select-wrap">
                    <select className="form-control" id="assignment_field" name="assignment_field" value={data.assignment_field} onChange={this.handleTypeChange}>
                        <option value="">-- Из какого поля --</option>
                        {fields}
                    </select>
                </div>}
            </div>;
        } else if (data.message_type === "text") {
            sendMessageType = <textarea name="value" value={data.value} onChange={this.handleChange} className="form-control message-text" />;
        } else if (data.message_type === "image" || data.message_type === "video") {
            let iconFiletypes = [];
            if (data.message_type === "image") {
                iconFiletypes = ['.jpg', '.png', '.gif'];
            } else {
                iconFiletypes = ['.mp4'];
            }
            const componentConfig = {
                    iconFiletypes: iconFiletypes,
                    showFiletypeIcon: true,
                    postUrl: this.props.item.route('steps.file.upload'),
                },
                djsConfig = {
                    maxFiles: 1,
                    headers: {
                        'X-XSRF-TOKEN': Cookies.get('XSRF-TOKEN')
                    },
                    previewTemplate: ReactDOMServer.renderToStaticMarkup(
                        <div className="dz-preview dz-file-preview">
                            <div className="dz-details">
                                <div className="dz-filename"><span data-dz-name="true"></span></div>
                                <img data-dz-thumbnail="true" />
                            </div>
                            <div className="dz-progress"><span className="dz-upload" data-dz-uploadprogress="true"></span></div>
                            <div className="dz-error-message"><span data-dz-errormessage="true"></span></div>
                        </div>
                    )
                },
                eventHandlers = {
                    success: (file, db_file) => {
                        this.data.setData({
                            'file_id': db_file.id,
                            'file_url': db_file.url
                        });
                        this.handleSave();
                    }
                };

            if (data.file_id) {
                sendMessageType = <div>
                    <div className={"message-" + data.message_type}>
                    { data.message_type === "image" ?
                        <img src={data.file_url} alt=""/> :
                        <video controls={true}>
                            <source src={data.file_url} />
                        </video> }
                    </div>
                    <textarea name="value" value={data.value || ""} onChange={this.handleChange} className="form-control message-text" />
                    <a href="#" onClick={this.handleDeleteFile} className="delete">удалить</a>
                </div>;
            } else {
                sendMessageType = <div>
                    <Dropzone config={componentConfig}
                              djsConfig={djsConfig}
                              eventHandlers={eventHandlers}/>
                    <textarea name="value" value={data.value || ""} onChange={this.handleChange} className="form-control message-text" />
                </div>;
            }
            // sendMessageType = <div>Не реализовано (image)</div>;
        } else if (data.message_type === "sticker") {
            sendMessageType = <div>Пока еще не реализовано (sticker)</div>;
        }
        if (sendMessageType) {
            return <div key="message_value" className={"form-group message-type message-type--" + data.message_type + " next"}>
                {sendMessageType}
            </div>
        } else {
            return null;
        }
    };

    render() {
        const {onSave, ...otherProps} = this.props;
        const data = this.data;

        let emptySelect = !data.type ? <option value="">Выбери тип действия</option> : null;
        return <Base title="Действие" onSave={this.handleSave} {...otherProps}>
            <div className="row wf-settings delay">
                <div className="col-sm-12">
                    <div className="form-group">
                        <div className="select-wrap number-width">
                            <select className="form-control" name="type" value={data.type} onChange={this.handleTypeChange}>
                                {emptySelect}
                                <option value="send_message">Отправить сообщение пользователю</option>
                                <option value="update_custom_field">Обновить пользовательское поле</option>
                                <option value="send_notification">Отправить оповещение</option>
                            </select>
                        </div>
                    </div>
                    {this.getTypeContent(data)}
                </div>
            </div>
        </Base>
    }
}