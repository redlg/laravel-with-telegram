import React, { Component } from 'react';
import WorkflowStore from 'stores/WorkflowStore';
import { observer } from 'mobx-react';
import { observable, action, toJS } from 'mobx';

@observer
export default class extends Component {
    @observable selectItem = '';
    @action handleMouseOver = (e) => {
        //(e) => {this.selectItem = sub_item.id}
        let id = e.target.getAttribute('data-id');
        if (!id)
            id = e.target.parentNode.getAttribute('data-id');
        this.selectItem = id;
    };

    renderTree() {
        let index = 0;
        return WorkflowStore.steps.map((item) => {
            let js = toJS(item);
            if (js.next) {
                js.next = js.next.map(function(item) {
                    return item.id;
                });
            }
            let style = {
                border: '1px solid grey',
                margin: '2px'
            };
            if (this.selectItem == item.id) {
                style['background'] = 'rgba(0,0,0,0.4)';
            }
            let children = item.next.map((sub_item) => {
                return <div key={sub_item.id}
                            data-id={sub_item.id}
                            onMouseLeave={this.handleMouseLeave}
                            onMouseEnter={this.handleMouseOver}><i>{sub_item.id}</i></div>
            });
            return <div key={item.id} style={style}>
                <div><b>{item.id}</b></div>
                data: <pre style={{textAlign: 'left'}}>{JSON.stringify(item.data, null, '\t')}</pre> <br/>
                childs:
                {children}
            </div>
        });
    }
    render() {
        return <div>{this.renderTree()}</div>;
    }
}