import React, { Component } from 'react';
import { observer } from 'mobx-react';
import WorkflowStore from 'stores/WorkflowStore';
import WorkflowItem from 'classes/workflow-item';
import { observable, action, toJS } from 'mobx';
import StepBaseSettings from './sidebar/settings/base';
import StepDelaySettings from './sidebar/settings/delay';
import StepActionSettings from './sidebar/settings/action';
import StepGotoSettings from './sidebar/settings/goto';
import StepStartSettings from './sidebar/settings/start';
import StepAnswerSettings from './sidebar/settings/answer';
import StepConditionSettings from './sidebar/settings/condition';

@observer
export default class WorkflowSidebar extends Component {
    handleStepSave = (data) => {
        WorkflowStore.selectedStep.setData(data);
        WorkflowStore.selectedStep.save();
    };
    render() {
        return <div className="lv-workflow-builder-sidebar">
            {this.getSettingsComponent()}
        </div>
    }

    getSettingsComponent() {
        if (!WorkflowStore.selectedStep) {
            return null;
        }
        const type = WorkflowStore.selectedStep.type;
        switch(type) {
            case WorkflowItem.TYPE_DELAY:
                return <StepDelaySettings item={WorkflowStore.selectedStep} onSave={this.handleStepSave} />;
            case WorkflowItem.TYPE_ACTION:
                return <StepActionSettings item={WorkflowStore.selectedStep} onSave={this.handleStepSave} />;
            case WorkflowItem.TYPE_START:
                return <StepStartSettings item={WorkflowStore.selectedStep} onSave={this.handleStepSave} />;
            case WorkflowItem.TYPE_CONDITION:
                return <StepConditionSettings item={WorkflowStore.selectedStep} onSave={this.handleStepSave} />;
            case WorkflowItem.TYPE_GOTO:
                return <StepGotoSettings item={WorkflowStore.selectedStep} onSave={this.handleStepSave} />;
            case WorkflowItem.TYPE_ANSWER:
                return <StepAnswerSettings item={WorkflowStore.selectedStep} onSave={this.handleStepSave} />;
            default:
                return <StepBaseSettings item={WorkflowStore.selectedStep} onSave={this.handleStepSave}>
                    not implemented type: {type}
                    </StepBaseSettings>
        }
    }
}