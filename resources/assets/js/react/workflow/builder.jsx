import React, { Component } from 'react';
import WorkflowTemplate from "./template";
import WorkflowSidebar from "./sidebar";
import WorkflowTree from 'workflow/sidebar/WorkflowTree'

export default class WorkflowBuilder extends Component {
    render() {
        return <div className="lv-workflow-builder">
            <div className="lv-workflow-builder-template">
                <div className="lv-workflow-builder-template__content">
                    <div className="lv-workflow-builder-tempalte__area">
                        <WorkflowTemplate />
                        <div style={{clear: 'both'}}></div>
                        <WorkflowTree />
                    </div>
                </div>
            </div>
            <WorkflowSidebar />
        </div>;
    }
}