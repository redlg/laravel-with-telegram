import React, {Component} from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import {observer} from 'mobx-react';
import {observable, action} from 'mobx';
import IconPlus from "react-icons/lib/fa/plus";
import Button from "button";

@observer
export default class AddStepButton extends Component {
    static propTypes = {
        onSelect: PropTypes.func.isRequired
    };
    @observable showOverLink = false;
    @observable isTypeSelectorOpened = false;

    @action toggleSelector = (state=null) => {
        if (state === null)
            state = !this.isTypeSelectorOpened;
        this.isTypeSelectorOpened = state;
        if (this.isTypeSelectorOpened) {
            // setTimeout(() => {
            //     console.log($(this.refs.link).offset().top, $('.lv-workflow-builder-tempalte__area').height());
                if ($('.lv-workflow-builder-tempalte__area').height() - $(this.refs.link).offset().top < 150) {
                    this.showOverLink = true;
                } else {
                    this.showOverLink = false;
                }
            // }, 0);
            this.registerMouseListener();
        } else {
            this.unregisterMouseListener();
        }
    };
    handleAdd = (e, type) => {
        e.preventDefault();
        this.toggleSelector(false);
        this.props.onSelect(type);
    };
    registerMouseListener = () => {
        $(window.document).on('mousedown.addStepButton', e => {
            if (!$(this.refs['add-button']).find(e.target).length) {
                this.toggleSelector();
            }
        });
    };
    unregisterMouseListener = () => {
        $(window.document).off('mousedown.addStepButton');
    };

    componentWillUnmount() {
        if (this.isTypeSelectorOpened) {
            $(window.document).off('mousedown.addStepButton');
        }
    }

    _handleButtonClick = () => {
        this.toggleSelector();
    };
    _handleDelayClick = (e) => {
        this.handleAdd(e, 'delay');
    };
    _handleConditionClick = (e) => {
        this.handleAdd(e, 'condition');
    };
    _handleActionClick = (e) => {
        this.handleAdd(e, 'action');
    };
    _handleTriggerClick = (e) => {
        this.handleAdd(e, 'trigger');
    };
    _handleAnswerClick = (e) => {
        this.handleAdd(e, 'answer');
    };
    _handleGotoClick = (e) => {
        this.handleAdd(e, 'goto');
    };
    render() {
        const popoverClasses = classnames({
            'add-step__popover': true,
            'add-step__popover--active': this.isTypeSelectorOpened,
            'add-step__popover--over-link': this.showOverLink
        });
        const className = 'add-step ' + (this.props.className || '');

        return <div className={className} ref="add-button">
            <a href="#" ref="link"
               onClick={this._handleButtonClick}
               style={{cursor: 'pointer'}}>
                <IconPlus />
            </a>
            <div className={popoverClasses}>
                <p>Добавить шаг</p>
                <div className="add-step__btns">
                    <Button onClick={this._handleActionClick}>Действие</Button>
                    &nbsp;
                    <Button onClick={this._handleConditionClick}>Условие</Button>
                    &nbsp;
                    <Button onClick={this._handleAnswerClick}>Ответ пользователя</Button>
                    &nbsp;
                    <Button onClick={this._handleGotoClick}>Переход к сценарию</Button>
                    &nbsp;
                    <Button onClick={this._handleDelayClick}>Задержка</Button>
                </div>
            </div>
        </div>;
    }
}

