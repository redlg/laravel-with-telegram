import React, { Component } from 'react'
import PropTypes from 'prop-types'
import {observer} from 'mobx-react'
import classnames from 'classnames'
import WorkflowStore from 'stores/WorkflowStore'
import WorkflowItem from 'classes/workflow-item'
import Popup from 'react-popup'

import StepBase from "./step-types/base";
import StepAction from "./step-types/action";
import StepCondition from "./step-types/condition";
import StepDelay from "./step-types/delay";
import StepAnswer from "./step-types/answer";
import StepGoto from "./step-types/goto";

@observer
export default class WorkflowStep extends Component {
    static propTypes = {
        firstStep: PropTypes.bool
    };
    static defaultProps = {
        firstStep: false
    };
    removeItem = (e) => {
        e.preventDefault();
        let {item} = this.props;
        let promise = Promise.resolve();
        if (item.type === WorkflowItem.TYPE_CONDITION) {
            if (item.data.get('next_yes') && item.data.get('next_no')) {
                promise = promise.then(this.chooseWhichSideDelete);
            }
        }
        promise
            .then((side) => {
                if (item.id === WorkflowStore.selectedStep.id)
                    WorkflowStore.deselectStep();
                return WorkflowStore.removeChild(item, side);
            })
            .then(item.destroy)
            .then(WorkflowStore.save);
    };
    chooseWhichSideDelete = () => {
        return new Promise(function(resolve, reject) {
            Popup.create({
                closeOnOutsideClick: false,
                content: 'Какую ветку удалить?',
                buttons: {
                    left: [
                        {
                            text: 'Левую',
                            action: () => {
                                resolve('yes');
                                Popup.close();
                            }
                        },
                        {
                            text: 'Обе',
                            action: () => {
                                resolve('both');
                                Popup.close();
                            }
                        },
                        {
                            text: 'Правую',
                            action: () => {
                                resolve('no');
                                Popup.close();
                            }
                        }
                    ],
                    right: [{
                        text: 'Отменить',
                        action: () => {
                            reject('cancel');
                            Popup.close();
                        }
                    }]
                }
            });
        });
    };
    chooseWhereMoveChildren = () => {
        return new Promise(function(resolve, reject) {
            Popup.create({
                closeOnOutsideClick: false,
                content: 'Куда перенести следующие шаги?',
                buttons: {
                    left: [{
                        text: 'Влево (да)',
                        action: () => {
                            resolve('yes');
                            Popup.close();
                        }
                    }],
                    right: [
                        {
                            text: 'Отменить',
                            action: () => {
                                reject('cancel');
                                Popup.close();
                            }
                        },
                        {
                            text: 'Вправо (нет)',
                            action: () => {
                                resolve('no');
                                Popup.close();
                            }
                        }],
                }
            });
        });
    };

    insertStep = (step, parent_side = '') => {
        let {item} = this.props;
        // let promise = Promise.resolve();
        let promise = step.save();
        if (step.type === WorkflowItem.TYPE_CONDITION && item.next.length) {
            if (item.type !== WorkflowItem.TYPE_CONDITION ||
                (parent_side === 'yes' && item.data.get('next_yes') || parent_side === 'no' && item.data.get('next_no'))) {
                promise = promise.then(this.chooseWhereMoveChildren);
            }
        }
        promise
            .then(side => WorkflowStore.moveChildren(item, step, side, parent_side))
            .then(function () { WorkflowStore.appendStep(step); })
            .then(WorkflowStore.save)
            .catch(function (err) {
                // in case of popup cancel or any error
                if (err instanceof Error) {
                    console.error(err);
                }
            });
    };

    render() {
        const { item } = this.props;
        let step = null;

        if (!item) {
            // return <div className="step-content start">Oops</div>;
            return null;
        } else {
            console.log(item.type);
            switch (item.type) {
                case WorkflowItem.TYPE_START:
                    step = <StepBase
                            item={item}
                            onInsertStep={this.insertStep}
                            onRemoveStep={this.removeItem}
                    >Начало</StepBase>;
                    break;
                case WorkflowItem.TYPE_ACTION:
                    step = <StepAction
                            item={item}
                            onInsertStep={this.insertStep}
                            onRemoveStep={this.removeItem}
                        />;
                    break;
                case WorkflowItem.TYPE_EVENT:
                    step = <StepDelay
                            item={item}
                            onInsertStep={this.insertStep}
                            onRemoveStep={this.removeItem}
                        />;
                    break;
                case WorkflowItem.TYPE_DELAY:
                    step = <StepDelay
                            item={item}
                            onInsertStep={this.insertStep}
                            onRemoveStep={this.removeItem}
                        />;
                    break;
                case WorkflowItem.TYPE_CONDITION:
                    step = <StepCondition
                            item={item}
                            onInsertStep={this.insertStep}
                            onRemoveStep={this.removeItem}
                        />;
                    break;
                case WorkflowItem.TYPE_ANSWER:
                    step = <StepAnswer
                            item={item}
                            onInsertStep={this.insertStep}
                            onRemoveStep={this.removeItem}
                        />;
                    break;
                case WorkflowItem.TYPE_GOTO:
                    step = <StepGoto
                            item={item}
                            onInsertStep={this.insertStep}
                            onRemoveStep={this.removeItem}
                        />;
                    break;
            }
            const className = classnames({
                'wf-steps-subtree': true,
                'no-line': this.props.firstStep,
            });
            return <div className={className}>
                {step}
                </div>;
        }
    }
}