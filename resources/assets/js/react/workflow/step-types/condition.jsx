import React from 'react';
import {observer} from 'mobx-react';
import WorkflowItem from 'classes/workflow-item'
import AddStepButton from '../add-step-button'
import WorkflowStore from '../../stores/WorkflowStore';
import SectionStore from '../../stores/SectionsStore';
import WorkflowStep from '../step';
import Condition from '../../classes/condition';
import StepBase from './base';
import IconCross from "react-icons/lib/fa/close";

@observer
export default class StepCondition extends StepBase {
    baseContentClasses = {
        'step-content': true,
        'condition': true,
    };

    handleAddStepYes = (type) => {
        this.props.onInsertStep(new WorkflowItem(type), 'yes');
    };
    handleAddStepNo = (type) => {
        this.props.onInsertStep(new WorkflowItem(type), 'no');
    };

    render() {
        const { item, className, onRemoveStep } = this.props;

        if (!item) {
            // return <div className="step-content start">Oops</div>;
            return null;
        } else {
            let removeButton = null;
            if (item.parent_id) {
                removeButton = <a href="#" className="step-delete" onClick={onRemoveStep}><IconCross /></a>;
            }
            let title = this.getTitle(item);
            return <div className="step-wrap">
                <div className={this.getClasses(className)} onClick={this.handleSelect}>
                    {title}
                    {removeButton}
                </div>
                <div className="wf-steps-decisions">
                    <div className="decision-steps">
                        <div className="condition-rule yes">
                            yes
                        </div>
                        <div className="wf-steps-subtree">
                            <AddStepButton onSelect={this.handleAddStepYes} className="no-line"/>
                            <WorkflowStep item={item.next_yes}/>
                        </div>
                    </div>
                    <div className="decision-steps">
                        <div className="condition-rule no">
                            no
                        </div>
                        <div className="wf-steps-subtree">
                            <AddStepButton onSelect={this.handleAddStepNo} className="no-line"/>
                            <WorkflowStep item={item.next_no}/>
                        </div>
                    </div>
                </div>
            </div>;
        }
    }

    getTitle(item) {
        let title = [];
        let br_index = 0;
        switch(item.data.get('type')) {
            case "user_field":
                title.push('Пользовательское поле');
                let user_fields = WorkflowStore.getUserFields();
                if (!item.data.get('field_name')) return title;

                let selected_field = user_fields[item.data.get('field_name')];
                if (!selected_field) return title;

                title.push(<br  key={br_index++}/>);
                title.push('"' + selected_field.name + '"');
                break;
            case "task_answer":
                title.push('Ответ пользователя');
                let section, assignment, task, assignment_title = '';
                if (item.data.get('section_id')) {
                    section = SectionStore.getById(item.data.get('section_id'));
                    assignment_title += `в задании из раздела "${section.name}"`;
                }
                if (item.data.get('assignment_id')) {
                    assignment = section.assignments.find(assignment => assignment.id == item.data.get('assignment_id'));
                    assignment_title += ` упражнения "${assignment.name}"`;
                }
                if (item.data.get('task_id')) {
                    task = assignment.tasks.find((assignment => assignment.id == item.data.get('task_id')));
                    assignment_title += ` "${(task.name ? task.name : 'id: ' + task.id)}"`;
                }
                if (assignment_title) {
                    title.push(<br key={br_index++}/>);
                    title.push(assignment_title);
                }
                break;
        }
        if (item.data.get('condition')) {
            title.push(<br  key={br_index++}/>);
            title.push(Condition.getConditionLabel(item.data.get('condition')));
        }

        if (['empty', 'not_empty'].indexOf(item.data.get('condition')) === -1) {
            title.push(<br  key={br_index++}/>);
            title.push('"' + item.data.get('condition_value') + '"');
        }

        return title;
    }
}