import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {observer} from 'mobx-react';
import classnames from 'classnames'
import WorkflowStore from 'stores/WorkflowStore'
import WorkflowItem from 'classes/workflow-item'
import AddStepButton from '../add-step-button'
import WorkflowStep from "../step";
import IconCross from "react-icons/lib/fa/close";

@observer
export default class StepBase extends Component {
    static propTypes = {
        item: PropTypes.object.isRequired,
        onInsertStep: PropTypes.func.isRequired,
        onRemoveStep: PropTypes.func.isRequired,
        className: PropTypes.string,
    };
    baseContentClasses = {
        'step-content': true,
    };
    handleAddStep = (type) => {
        let step = new WorkflowItem(type);
        this.props.onInsertStep(step);
    };
    handleSelect = (e) => {
        e.preventDefault();
        WorkflowStore.selectStep(this.props.item);
    };
    getSelected() {
        return this.props.item.equals(WorkflowStore.selectedStep);
    };
    getClasses(additionalClass) {
        let classes = Object.assign(
            this.baseContentClasses,
            {
                selected: this.getSelected(),
            });
        if (additionalClass)
            classes[additionalClass] = true;
        return classnames(classes);
    }

    render() {
        const { item, className, onRemoveStep, children } = this.props;

        if (!item) {
            // return <div className="step-content start">Oops</div>;
            return null;
        } else {
            let removeButton = null;
            if (item.parent_id) {
                removeButton = <a href="#" className="step-delete" onClick={onRemoveStep}><IconCross /></a>;
            }
            return <div className="step-wrap">
                <div className={this.getClasses(className)} onClick={this.handleSelect}>
                    {children}
                    {removeButton}
                </div>
                <AddStepButton onSelect={this.handleAddStep}/>
                <WorkflowStep item={item.next_one}/>
            </div>;
        }
    }
}