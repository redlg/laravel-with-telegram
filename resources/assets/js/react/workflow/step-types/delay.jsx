import React, { Component } from 'react';
import {observer} from 'mobx-react';
import AddStepButton from '../add-step-button'
import IconCross from "react-icons/lib/fa/close";
import WorkflowStep from '../step';
import StepBase from './base';

@observer
export default class StepDelay extends StepBase {
    baseContentClasses = {
        'step-content': true,
        'delay': true,
    };
    delay_units = {
        minutes: 'минут',
        hours: 'часов',
        days: 'дней',
    };

    render() {
        const {item, className, onRemoveStep} = this.props;

        if (!item) {
            // return <div className="step-content start">Oops</div>;
            return null;
        } else {
            let removeButton = null;
            if (item.parent_id) {
                removeButton = <a href="#" className="step-delete" onClick={onRemoveStep}><IconCross /></a>;
            }
            let period = this.delay_units[item.data.get('delay_unit')] || '';
            let text = item.data.get('value') ? `Подождать ${item.data.get('value')} ${period}` : "Задержка";
            return <div className="step-wrap">
                <div className={this.getClasses(className)} onClick={this.handleSelect}>
                    {text}
                    {removeButton}
                </div>
                <AddStepButton onSelect={this.handleAddStep}/>
                <WorkflowStep item={item.next_one}/>
            </div>;
        }
    }
}