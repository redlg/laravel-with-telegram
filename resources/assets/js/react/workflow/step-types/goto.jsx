import React, { Component } from 'react';
import {observer} from 'mobx-react';
import AddStepButton from '../add-step-button'
import IconCross from "react-icons/lib/fa/close";
import WorkflowStep from '../step';
import WorkflowStore from '../../stores/WorkflowStore';
import StepBase from './base';

@observer
export default class StepGoto extends StepBase {
    baseContentClasses = {
        'step-content': true,
        'delay': true,
    };

    render() {
        const {item, className, onRemoveStep} = this.props;

        if (!item) {
            // return <div className="step-content start">Oops</div>;
            return null;
        } else {
            let removeButton = null;
            if (item.parent_id) {
                removeButton = <a href="#" className="step-delete" onClick={onRemoveStep}><IconCross /></a>;
            }
            let target_workflow = WorkflowStore.getById(parseInt(item.data.get('target')));
            return <div className="step-wrap">
                <div className={this.getClasses(className)} onClick={this.handleSelect}>
                    Переход к сценарию <br />
                    "{target_workflow ? target_workflow.name : ""}"
                    {removeButton}
                </div>
                <AddStepButton onSelect={this.handleAddStep}/>
                <WorkflowStep item={item.next_one}/>
            </div>;
        }
    }
}