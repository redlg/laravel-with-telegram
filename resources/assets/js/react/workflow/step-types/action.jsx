import React, { Component } from 'react';
import {observer} from 'mobx-react';
import AddStepButton from '../add-step-button'
import IconCross from "react-icons/lib/fa/close";
import WorkflowStep from '../step';
import StepBase from './base';
import AssignmentStore from "stores/AssignmentsStore";

@observer
export default class StepAction extends StepBase {
    baseContentClasses = {
        'step-content': true,
        'delay': true,
    };
    action_type = {
        send_message: 'Отправить сообщение',
        set_custom_field: 'Обновить поле',
    };
    message_type = {
        text: 'текст',
        image: 'изображение',
        video: 'видео',
    };

    render() {
        const {item, className, onRemoveStep} = this.props;

        if (!item) {
            // return <div className="step-content start">Oops</div>;
            return null;
        } else {
            let removeButton = null;
            if (item.parent_id) {
                removeButton = <a href="#" className="step-delete" onClick={onRemoveStep}><IconCross /></a>;
            }
            let text = "";
            if (item.data.get('type') === 'send_message') {
                switch (item.data.get('message_type')) {
                    case 'text':
                        let assignment;
                        if (item.data.get('save_answers')) {
                            assignment = AssignmentStore.getById(item.data.get('save_answer_to'));
                        }
                        text = <div className="step-action__message">
                            Отправить текст
                            <p className="message-text">
                                {item.data.get('value')}
                            </p>
                            {item.data.get('save_answers') &&
                                <div>
                                    И сохранять ответы в упражнение:
                                    <b>{assignment ? assignment.name : 'не выбрано'}</b>
                                </div>
                            }
                        </div>;
                        break;
                    case 'image':
                        text = <div className="step-action__message">
                            Отправить картинку
                            { item.data.get('file_id') &&
                                [
                                    <br key="1"/>,
                                    <img key="2" className="message-image" src={item.data.get('file_url')} alt=""/>
                                ]
                            }
                            { item.data.get('value') &&
                            <p className="message-text">
                                {item.data.get('value')}
                            </p>}
                        </div>;
                        break;
                    case 'video':
                        text = <div className="step-action__message">
                            Отправить видео
                            { item.data.get('file_id') &&
                            [
                                <br key="1"/>,
                                <video key="2" className="message-video">
                                    <source src={item.data.get('file_url')}/>
                                </video>
                            ]}
                            {item.data.get('value') &&
                            <p key="3" className="message-text">
                                {item.data.get('value')}
                            </p>}
                        </div>;
                        break;
                    case 'sticker':
                        text = <div className="step-action__message">
                            Отправить стикер:
                            <img className="message-sticker" src={item.data.get('file_id')} alt=""/>
                        </div>;
                        break;
                    case 'assignment_field':{
                        let assignment = AssignmentStore.getById(parseInt(item.data.get('assignment_id')));
                        let field_label = AssignmentStore.getFieldLabel(item.data.get('assignment_field'));
                        text = <div className="step-action__message">
                            Отправить {field_label} из упражнения "{assignment && assignment.name}"
                        </div>;
                    }
                        break;
                    default:
                        text = <div className="step-action__message">
                            Отправить сообщение
                        </div>;
                        break;
                }
            } else if (item.data.get('type') === 'update_custom_field') {
                text = <div className="step-action__update_field">
                    Установить значение поля <b>{item.data.get('field')}</b> равным <b>{item.data.get('value')}</b>
                </div>;
            } else if (item.data.get('type') === 'send_notification') {
                text = <div className="step-action__update_field">
                    Установить значение поля <b>{item.data.get('field')}</b> равным <b>{item.data.get('value')}</b>
                </div>;
            } else {
                text = <div className="step-action__none">
                    Действие
                </div>;
            }
            return <div className="step-wrap">
                <div className={this.getClasses(className)} onClick={this.handleSelect}>
                    {text}
                    {removeButton}
                </div>
                <AddStepButton onSelect={this.handleAddStep}/>
                <WorkflowStep item={item.next_one}/>
            </div>;
        }
    }
}