import React, { Component } from 'react';
import {observer} from 'mobx-react';
import AddStepButton from '../add-step-button'
import IconCross from "react-icons/lib/fa/close";
import WorkflowStep from '../step';
import StepBase from './base';
import SectionStore from '../../stores/SectionsStore';

@observer
export default class StepAnswer extends StepBase {
    baseContentClasses = {
        'step-content': true,
        'delay': true,
    };

    render() {
        const {item, className, onRemoveStep} = this.props;

        if (!item) {
            // return <div className="step-content start">Oops</div>;
            return null;
        } else {
            let removeButton = null;
            if (item.parent_id) {
                removeButton = <a href="#" className="step-delete" onClick={onRemoveStep}><IconCross /></a>;
            }
            let text = "Ответ от пользователя";
            let section, assignment, task;
            if (item.data.get('section_id')) {
                section = SectionStore.getById(item.data.get('section_id'));
                text += ` сохранить в задание из раздела "${section.name}"`;
            }
            if (item.data.get('assignment_id')) {
                assignment = section.assignments.find(assignment => assignment.id == item.data.get('assignment_id'));
                text += ` упражнения "${assignment.name}"`;
            }
            if (item.data.get('task_id')) {
                task = assignment.tasks.find((assignment => assignment.id == item.data.get('task_id')));
                text += ` "${(task.name ? task.name : 'id: ' + task.id)}"`;
            }
            return <div className="step-wrap">
                <div className={this.getClasses(className)} onClick={this.handleSelect}>
            {text}
            {removeButton}
        </div>
            <AddStepButton onSelect={this.handleAddStep}/>
        <WorkflowStep item={item.next_one}/>
        </div>;
        }
    }
}