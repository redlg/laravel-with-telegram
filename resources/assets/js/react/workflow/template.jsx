import React, { Component } from 'react';
import {observer} from 'mobx-react';
import WorkflowStore from '../stores/WorkflowStore';
import WorkflowStep from './step';

@observer
class WorkflowTemplate extends Component {
    render() {
        return <WorkflowStep firstStep={true} item={WorkflowStore.first}/>;
    }
}
export default WorkflowTemplate;
