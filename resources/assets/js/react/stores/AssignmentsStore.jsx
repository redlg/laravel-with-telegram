import _ from 'lodash';
import axios from 'axios';
import uuid from 'uuid';
import { observable, action, computed, extendObservable } from 'mobx';

const _AssignmentsStore = new class AssignmentsStore {
    @observable items = [];

    constructor() {
        this.load();
        // this.items.push({
        //     "id": 1,
        //     "title": "Начало",
        //     "description": "Я хочу сделать:"
        // });
        // this.items.push({
        //     "id": 2,
        //     "title": "Что я люблю",
        //     "description": "Я люблю:"
        // });
    }

    load() {
        let routes = Love.routes;
        if (!routes) return;
        axios.get(routes.assignment.all)
            .then(action((result) => {
                if (result.data) {
                    result.data.forEach(item => this.append(item));
                }
            }));
    }

    getById(id) {
        return _.find(this.items, item => {return item.id === id});
    }

    equals(assign1, assign2) {
        return assign1 && assign2 && assign1.id === assign2.id;
    }

    @action append(item) {
        this.items.push(item);
    }

    @action remove (item) {
        this.items = this.items.filter((l) => {
            return l.id !== item.id
        })
    }

    fields = {
        'name': {
            label: 'Название',
            code: 'name',
        },
        'description': {
            label: 'Описание',
            code: 'description',
        },
        'video': {
            label: 'Видео',
            code: 'video',
        },
    };
    getFieldLabel(code) {
        if (this.fields[code]) {
            return this.fields[code].label;
        }
        return '';
    }
};

export default _AssignmentsStore;