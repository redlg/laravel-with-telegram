import _ from 'lodash';
import uuid from 'uuid';
import axios from 'axios';
import { observable, action, computed } from 'mobx';
import Workflow from 'classes/workflow';
import WorkflowItem from 'classes/workflow-item';
const Love = window.Love;

const _WorkflowStore = new class WorkflowStore {
    @observable steps = [];
    @observable items = [];
    @observable first = null;
    @observable selectedStep = null;

    constructor() {
        this.load();
    }

    load() {
        let routes = Love.routes;
        if (!routes) return;
        axios.get(routes.workflow.list)
            .then(action((result) => {
                if (result.data) {
                    this.items = result.data.map((item) => new Workflow(item));
                }
            }))
            .then(() => {
                return axios.get(routes.workflow.steps.list);
            })
            .then(action((result) => {
                if (result.data) {
                    this.first = new WorkflowItem(result.data);
                    this.fillStepItems(this.first);
                }
            }));
    }

    fillStepItems(item) {
        this.steps.push(item);
        _.each(item.next, (next_item) => {
            this.fillStepItems(next_item);
        });
    }
    getById(id) {
        return _.find(this.items, item => item.id === id);
    }
    getStepById(id) {
        return _.find(this.steps, item => item.id === id);
    }
    equals(step1, step2) {
        return step1 && step1.equals(step2);
    }

    @action selectStep(step) {
        this.selectedStep = step;
    }
    @action deselectStep() {
        this.selectedStep = null;
    }

    @action appendStep(newStep) {
        this.steps.push(newStep);
    }

    @action removeStep (item) {
        this.steps = this.steps.filter((l) => {
            return l.id !== item.id
        })
    }

    @action moveChildren(item, newStep, stepSide = '', parent_side = '') {
        if (!newStep) return null;

        let next_yes_no_id = false,
            child;
        if (parent_side) {
            next_yes_no_id = parent_side === 'yes' ? item.data.get('next_yes') : item.data.get('next_no');
            child = _.find(item.next, child => child.id === next_yes_no_id);
        } else {
            child = item.next.length && item.next[0];
        }

        newStep.parent_id = item.id;
        // if item is new
        if (child || !newStep.next)
            newStep.next = child ? [ child ] : [];

        // if moving children to yes
        if (stepSide === 'yes') {
            newStep.data.set('next_yes', child.id);
            // if moving children to no
        } else if (stepSide === 'no') {
            newStep.data.set('next_no', child.id);
        }
        // else newstep is not a conditional step or there wasn't children in item

        if (child) {
            child.parent_id = newStep.id;
            item.next = item.next.filter(item_child => item_child.id !== child.id);
        }
        if (parent_side) {
            if (parent_side === 'yes') {
                item.data.set('next_yes', newStep.id);
                // if moving children to no
            } else if (parent_side === 'no') {
                item.data.set('next_no', newStep.id);
            }
        }
        item.next.push(newStep);

        return null;
    }

    save = () => axios.put(Love.routes.workflow.step_path, this.first);

    @action deleteItemChain(items) {
        if (!items) return;
        if (!items.length) {
            items = [items];
        }
        items.forEach((item) => {
            this.deleteItemChain(item.next);
            this.removeStep(item);
        });
    }
    @action removeChild(item, side_to_delete) {
        return new Promise((resolve, reject) => {
            let parent = this.getStepById(item.parent_id);
            let parent_side = '';
            if (parent.type === WorkflowItem.TYPE_CONDITION) {
                if (parent.data.get('next_yes') === item.id) {
                    parent_side = 'yes';
                } else if (parent.data.get('next_no') === item.id) {
                    parent_side = 'no';
                }
            }
            if (item.type === WorkflowItem.TYPE_CONDITION) {
                if (item.data.get('next_yes') && item.data.get('next_no')) {
                    parent.removeChild(item.id);
                    if (side_to_delete === 'yes') {
                        return this.moveChildren(parent, item.next_no, '', parent_side);
                    } else if (side_to_delete === 'no') {
                        return this.moveChildren(parent, item.next_yes, '', parent_side);
                    } else if (side_to_delete === 'both') {
                        if (parent_side === 'yes') {
                            delete parent.data.delete('next_yes');
                        } else if (parent_side === 'no') {
                            delete parent.data.delete('next_no');
                        }
                        parent.next = parent.next.filter((child) => child.id !== item.id);
                    }
                } else {
                    parent.removeChild(item.id);
                    if (item.data.get('next_yes')) {
                        this.moveChildren(parent, item.next_yes, '', parent_side);
                    } else if (item.data.get('next_no')) {
                        this.moveChildren(parent, item.next_no, '', parent_side);
                    }
                }
                if (side_to_delete) {
                    this.deleteItemChain(item.next);
                }
            } else {
                parent.removeChild(item.id);
                this.moveChildren(parent, item.next_one, '', parent_side);
            }
            this.removeStep(item);
            resolve();
        });
    }

    getUserFields() {
        return _(Love.Workflow.fields.User)
            .merge(
                _(Love.Workflow.fields.UserFields)
                    .mapValues((field) => _.extend({}, field, {field_name: "fields." + field.field_name}))
                    .mapKeys((field) => field.field_name)
                    .value()
            )
            .value();
    }
};

export default _WorkflowStore;