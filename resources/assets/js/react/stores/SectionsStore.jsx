import _ from 'lodash';
import axios from 'axios';
import uuid from 'uuid';
import { observable, action, computed, extendObservable } from 'mobx';

const _SectionsStore = new class SectionsStore {
    @observable items = [];

    constructor() {
        this.load();
        // this.items.push({
        //     "id": 1,
        //     "title": "Начало",
        //     "description": "Я хочу сделать:"
        // });
        // this.items.push({
        //     "id": 2,
        //     "title": "Что я люблю",
        //     "description": "Я люблю:"
        // });
    }

    load() {
        let routes = Love.routes;
        if (!routes) return;
        axios.get(routes.section.list)
            .then(action((result) => {
                if (result.data) {
                    result.data.forEach(item => this.append(item));
                }
            }));
    }

    getById(id) {
        return _.find(this.items, item => item.id === id);
    }

    equals(assign1, assign2) {
        return assign1 && assign2 && assign1.id === assign2.id;
    }

    @action append(item) {
        this.items.push(item);
    }

    @action remove (item) {
        this.items = this.items.filter((l) => {
            return l.id !== item.id
        })
    }
};

export default _SectionsStore;