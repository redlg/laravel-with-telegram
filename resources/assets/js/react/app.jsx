import React from 'react';
import PropTypes from 'prop-types';
import ReactDOM from 'react-dom';
import WorkflowBuilder from './workflow/builder';
import { AppContainer } from 'react-hot-loader';
import Popup from 'react-popup';
import DevTools from 'mobx-react-devtools';
import LoveTheme from 'LoveTheme';
import { useStrict } from 'mobx';
import axios from 'axios';

useStrict(true);

class App extends React.Component {
    static childContextTypes = {
        theme: PropTypes.object.isRequired
    };
    getChildContext() {
        return {
            theme: LoveTheme
        };
    }
    render() {
        return <div>
            <WorkflowBuilder />
            <Popup/>
            <DevTools/>
        </div>
    }
}

const render = (WorkflowBuilder) => {
    ReactDOM.render(
        <AppContainer>
            <App />
        </AppContainer>,
        document.getElementById('wf-builder')
    );
};

// Hot Module Replacement API
if (module.hot) {
    module.hot.accept('./workflow/builder', () => {
        render(WorkflowBuilder);
    });
}

$(function () {
    document.body.classList.add('fadeOut');
    setTimeout(function() {
        document.body.classList.remove('loading', 'fadeOut');
    }, 500);
    render(WorkflowBuilder);
});