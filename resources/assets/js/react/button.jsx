import React, {Component} from 'react';
import classnames from 'classnames';

export default class Button extends Component {
    constructor(props) {
        super(props);
        this.state = {
            hover: false,
            focus: false
        }
    }
    onMouseEnter = (e) => {
        this.setState({
            hover: true
        });
    };
    onMouseLeave = (e) => {
        this.setState({
            hover: false
        });
    };
    onFocus = (e) => {
        this.setState({
            focus: true
        });
    };
    onBlur = (e) => {
        this.setState({
            focus: false
        });
    };
    render() {
        const {className, ...other} = this.props;
        const {hover, focus} = this.state;
        const classes = classnames({
                "btn": true,
                hover,
                focus,
        }) + " " + (className || "");
        return <button onMouseEnter={this.onMouseEnter}
                       onMouseLeave={this.onMouseLeave}
                       onFocus={this.onFocus}
                       onBlur={this.onBlur}
                       className={classnames(classes)}
                       {...other} />
    }
}