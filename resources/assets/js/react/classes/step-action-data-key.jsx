import _ from 'lodash';
import { observable, action } from 'mobx';
import BaseObject from './base-object';

export default class StepActionDataKey extends BaseObject {
    @observable id = '';
    @observable text = '';
    @observable url = '';
    @observable type = '';
}