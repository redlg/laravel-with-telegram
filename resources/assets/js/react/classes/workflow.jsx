import { observable, action, computed, toJS } from 'mobx';
import _ from 'lodash';
import uuid from 'uuid';
import axios from 'axios';
import WorkflowStore from 'stores/WorkflowStore';

let counter = 1;
const Love = window.Love;
export default class WorkflowItem {
    @observable id = 0;
    @observable name = '';
    @observable active = '';

    constructor(params) {
        if (!_.isObject(params)) {
            params = { type: params };
        }
        this.id = params.id;
        this.name = params.name;
        this.active = params.active;
    }

    getUrl(urlTemplate, params) {
        // if (!_.isObject(params)) {
        //     params = { '_workflow_': params };
        // }
        // return _.reduce(params, (result, value, key) => {
        //     return result.replace(key, value);
        // }, urlTemplate);
        return urlTemplate;
    }

    route(type) {
        switch (type) {
            case 'list':
                return this.getUrl(Love.routes.workflow.list);
            case 'show':
                return this.getUrl(Love.routes.workflow.show);
        }
    }

    equals(step2) {
        return step2 && this.id === step2.id;
    }
}