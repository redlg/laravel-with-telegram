import _ from 'lodash';

class Condition {
    static conditions = {
        "eq": "=",
        "lt": "&lt;",
        "gt": "&gt;",
        "lt_eq": "&lt;=",
        "gt_eq": "&gt;=",
        "not_empty": "Не пусто",
        "empty": "Пусто",
        "starts_with": "Начинается с",
        "ends_with": "Заканчивается на",
        "contains": "Содержит",
    };

    static getConditions() {
        return _.keys(Condition.conditions);
    }
    static getConditionLabel(condition) {
        return _.get(Condition.conditions, condition, condition);
    }
}

export default Condition;