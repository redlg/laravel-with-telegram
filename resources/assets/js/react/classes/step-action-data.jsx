import _ from 'lodash';
import { observable, action } from 'mobx';
import BaseObject from './base-object';

export default class StepActionData extends BaseObject {
    @observable type = '';
    @observable message_type = '';
    @observable save_answers = false;
    @observable save_answer_to = false;

    @observable section_id = 0;
    @observable assignment_id = 0;
    @observable assignment_field = '';

    @observable keyboard_show = false;
    @observable keyboard_keys = [];

    @observable value = '';
    @observable file_id = '';
    @observable file_url = '';
}