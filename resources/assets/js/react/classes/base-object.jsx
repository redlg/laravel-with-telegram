import { observable, action, toJS } from 'mobx';

export default class BaseObject {
    casts = {};

    constructor(props) {
        if (!_.isUndefined(props))
            this.setData(props);
    }

    @action setData(props) {
        _.each(props, (value, key) => {
            this[key] = value  !== undefined ? this.castAttribute(key, value) : null;
        });
    }
    toJS() {
        let result = {};
        _.each(Object.keys(this), (key) => {
            result[key] = toJS(this[key]);
        });
        return result;
    }

    castAttribute(field, value) {
        if (this.hasCast(field)) {
            let castType = this.getCastType(field);
            switch(castType) {
                case 'int':
                case 'integer':
                    return parseInt(value);
                case 'real':
                case 'float':
                case 'double':
                    return parseFloat(value);
                case 'string':
                    return String(value);
                case 'date':
                case 'datetime':
                    return new Date(value);
            }
        }
        return value;
    }
    getCastType(field) {
        return this.casts[field].toLowerCase();
    }
    hasCast(field) {
        return this.casts[field] !== undefined;
    }
}