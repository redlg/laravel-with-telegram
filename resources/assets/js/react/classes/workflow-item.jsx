import { observable, action, computed, toJS } from 'mobx';
import _ from 'lodash';
import uuid from 'uuid';
import axios from 'axios';
import WorkflowStore from 'stores/WorkflowStore';

let counter = 1;
const Love = window.Love;
export default class WorkflowItem {
    static get TYPE_START() {
        return 'start';
    };
    static get TYPE_DELAY() {
        return 'delay';
    };
    static get TYPE_ACTION() {
        return 'action';
    };
    static get TYPE_CONDITION() {
        return 'condition';
    };
    static get TYPE_ANSWER() {
        return 'answer';
    };
    static get TYPE_GOTO() {
        return 'goto';
    };

    @observable id = '';
    // @observable type = 'delay';
    // data = observable.map({});
    data = observable.map({});
    workflow_id = null;
    @observable next = [];
    @observable parent_id = undefined;
    constructor(params) {
        if (!_.isObject(params)) {
            params = { type: params };
        }
        if (params.id) {
            this.id = params.id;
        } else {
            this.id = uuid.v4();
            this.is_new = true;
        }
        this.workflow_id = params.workflow_id;
        this.type = params.type;
        this.setData(params.data);
        this.parent_id = params.parent_id;
        this.next = _.map(params.next, (item) => {
            if (!(item instanceof WorkflowItem)) {
                return new WorkflowItem(item);
            }
            return item;
        });
    }

    @computed get next_one() {
        return this.getNext();
    }
    @computed get next_yes() {
        if (this.next && this.data.get('next_yes')) {
            return this.getNext(this.data.get('next_yes'));
        }
        return undefined;
    }
    @computed get next_no() {
        if (this.next && this.data.get('next_no')) {
            return this.getNext(this.data.get('next_no'));
        }
        return undefined;
    }

    @action removeChild(child_id) {
        if (this.type === WorkflowItem.TYPE_CONDITION) {
            if (this.data.get('next_yes') === child_id) {
                delete this.data.delete('next_yes');
            } else if (this.data.get('next_no') === child_id) {
                delete this.data.delete('next_no');
            }
        }
        this.next = this.next.filter(child => child.id !== child_id);
    }

    @action
    setData(data) {
        this.data = observable.map(data);
    }

    save = () => {
        let obj = toJS(this);
        delete obj.next;
        if (this.is_new) {
            return axios.post(this.route('steps.create'), obj)
                .then((result) => {
                    this.id = result.data.id;
                    this.is_new = false;
                    return this;
                });
        } else {
            return axios.put(this.route('steps.update'), obj);
        }
    };

    @action deleteFile = () => {
        let delete_route = this.route('steps.file.delete');

        return axios.delete(delete_route)
            .then(this.afterDeleteFile)
            .catch(this.onDeleteError);
    };
    @action afterDeleteFile = () => {
        this.data.set('file_id', null);
        this.data.set('file_url', null);
    };
    @action onDeleteError = (error) => {
        if (error.response) {
            toastr.error(error.response.data, error.response.status);
        } else {
            toastr.error(error.message, "Ошибка");
        }
        this.data.file_id = file_id;
        this.data.file_url = file_url;
    };

    destroy = () => {
        return axios.delete(this.route('steps.delete'))
    };

    getUrl(urlTemplate, params) {
        if (!_.isObject(params)) {
            params = { '_step_': params };
        }
        return _.reduce(params, (result, value, key) => {
            return result.replace(key, value);
        }, urlTemplate);
    }

    route(type) {
        switch (type) {
            case 'steps.create':
                return this.getUrl(Love.routes.workflow.steps.create, this.id);
            case 'steps.update':
                return this.getUrl(Love.routes.workflow.steps.update, this.id);
            case 'steps.delete':
                return this.getUrl(Love.routes.workflow.steps.delete, this.id);
            case 'steps.file.upload':
                return this.getUrl(Love.routes.workflow.steps.file.upload, this.id);
            case 'steps.file.delete':
                return this.getUrl(Love.routes.workflow.steps.file.delete, {
                    '_step_': this.id,
                    '_file_': this.data.get('file_id')
                });
        }
    }

    /**
     * Returns next next step with id equals to `next_id` if no `next_id` provided returns first element from next's
     * @param next_id
     * @returns {*}
     */
    getNext(next_id) {
        if(this.next) {
            if (next_id)
                return _.find(this.next, next_item => next_item.id === next_id);
            else if(this.next.length)
                return this.next[0];
        }
        return undefined;
    };
    equals(step2) {
        return step2 && this.id === step2.id;
    }
}