import _ from 'lodash';
import { observable, action } from 'mobx';
import BaseObject from './base-object';

export default class StepDelayData extends BaseObject {
    casts = {
        section_id: 'int',
        assignment_id: 'int',
        task_id: 'int'
    };

    @observable type = '';
    @observable field_name = '';

    @observable section_id = 0;
    @observable assignment_id = 0;
    @observable task_id = 0;

    @observable condition = '';
    @observable condition_value = '';
}
