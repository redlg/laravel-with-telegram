import _ from 'lodash';

export default new class Cookies {
    constructor() {
        this.cookies = _.fromPairs(document.cookie.split(';').map(cookie => {
            let pair = cookie.trim().split('=', 2);
            return [pair[0], decodeURIComponent(pair[1])]
        }), 'key');
    }
    set(name, value, options = {}) {
        let expires = options.expires;

        if (typeof expires == "number" && expires) {
            let d = new Date();
            d.setTime(d.getTime() + expires * 1000);
            expires = options.expires = d;
        }
        if (expires && expires.toUTCString) {
            options.expires = expires.toUTCString();
        }

        value = encodeURIComponent(value);

        let updatedCookie = name + "=" + value;

        for (let propName in options) {
            updatedCookie += "; " + propName;
            let propValue = options[propName];
            if (propValue !== true) {
                updatedCookie += "=" + propValue;
            }
        }

        document.cookie = updatedCookie;
        this.cookies[name] = value;
    }
    get(name, _default = undefined) {
        return this.cookies[name] !== undefined ? this.cookies[name] : _default;
    }
    delete(name) {
        this.set(name, "", {
            expires: -1
        })
    }

}
