<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title>Lovelify | Построй осознанные отношения</title>
    <meta property="og:url" content="http://lovelify.ru/"/>
    <meta property="og:title" content="Lovelify | Построй осознанные отношения"/>
    <meta property="og:description" content="Удели 10 минут в день, чтобы понять себя лучше и построить осознанные отношения. Запуск 1 июня. Регистрация уже открыта!"/>
    <meta property="og:type" content="website"/>
    <meta property="og:image" content="http://lovelify.ru/assets/landing/images/lov_fb.jpg"/>
    <meta property="fb:app_id" content="257953674358265"/>
    <meta name="format-detection" content="telephone=no"/>
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon"/><!-- Assets -->

    <link href='//fonts.googleapis.com/css?family=Ubuntu:400,300,500,700&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="/css/normalize.css">
    {{--<link rel="stylesheet" href="/components/toastr/toastr.min.css">--}}
    <link rel="stylesheet" href="/css/toastr.css">

    <script src="/components/jquery/dist/jquery.min.js"></script>
    <script src="/components/toastr/toastr.min.js"></script>
    <script src="/components/lodash/dist/lodash.core.js"></script>
    <script src="/js/vendor/jquery.expanding.js"></script>
    <script src="/js/imageLoader.js"></script>
    <script src="/js/validation.js"></script>
    <script src="/js/jsonrpc.js"></script>
    <script src="/js/utils.js"></script>
    <script src="/js/app.js"></script>

    @stack('styles')
    @stack('scripts')
</head>
<body class="t-body" style="margin:0;"><!--allrecords-->

@yield('content')

@include('layouts/analytics')
</body>
</html>