@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <a href="{{ route('section.index') }}" class="btn-link">← К списку</a>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        @if($section->exists)
                            Изменение раздела
                        @else
                            Добавление раздела
                        @endif
                    </div>
                    <div class="panel-body">
                        <form class="form-horizontal"
                              role="form" method="POST"
                              action="{{ $section->exists ? route('section.update', $section->id) : route('section.store') }}">
                            {{ csrf_field() }}
                            {{ $section->exists ? method_field('PUT') : "" }}

                            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                <label for="name" class="col-md-4 control-label">Название</label>

                                <div class="col-md-6">
                                    <input id="name" type="text" class="form-control" name="name" value="{{ old('name', $section->name) }}" required autofocus>

                                    @if ($errors->has('name'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('position') ? ' has-error' : '' }}">
                                <label for="position" class="col-md-4 control-label">Порядок</label>
                                <div class="col-md-6">
                                    {{ \Form::select('position', $select_positions, old('position', $section->position), ['class' => 'form-control']) }}
                                    @if ($errors->has('position'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('position') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-8 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        @if($section->exists)
                                            Измененить
                                        @else
                                            Добавить
                                        @endif
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
