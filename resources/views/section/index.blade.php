@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Разделы</div>
                    {{--<div class="panel-body"></div>--}}
                    <table class="table table-striped table-hover">
                        <tr>
                            <th width="50">ID</th>
                            <th>Название</th>
                            <th>Позиция</th>
                            <th width="250"></th>
                        </tr>
                        @foreach($sections as $item)
                            <tr id="section_{{ $item->id }}" class="{{ $item->active ? 'active' : '' }}">
                                <td>{{ $item->id }}</td>
                                <td><a href="{{ route('section.edit', $item->id) }}">{{ $item->name }}</a></td>
                                <td>{{ $item->position }}</td>
                                <td>
                                    <a href="{{ route('section.assignment.index', $item->id) }}"
                                       class="btn btn-xs btn-primary">упражнения</a>
                                    <a href="{{ route('section.edit', $item->id) }}"
                                       class="btn btn-xs btn-primary">изменить</a>
                                    <a href="{{ route('section.destroy', $item->id) }}"
                                       onclick="return postDelete(this, $('#section_{{ $item->id }}'))"
                                       class="btn btn-xs btn-danger">удалить</a>
                                </td>
                            </tr>
                        @endforeach
                        <tr>
                            <td colspan="4">
                                <a href="{{ route('section.create') }}" class="btn btn-primary">Добавить новый</a>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <script>
        function postDelete(link, deleteItem) {
            var url = $(link).attr('href');
            if (confirm('Уверены, что хотите удалить?')) {
                $.ajax({
                    url: url,
                    method: 'post',
                    dataType: 'json',
                    data: {
                        _method: 'delete'
                    }
                }).done(function (data) {
                    if (data.error) {
                        toastr.error(data.error);
                    } else {
                        toastr.info(data.message || 'Удалено');
                        if (deleteItem) {
                            if (!deleteItem.jquery) {
                                deleteItem = $(deleteItem);
                            }
                            deleteItem.remove();
                        }
                    }
                }).fail(function(xhr, status, error) {
                    toastr.error(error);
                });
            }
            return false;
        }
    </script>
@endsection