@extends('layouts/app')

@section('content')
    <div class="container">
        <a target="_blank" href="{{ Auth::user()->getBotAddingUrl() }}" class="btn btn-default">Добавить бота</a>
        @foreach($sections as $section)
            <h2>{{ $section->name }}</h2>
            @foreach($section->assignments as $assignment)
                <h3>{{ $assignment->name }}</h3>
                @foreach($assignment->tasks as $task)
                    <h4>{{ $task->name }}</h4>
                    <p>{{ $task->text }}</p>
                    @if ($answers->has($task->id))
                        <div class="well well-sm">
                            {{ $answers[$task->id]->toHtml() }}
                        </div>
                    @else
                        <p>Нет ответа</p>
                    @endif
                @endforeach
            @endforeach
        @endforeach
    </div>
@endsection
