@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <a href="{{ route('section.assignment.index', [$section->id]) }}" class="btn-link">← К списку</a>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        @if($assignment->exists)
                            Изменение упражнения
                        @else
                            Добавление упражнения
                        @endif
                    </div>
                    <div class="panel-body">
                        <form class="form-horizontal"
                              role="form" method="POST"
                              action="{{ $assignment->exists ? route('section.assignment.update', [$section->id, $assignment->id]) : route('section.assignment.store', $section->id) }}">
                            {{ csrf_field() }}
                            {{ $assignment->exists ? method_field('PUT') : "" }}
                            <input type="hidden" name="section_id" value="{{ $section->id }}">

                            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                <label for="name" class="col-md-4 control-label">Название</label>

                                <div class="col-md-6">
                                    <input id="name" type="text" class="form-control" name="name" value="{{ old('name', $assignment->name) }}" required autofocus>

                                    @if ($errors->has('name'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                                <label for="description" class="col-md-4 control-label">Описание</label>
                                <div class="col-md-6">
                                    <textarea id="description" name="description" class="form-control">{{ old('description', $assignment->description) }}</textarea>
                                    @if ($errors->has('description'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('video') ? ' has-error' : '' }}">
                                <label for="video" class="col-md-4 control-label">Видео (ссылка)</label>
                                <div class="col-md-6">
                                    <input id="video" type="text" name="video" value="{{ old('video', $assignment->video) }}" class="form-control">
                                    @if ($errors->has('video'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('video') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            {{--<div class="form-group{{ $errors->has('position') ? ' has-error' : '' }}">
                                <label for="position" class="col-md-4 control-label">Порядок</label>

                                <div class="col-md-6">
                                    <input id="position" type="text" name="position" value="{{ old('position', $assignment->position) }}" class="form-control">
                                    @if ($errors->has('position'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('position') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>--}}

                            <div class="form-group{{ $errors->has('position') ? ' has-error' : '' }}">
                                <label for="position" class="col-md-4 control-label">Порядок</label>
                                <div class="col-md-6">
                                    {{ \Form::select('position', $select_positions, old('position', $assignment->position), ['class' => 'form-control']) }}
                                    @if ($errors->has('position'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('position') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-8 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        @if($assignment->exists)
                                            Измененить
                                        @else
                                            Добавить
                                        @endif
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection