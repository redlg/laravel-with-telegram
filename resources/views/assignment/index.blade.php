@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <a href="{{ route('section.index') }}" class="btn-link">← К разделам</a>
                <div class="panel panel-default">
                    <div class="panel-heading">Упражнения раздела "{{ $section->name }}"</div>
                    {{--<div class="panel-body"></div>--}}
                    <table class="table table-striped table-hover">
                        <tr>
                            <th width="50">ID</th>
                            <th>Название</th>
                            <th>Порядок</th>
                            <th width="250"></th>
                        </tr>
                        @foreach($assignments as $item)
                            <tr id="assignment_{{ $item->id }}" class="{{ $item->active ? 'active' : '' }}">
                                <td>{{ $item->id }}</td>
                                <td><a href="{{ route('section.assignment.edit', [$section->id, $item->id]) }}">{{ $item->name }}</a></td>
                                <td>{{ $item->position }}</td>
                                <td>
                                    <a href="{{ route('section.assignment.task.index', [$section->id, $item->id]) }}"
                                       class="btn btn-xs btn-primary">задания</a>
                                    <a href="{{ route('section.assignment.edit', [$section->id, $item->id]) }}"
                                       class="btn btn-xs btn-primary">изменить</a>
                                    <a href="{{ route('section.assignment.destroy', [$section->id, $item->id]) }}"
                                       onclick="return postDelete(this, $('#assignment_{{ $item->id }}'))"
                                       class="btn btn-xs btn-danger">удалить</a>
                                </td>
                            </tr>
                        @endforeach
                        <tr>
                            <td colspan="4">
                                <a href="{{ route('section.assignment.create', [$section->id]) }}" class="btn btn-primary">Добавить новый</a>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <script>
        function postDelete(link, deleteItem) {
            var url = $(link).attr('href');
            if (confirm('Уверены, что хотите удалить?')) {
                $.ajax({
                    url: url,
                    method: 'post',
                    dataType: 'json',
                    data: {
                        _method: 'delete'
                    }
                }).done(function (data) {
                    if (data.error) {
                        toastr.error(data.error);
                    } else {
                        toastr.info(data.message || 'Удалено');
                        if (deleteItem) {
                            if (!deleteItem.jquery) {
                                deleteItem = $(deleteItem);
                            }
                            deleteItem.remove();
                        }
                    }
                }).fail(function(xhr, status, error) {
                    toastr.error(error);
                });
            }
            return false;
        }
    </script>
@endsection