<!DOCTYPE html>
<html>
<head>
    <!-- Google Analytics Content Experiment code -->
    <script>function utmx_section(){}function utmx(){}(function(){var
            k='148441338-8',d=document,l=d.location,c=d.cookie;
            if(l.search.indexOf('utm_expid='+k)>0)return;
            function f(n){if(c){var i=c.indexOf(n+'=');if(i>-1){var j=c.
            indexOf(';',i);return escape(c.substring(i+n.length+1,j<0?c.
                length:j))}}}var x=f('__utmx'),xx=f('__utmxx'),h=l.hash;d.write(
                '<sc'+'ript src="'+'http'+(l.protocol=='https:'?'s://ssl':
                    '://www')+'.google-analytics.com/ga_exp.js?'+'utmxkey='+k+
                '&utmx='+(x?x:'')+'&utmxx='+(xx?xx:'')+'&utmxtime='+new Date().
                valueOf()+(h?'&utmxhash='+escape(h.substr(1)):'')+
                '" type="text/javascript" charset="utf-8"><\/sc'+'ript>')})();
    </script><script>utmx('url','A/B');</script>
    <!-- End of Google Analytics Content Experiment code -->

    <meta charset="utf-8"/>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title>Lovelify | Построй осознанные отношения</title>
    <meta property="og:url" content="http://lovelify.ru/"/>
    <meta property="og:title" content="Lovelify | Построй осознанные отношения"/>
    <meta property="og:description" content="Удели 10 минут в день, чтобы понять себя лучше и построить осознанные отношения. Запуск 1 июня. Регистрация уже открыта!"/>
    <meta property="og:type" content="website"/>
    <meta property="og:image" content="http://lovelify.ru/assets/landing/images/lov_fb.jpg"/>
    <meta property="fb:app_id" content="257953674358265"/>
    <meta name="format-detection" content="telephone=no"/>
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon"/><!-- Assets -->
    <link rel="stylesheet" href="/assets/landing/css/tilda-grid-3.0.min.css" type="text/css" media="all"/>
    <link rel="stylesheet" href="/assets/landing/css/tilda-blocks-2.12.css?t=1492599028" type="text/css" media="all"/>
    <link rel="stylesheet" href="/assets/landing/css/tilda-zoom-1.0.min.css" type="text/css" media="all"/>
    <script src="/assets/landing/js/jquery-1.10.2.min.js"></script>
    <script src="/assets/landing/js/tilda-scripts-2.8.min.js"></script>
    <script src="/assets/landing/js/tilda-blocks-2.6.js?t=1492599028"></script>
    <script src="/assets/landing/js/tilda-zoom-1.0.min.js"></script>
    <script src="/assets/landing/js/tilda-forms-1.0.min.js"></script>
    <script src="/assets/landing/js/lazyload-1.3.min.js"></script>
</head>
<body class="t-body" style="margin:0;"><!--allrecords-->
<div id="allrecords" class="t-records" data-hook="blocks-collection-content-node" data-tilda-project-id="5978"
     data-tilda-page-id="746611" data-tilda-page-alias="beta-lovelify"
     data-tilda-formskey="30db6fc780025b507230ea94aab51193">
    <div id="rec17082708" class="r" style="background-color:#f5f0f0; " data-record-type="396" data-bg-color="#f5f0f0">
        <!-- T396 -->
        <style>#rec17082708 .t396__artboard {
                height: 800px;
                background-color: #ffffff;
            }

            #rec17082708 .t396__filter {
                height: 800px;
            }

            #rec17082708 .t396__carrier {
                height: 800px;
                background-position: center center;
                background-attachment: scroll;
                background-image: url('/assets/landing/images/tild6265-3636-4434-b634-363363303836__-__resize__20x__bgbase2.jpg');
                background-size: cover;
                background-repeat: no-repeat;
            }

            @media screen and (max-width: 1199px) {
                #rec17082708 .t396__artboard {
                    height: 600px;
                }

                #rec17082708 .t396__filter {
                    height: 600px;
                }

                #rec17082708 .t396__carrier {
                    height: 600px;
                    background-attachment: scroll;
                }
            }

            @media screen and (max-width: 959px) {
                #rec17082708 .t396__artboard {
                    height: 1400px;
                }

                #rec17082708 .t396__filter {
                    height: 1400px;
                }

                #rec17082708 .t396__carrier {
                    height: 1400px;
                }
            }

            @media screen and (max-width: 639px) {
                #rec17082708 .t396__artboard {
                    height: 1300px;
                }

                #rec17082708 .t396__filter {
                    height: 1300px;
                }

                #rec17082708 .t396__carrier {
                    height: 1300px;
                }
            }

            @media screen and (max-width: 479px) {
                #rec17082708 .t396__artboard {
                    height: 1300px;
                }

                #rec17082708 .t396__filter {
                    height: 1300px;
                }

                #rec17082708 .t396__carrier {
                    height: 1300px;
                }
            }

            #rec17082708 .tn-elem[data-elem-id="1474457880885"] {
                color: #000000;
                text-align: center;
                z-index: 14;
                top: 490px;
                left: calc(50% - 600px + 445px);
                width: 310px;
            }

            #rec17082708 .tn-elem[data-elem-id="1474457880885"] .tn-atom {
                color: #000000;
                font-size: 22px;
                font-family: 'Ubuntu';
                line-height: 1.3;
                font-weight: 500;
                background-position: center center;
                border-color: transparent;
                border-style: solid;
            }

            @media screen and (max-width: 1199px) {
                #rec17082708 .tn-elem[data-elem-id="1474457880885"] .tn-atom {
                    font-size: 20px;
                }
            }

            @media screen and (max-width: 959px) {
                #rec17082708 .tn-elem[data-elem-id="1474457880885"] .tn-atom {
                    font-size: 24px;
                }
            }

            @media screen and (max-width: 639px) {
                #rec17082708 .tn-elem[data-elem-id="1474457880885"] .tn-atom {
                    font-size: 20px;
                    line-height: 1.45;
                }
            }

            @media screen and (max-width: 479px) {
            }

            #rec17082708 .tn-elem[data-elem-id="1474457891812"] {
                color: #000000;
                text-align: center;
                z-index: 15;
                top: 490px;
                left: calc(50% - 600px + 870px);
                width: 310px;
            }

            #rec17082708 .tn-elem[data-elem-id="1474457891812"] .tn-atom {
                color: #000000;
                font-size: 22px;
                font-family: 'Ubuntu';
                line-height: 1.3;
                font-weight: 500;
                background-position: center center;
                border-color: transparent;
                border-style: solid;
            }

            @media screen and (max-width: 1199px) {
                #rec17082708 .tn-elem[data-elem-id="1474457891812"] .tn-atom {
                    font-size: 20px;
                }
            }

            @media screen and (max-width: 959px) {
                #rec17082708 .tn-elem[data-elem-id="1474457891812"] .tn-atom {
                    font-size: 24px;
                }
            }

            @media screen and (max-width: 639px) {
                #rec17082708 .tn-elem[data-elem-id="1474457891812"] .tn-atom {
                    font-size: 20px;
                    line-height: 1.45;
                }
            }

            @media screen and (max-width: 479px) {
            }

            #rec17082708 .tn-elem[data-elem-id="1477661705844"] {
                color: #eb4961;
                text-align: center;
                z-index: 16;
                top: 42px;
                left: calc(50% - 600px + 149px);
                width: 210px;
            }

            #rec17082708 .tn-elem[data-elem-id="1477661705844"] .tn-atom {
                color: #eb4961;
                font-size: 50px;
                font-family: 'Ubuntu';
                line-height: 1.55;
                font-weight: 600;
                background-position: center center;
                border-color: transparent;
                border-style: solid;
            }

            @media screen and (max-width: 1199px) {
                #rec17082708 .tn-elem[data-elem-id="1477661705844"] .tn-atom {
                    font-size: 40px;
                }
            }

            @media screen and (max-width: 959px) {
                #rec17082708 .tn-elem[data-elem-id="1477661705844"] .tn-atom {
                    font-size: 50px;
                }
            }

            @media screen and (max-width: 639px) {
                #rec17082708 .tn-elem[data-elem-id="1477661705844"] .tn-atom {
                    font-size: 40px;
                }
            }

            @media screen and (max-width: 479px) {
                #rec17082708 .tn-elem[data-elem-id="1477661705844"] .tn-atom {
                    font-size: 35px;
                }
            }

            #rec17082708 .tn-elem[data-elem-id="1477661731338"] {
                color: #292224;
                z-index: 17;
                top: 63px;
                left: calc(50% - 600px + 398px);
                width: 620px;
            }

            #rec17082708 .tn-elem[data-elem-id="1477661731338"] .tn-atom {
                color: #292224;
                font-size: 34px;
                font-family: 'Ubuntu';
                line-height: 1.2;
                font-weight: 400;
                background-position: center center;
                border-color: transparent;
                border-style: solid;
            }

            @media screen and (max-width: 1199px) {
                #rec17082708 .tn-elem[data-elem-id="1477661731338"] .tn-atom {
                    font-size: 31px;
                }
            }

            @media screen and (max-width: 959px) {
                #rec17082708 .tn-elem[data-elem-id="1477661731338"] {
                    text-align: center;
                }

                #rec17082708 .tn-elem[data-elem-id="1477661731338"] .tn-atom {
                    font-size: 30px;
                }
            }

            @media screen and (max-width: 639px) {
                #rec17082708 .tn-elem[data-elem-id="1477661731338"] .tn-atom {
                    font-size: 24px;
                    line-height: 1.45;
                }
            }

            @media screen and (max-width: 479px) {
                #rec17082708 .tn-elem[data-elem-id="1477661731338"] .tn-atom {
                    font-size: 25px;
                    line-height: 1.2;
                }
            }

            #rec17082708 .tn-elem[data-elem-id="1488345874707"] {
                z-index: 18;
                top: 150px;
                left: calc(50% - 600px + 4px);
                width: 350px;
            }

            #rec17082708 .tn-elem[data-elem-id="1488345874707"] .tn-atom {
                background-position: center center;
                border-color: transparent;
                border-style: solid;
            }

            @media screen and (max-width: 1199px) {
            }

            @media screen and (max-width: 959px) {
            }

            @media screen and (max-width: 639px) {
            }

            @media screen and (max-width: 479px) {
            }

            #rec17082708 .tn-elem[data-elem-id="1488349470030"] {
                z-index: 19;
                top: 150px;
                left: calc(50% - 600px + 425px);
                width: 350px;
            }

            #rec17082708 .tn-elem[data-elem-id="1488349470030"] .tn-atom {
                background-position: center center;
                border-color: transparent;
                border-style: solid;
            }

            @media screen and (max-width: 1199px) {
            }

            @media screen and (max-width: 959px) {
            }

            @media screen and (max-width: 639px) {
            }

            @media screen and (max-width: 479px) {
            }

            #rec17082708 .tn-elem[data-elem-id="1488349493503"] {
                z-index: 20;
                top: 144px;
                left: calc(50% - 600px + 850px);
                width: 350px;
            }

            #rec17082708 .tn-elem[data-elem-id="1488349493503"] .tn-atom {
                background-position: center center;
                border-color: transparent;
                border-style: solid;
            }

            @media screen and (max-width: 1199px) {
            }

            @media screen and (max-width: 959px) {
            }

            @media screen and (max-width: 639px) {
            }

            @media screen and (max-width: 479px) {
            }

            #rec17082708 .tn-elem[data-elem-id="1488783812826"] {
                color: #eb4961;
                text-align: center;
                z-index: 21;
                top: 634px;
                left: calc(50% - 600px + 400px);
                width: 400px;
                height: 80px;
            }

            #rec17082708 .tn-elem[data-elem-id="1488783812826"] .tn-atom {
                color: #eb4961;
                font-size: 20px;
                font-family: 'Ubuntu';
                line-height: 1.55;
                font-weight: 600;
                border-width: 3px;
                border-radius: 40px;
                background-position: center center;
                border-color: #eb4961;
                border-style: solid;
                transition: background-color 0.3s ease-in-out, color 0.3s ease-in-out, border-color 0.3s ease-in-out;
            }

            #rec17082708 .tn-elem[data-elem-id="1488783812826"] .tn-atom:hover {
                background-color: #eb4961;
                color: #ffffff;
            }

            @media screen and (max-width: 1199px) {
            }

            @media screen and (max-width: 959px) {
            }

            @media screen and (max-width: 639px) {
            }

            @media screen and (max-width: 479px) {
            }

            #rec17082708 .tn-elem[data-elem-id="1490226170138"] {
                z-index: 22;
                top: 75px;
                left: calc(50% - 600px + 365px);
                width: 21px;
            }

            #rec17082708 .tn-elem[data-elem-id="1490226170138"] .tn-atom {
                background-position: center center;
                border-color: transparent;
                border-style: solid;
            }

            @media screen and (max-width: 1199px) {
            }

            @media screen and (max-width: 959px) {
            }

            @media screen and (max-width: 639px) {
            }

            @media screen and (max-width: 479px) {
            }

            #rec17082708 .tn-elem[data-elem-id="1490260414321"] {
                color: #000000;
                text-align: center;
                z-index: 23;
                top: 490px;
                left: calc(50% - 600px + 26px);
                width: 310px;
            }

            #rec17082708 .tn-elem[data-elem-id="1490260414321"] .tn-atom {
                color: #000000;
                font-size: 22px;
                font-family: 'Ubuntu';
                line-height: 1.3;
                font-weight: 500;
                background-position: center center;
                border-color: transparent;
                border-style: solid;
            }

            @media screen and (max-width: 1199px) {
                #rec17082708 .tn-elem[data-elem-id="1490260414321"] .tn-atom {
                    font-size: 20px;
                }
            }

            @media screen and (max-width: 959px) {
                #rec17082708 .tn-elem[data-elem-id="1490260414321"] .tn-atom {
                    font-size: 24px;
                }
            }

            @media screen and (max-width: 639px) {
                #rec17082708 .tn-elem[data-elem-id="1490260414321"] .tn-atom {
                    font-size: 20px;
                    line-height: 1.45;
                }
            }

            @media screen and (max-width: 479px) {
            }
        </style>
        <div class='t396'>
            <div class="t396__artboard" data-artboard-recid="17082708" data-artboard-height="800"
                 data-artboard-height-res-960="600" data-artboard-height-res-640="1400"
                 data-artboard-height-res-480="1300" data-artboard-height-res-320="1300" data-artboard-height_vh=""
                 data-artboard-valign="center">
                <div class="t396__carrier t-bgimg" data-artboard-recid="17082708"
                     data-original="/assets/landing/images/tild6265-3636-4434-b634-363363303836__bgbase2.jpg"></div>
                <div class="t396__filter" data-artboard-recid="17082708"></div>
                <div class='t396__elem tn-elem tn-elem__170827081474457880885' data-elem-id='1474457880885'
                     data-elem-type='text' data-field-top-value="490" data-field-top-res-960-value="358"
                     data-field-top-res-640-value="791" data-field-top-res-480-value="693"
                     data-field-top-res-320-value="711" data-field-left-value="445" data-field-left-res-960-value="330"
                     data-field-left-res-640-value="170" data-field-left-res-480-value="90"
                     data-field-left-res-320-value="30" data-field-width-value="310"
                     data-field-width-res-960-value="300" data-field-width-res-640-value="300"
                     data-field-width-res-480-value="300" data-field-width-res-320-value="260"
                     data-field-axisy-value="top" data-field-axisx-value="left" data-field-container-value="grid"
                     data-field-topunits-value="" data-field-leftunits-value="" data-field-heightunits-value=""
                     data-field-widthunits-value="">
                    <div class='tn-atom' field='tn_text_1474457880885'>Какие люди и ситуации в прошлом влияют на твои
                        отношения
                    </div>
                </div>
                <div class='t396__elem tn-elem tn-elem__170827081474457891812' data-elem-id='1474457891812'
                     data-elem-type='text' data-field-top-value="490" data-field-top-res-960-value="358"
                     data-field-top-res-640-value="1197" data-field-top-res-480-value="1056"
                     data-field-top-res-320-value="1079" data-field-left-value="870" data-field-left-res-960-value="698"
                     data-field-left-res-640-value="170" data-field-left-res-480-value="90"
                     data-field-left-res-320-value="30" data-field-width-value="310"
                     data-field-width-res-960-value="250" data-field-width-res-640-value="300"
                     data-field-width-res-480-value="300" data-field-width-res-320-value="260"
                     data-field-axisy-value="top" data-field-axisx-value="left" data-field-container-value="grid"
                     data-field-topunits-value="" data-field-leftunits-value="" data-field-heightunits-value=""
                     data-field-widthunits-value="">
                    <div class='tn-atom' field='tn_text_1474457891812'>Какие отношения<br>тебе нужны</div>
                </div>
                <div class='t396__elem tn-elem tn-elem__170827081477661705844' data-elem-id='1477661705844'
                     data-elem-type='text' data-field-top-value="42" data-field-top-res-960-value="33"
                     data-field-top-res-640-value="25" data-field-top-res-480-value="27"
                     data-field-top-res-320-value="13" data-field-left-value="149" data-field-left-res-960-value="91"
                     data-field-left-res-640-value="200" data-field-left-res-480-value="130"
                     data-field-left-res-320-value="75" data-field-width-value="210"
                     data-field-width-res-960-value="170" data-field-width-res-480-value="190"
                     data-field-width-res-320-value="140" data-field-axisy-value="top" data-field-axisx-value="left"
                     data-field-container-value="grid" data-field-topunits-value="" data-field-leftunits-value=""
                     data-field-heightunits-value="" data-field-widthunits-value="">
                    <div class='tn-atom' field='tn_text_1477661705844'>Lovelify</div>
                </div>
                <div class='t396__elem tn-elem tn-elem__170827081477661731338' data-elem-id='1477661731338'
                     data-elem-type='text' data-field-top-value="63" data-field-top-res-960-value="47"
                     data-field-top-res-640-value="105" data-field-top-res-480-value="87"
                     data-field-top-res-320-value="65" data-field-left-value="398" data-field-left-res-960-value="299"
                     data-field-left-res-640-value="30" data-field-left-res-480-value="25"
                     data-field-left-res-320-value="10" data-field-width-value="620"
                     data-field-width-res-960-value="560" data-field-width-res-640-value="580"
                     data-field-width-res-480-value="430" data-field-width-res-320-value="300"
                     data-field-axisy-value="top" data-field-axisx-value="left" data-field-container-value="grid"
                     data-field-topunits-value="" data-field-leftunits-value="" data-field-heightunits-value=""
                     data-field-widthunits-value="">
                    <div class='tn-atom' field='tn_text_1477661731338'>Помощник в осознанных отношениях</div>
                </div>
                <div class='t396__elem tn-elem tn-elem__170827081488345874707' data-elem-id='1488345874707'
                     data-elem-type='image' data-field-top-value="150" data-field-top-res-960-value="110"
                     data-field-top-res-640-value="150" data-field-top-res-480-value="130"
                     data-field-top-res-320-value="150" data-field-left-value="4" data-field-left-res-960-value="11"
                     data-field-left-res-640-value="170" data-field-left-res-480-value="115"
                     data-field-left-res-320-value="35" data-field-width-value="350"
                     data-field-width-res-960-value="250" data-field-width-res-640-value="300"
                     data-field-width-res-480-value="250" data-field-axisy-value="top" data-field-axisx-value="left"
                     data-field-container-value="grid" data-field-topunits-value="" data-field-leftunits-value=""
                     data-field-heightunits-value="" data-field-widthunits-value="">
                    <div class='tn-atom'><img class='tn-atom__img t-img'
                                              data-original='/assets/landing/images/tild6361-6632-4534-a236-326331386238__1f.png'
                                              imgfield='tn_img_1488345874707'></div>
                </div>
                <div class='t396__elem tn-elem tn-elem__170827081488349470030' data-elem-id='1488349470030'
                     data-elem-type='image' data-field-top-value="150" data-field-top-res-960-value="110"
                     data-field-top-res-640-value="518" data-field-top-res-480-value="460"
                     data-field-top-res-320-value="480" data-field-left-value="425" data-field-left-res-960-value="355"
                     data-field-left-res-640-value="170" data-field-left-res-480-value="115"
                     data-field-left-res-320-value="35" data-field-width-value="350"
                     data-field-width-res-960-value="250" data-field-width-res-640-value="300"
                     data-field-width-res-480-value="250" data-field-axisy-value="top" data-field-axisx-value="left"
                     data-field-container-value="grid" data-field-topunits-value="" data-field-leftunits-value=""
                     data-field-heightunits-value="" data-field-widthunits-value="">
                    <div class='tn-atom'><img class='tn-atom__img t-img'
                                              data-original='/assets/landing/images/tild3963-3466-4264-a539-363136383661__2f.png'
                                              imgfield='tn_img_1488349470030'></div>
                </div>
                <div class='t396__elem tn-elem tn-elem__170827081488349493503' data-elem-id='1488349493503'
                     data-elem-type='image' data-field-top-value="144" data-field-top-res-960-value="110"
                     data-field-top-res-640-value="907" data-field-top-res-480-value="810"
                     data-field-top-res-320-value="837" data-field-left-value="850" data-field-left-res-960-value="698"
                     data-field-left-res-640-value="170" data-field-left-res-480-value="115"
                     data-field-left-res-320-value="35" data-field-width-value="350"
                     data-field-width-res-960-value="250" data-field-width-res-640-value="300"
                     data-field-width-res-480-value="250" data-field-axisy-value="top" data-field-axisx-value="left"
                     data-field-container-value="grid" data-field-topunits-value="" data-field-leftunits-value=""
                     data-field-heightunits-value="" data-field-widthunits-value="">
                    <div class='tn-atom'><img class='tn-atom__img t-img'
                                              data-original='/assets/landing/images/tild6262-3032-4438-a437-653565653339__3f.png'
                                              imgfield='tn_img_1488349493503'></div>
                </div>
                <div class='t396__elem tn-elem tn-elem__170827081488783812826' data-elem-id='1488783812826'
                     data-elem-type='button' data-field-top-value="634" data-field-top-res-960-value="474"
                     data-field-top-res-640-value="1294" data-field-top-res-480-value="1154"
                     data-field-top-res-320-value="1176" data-field-left-value="400" data-field-left-res-960-value="280"
                     data-field-left-res-640-value="120" data-field-left-res-480-value="40"
                     data-field-left-res-320-value="20" data-field-height-value="80" data-field-width-value="400"
                     data-field-width-res-320-value="280" data-field-axisy-value="top" data-field-axisx-value="left"
                     data-field-container-value="grid" data-field-topunits-value="" data-field-leftunits-value=""
                     data-field-heightunits-value="" data-field-widthunits-value=""><a class='tn-atom'
                                                                                       href="#rec17082718"> МНЕ
                        ИНТЕРЕСНО </a></div>
                <div class='t396__elem tn-elem tn-elem__170827081490226170138' data-elem-id='1490226170138'
                     data-elem-type='image' data-field-top-value="75" data-field-top-res-960-value="56"
                     data-field-top-res-640-value="57" data-field-top-res-480-value="50"
                     data-field-top-res-320-value="33" data-field-left-value="365" data-field-left-res-960-value="265"
                     data-field-left-res-640-value="419" data-field-left-res-480-value="321"
                     data-field-left-res-320-value="223" data-field-width-value="21" data-field-width-res-640-value="25"
                     data-field-width-res-320-value="21" data-field-axisy-value="top" data-field-axisx-value="left"
                     data-field-container-value="grid" data-field-topunits-value="" data-field-leftunits-value=""
                     data-field-heightunits-value="" data-field-widthunits-value="">
                    <div class='tn-atom'><img class='tn-atom__img t-img'
                                              data-original='/assets/landing/images/tild3532-3166-4966-a538-373832326337__heart.svg'
                                              imgfield='tn_img_1490226170138'></div>
                </div>
                <div class='t396__elem tn-elem tn-elem__170827081490260414321' data-elem-id='1490260414321'
                     data-elem-type='text' data-field-top-value="490" data-field-top-res-960-value="358"
                     data-field-top-res-640-value="431" data-field-top-res-480-value="363"
                     data-field-top-res-320-value="381" data-field-left-value="26" data-field-left-res-960-value="7"
                     data-field-left-res-640-value="170" data-field-left-res-480-value="90"
                     data-field-left-res-320-value="30" data-field-width-value="310"
                     data-field-width-res-960-value="260" data-field-width-res-640-value="300"
                     data-field-width-res-480-value="300" data-field-width-res-320-value="260"
                     data-field-axisy-value="top" data-field-axisx-value="left" data-field-container-value="grid"
                     data-field-topunits-value="" data-field-leftunits-value="" data-field-heightunits-value=""
                     data-field-widthunits-value="">
                    <div class='tn-atom' field='tn_text_1490260414321'>Где ты сейчас.<br>Диагностика ситуации</div>
                </div>
            </div>
        </div>
        <script>$(document).ready(function () {
                t396_init('17082708');
            });</script><!-- /T396 -->
    </div>
    <div id="rec22327634" class="r" style=" " data-animationappear="off" data-record-type="275"><!-- t256 -->
        <!-- cover -->
        <div class="t-cover" id="recorddiv22327634" bgimgfield="img"
             style="height:70vh; background-image:url('/assets/landing/images/tild6337-3639-4365-b432-366363646233__-__resize__20x__Video.jpg');">
            <div class="t-cover__carrier" id="coverCarry22327634" data-content-cover-id="22327634"
                 data-content-cover-bg="/assets/landing/images/tild6337-3639-4365-b432-366363646233__video.jpg"
                 data-content-cover-height="70vh" data-content-cover-parallax="fixed"
                 style="background-image:url('');height:70vh; "></div>
            <div class="t-cover__filter"
                 style="height:70vh;background-color:#000;filter: alpha(opacity:0); KHTMLOpacity: 0.0; MozOpacity: 0.0; opacity: 0.0;"></div>
            <div class="t-container">
                <div class="t-width t-width_8 t256__mainblock">
                    <div class="t256 t-align_center">
                        <div class="t-cover__wrapper t-valign_middle" style="height:70vh;">
                            <div class="t256__wrapper" data-hook-content="covercontent"><a class="t256__play-link"
                                                                                           href="javascript:t256showvideo('22327634');">
                                    <div class="t256__play-icon ">
                                        <svg width="70px" height="70px" viewBox="0 0 60 60">
                                            <g stroke="none" stroke-width="1" fill="" fill-rule="evenodd">
                                                <g transform="translate(-691.000000, -3514.000000)" fill="#231f20">
                                                    <path d="M721,3574 C737.568542,3574 751,3560.56854 751,3544 C751,3527.43146 737.568542,3514 721,3514 C704.431458,3514 691,3527.43146 691,3544 C691,3560.56854 704.431458,3574 721,3574 Z M715,3534 L732,3544.5 L715,3555 L715,3534 Z"></path>
                                                </g>
                                            </g>
                                        </svg>
                                    </div>
                                </a>
                                <div class="t256__title t-title t-title_sm " style="color:#231f20;font-size:42px;"
                                     field="title"><strong>О программе</strong><br/></div>
                                <div class="t256__descr t-descr t-descr_xs " style="" field="descr"></div>
                                <span class="space"></span></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="t256__video-container t256__hidden" data-content-popup-video-url-youtube="8oRMmRsBLgw">
                <div class="t256__video-carier"></div>
                <a class="t256__video-bg" href="javascript:t256hidevideo('22327634');"
                   style="background:#222226; opacity:0.80;"></a></div>
        </div>
        <script type="text/javascript">$(document).ready(function () {
                $("#rec22327634").attr('data-animationappear', 'off');
                $("#rec22327634").css('opacity', '1');
            });</script>
    </div>
    <div id="rec23072520" class="r" style="background-color:#f5f0f0; " data-record-type="396" data-bg-color="#f5f0f0">
        <!-- T396 -->
        <style>#rec23072520 .t396__artboard {
                height: 200px;
                background-color: #ffffff;
            }

            #rec23072520 .t396__filter {
                height: 200px;
            }

            #rec23072520 .t396__carrier {
                height: 200px;
                background-position: center center;
                background-attachment: scroll;
                background-image: url('/assets/landing/images/tild6265-3636-4434-b634-363363303836__-__resize__20x__bgbase2.jpg');
                background-size: cover;
                background-repeat: no-repeat;
            }

            @media screen and (max-width: 1199px) {
                #rec23072520 .t396__artboard {
                    height: 200px;
                }

                #rec23072520 .t396__filter {
                    height: 200px;
                }

                #rec23072520 .t396__carrier {
                    height: 200px;
                    background-attachment: scroll;
                }
            }

            @media screen and (max-width: 959px) {
                #rec23072520 .t396__artboard {
                    height: 150px;
                }

                #rec23072520 .t396__filter {
                    height: 150px;
                }

                #rec23072520 .t396__carrier {
                    height: 150px;
                }
            }

            @media screen and (max-width: 639px) {
                #rec23072520 .t396__artboard {
                    height: 150px;
                }

                #rec23072520 .t396__filter {
                    height: 150px;
                }

                #rec23072520 .t396__carrier {
                    height: 150px;
                }
            }

            @media screen and (max-width: 479px) {
                #rec23072520 .t396__artboard {
                    height: 150px;
                }

                #rec23072520 .t396__filter {
                    height: 150px;
                }

                #rec23072520 .t396__carrier {
                    height: 150px;
                }
            }

            #rec23072520 .tn-elem[data-elem-id="1488783812826"] {
                color: #eb4961;
                text-align: center;
                z-index: 21;
                top: 60px;
                left: calc(50% - 600px + 400px);
                width: 400px;
                height: 80px;
            }

            #rec23072520 .tn-elem[data-elem-id="1488783812826"] .tn-atom {
                color: #eb4961;
                font-size: 25px;
                font-family: 'Ubuntu';
                line-height: 1.5;
                font-weight: 600;
                letter-spacing: 1px;
                border-width: 3px;
                border-radius: 40px;
                background-position: center center;
                border-color: #eb4961;
                border-style: solid;
                transition: background-color 0.3s ease-in-out, color 0.3s ease-in-out, border-color 0.3s ease-in-out;
            }

            #rec23072520 .tn-elem[data-elem-id="1488783812826"] .tn-atom:hover {
                background-color: #eb4961;
                color: #ffffff;
            }

            @media screen and (max-width: 1199px) {
            }

            @media screen and (max-width: 959px) {
            }

            @media screen and (max-width: 639px) {
            }

            @media screen and (max-width: 479px) {
            }
        </style>
        <div class='t396'>
            <div class="t396__artboard" data-artboard-recid="23072520" data-artboard-height="200"
                 data-artboard-height-res-960="200" data-artboard-height-res-640="150"
                 data-artboard-height-res-480="150" data-artboard-height-res-320="150" data-artboard-height_vh=""
                 data-artboard-valign="center">
                <div class="t396__carrier t-bgimg" data-artboard-recid="23072520"
                     data-original="/assets/landing/images/tild6265-3636-4434-b634-363363303836__bgbase2.jpg"></div>
                <div class="t396__filter" data-artboard-recid="23072520"></div>
                <div class='t396__elem tn-elem tn-elem__230725201488783812826' data-elem-id='1488783812826'
                     data-elem-type='button' data-field-top-value="60" data-field-top-res-960-value="60"
                     data-field-top-res-640-value="45" data-field-top-res-480-value="45"
                     data-field-top-res-320-value="45" data-field-left-value="400" data-field-left-res-960-value="280"
                     data-field-left-res-640-value="120" data-field-left-res-480-value="65"
                     data-field-left-res-320-value="35" data-field-height-value="80"
                     data-field-height-res-640-value="60" data-field-width-value="400"
                     data-field-width-res-480-value="350" data-field-width-res-320-value="250"
                     data-field-axisy-value="top" data-field-axisx-value="left" data-field-container-value="grid"
                     data-field-topunits-value="" data-field-leftunits-value="" data-field-heightunits-value=""
                     data-field-widthunits-value=""><a class='tn-atom' href="#rec17082718"> НАЧАТЬ </a></div>
            </div>
        </div>
        <script>$(document).ready(function () {
                t396_init('23072520');
            });</script><!-- /T396 -->
    </div>
    <div id="rec17082709" class="r" style=" " data-animationappear="off" data-record-type="408"><!-- t408 -->
        <!-- cover -->
        <div class="t-cover" id="recorddiv17082709" bgimgfield="img"
             style="height:65vh; background-image:url('/assets/landing/images/tild3661-6664-4630-b062-343732643162__-__resize__20x__bgbase2flipped.jpg');">
            <div class="t-cover__carrier" id="coverCarry17082709" data-content-cover-id="17082709"
                 data-content-cover-bg="/assets/landing/images/tild3661-6664-4630-b062-343732643162__bgbase2flipped.jpg"
                 data-content-cover-height="65vh" data-content-cover-parallax=""
                 style="background-image:url('');height:65vh;background-attachment:scroll; "></div>
            <div class="t-cover__filter"
                 style="height:65vh;background-image: -moz-linear-gradient(top, rgba(255,255,255,0.10), rgba(255,255,255,0.0));background-image: -webkit-linear-gradient(top, rgba(255,255,255,0.10), rgba(255,255,255,0.0));background-image: -o-linear-gradient(top, rgba(255,255,255,0.10), rgba(255,255,255,0.0));background-image: -ms-linear-gradient(top, rgba(255,255,255,0.10), rgba(255,255,255,0.0));background-image: linear-gradient(top, rgba(255,255,255,0.10), rgba(255,255,255,0.0));filter: progid:DXImageTransform.Microsoft.gradient(startColorStr='#e5ffffff', endColorstr='#feffffff');"></div>
            <div class="t408">
                <div class="t-container">
                    <div class="t-cover__wrapper t-valign_middle" style="height:65vh;">
                        <div class="t408__wrapper" data-hook-content="covercontent">
                            <div class="t408__textwrapper t-width t-width_8">
                                <div class="t408__title t-title t-title_md" field="title">
                                    <div style="font-size:42px;line-height:52px;color:#231f20;" data-customstyle="yes">К
                                        нам обычно приходят<br/>в одной их трех ситуаций
                                    </div>
                                </div>
                            </div>
                            <div class="t408__blockswrapper">
                                <div class="t408__col t-col t-col_4">
                                    <div class="t408__wrapper"><img data-tu-max-width="400" data-tu-max-height="400"
                                                                    class="t408__img t408__img_circle"
                                                                    src="/assets/landing/images/tild3833-6333-4136-a363-313836343364__1_.png"
                                                                    style="width: 150px; height: 150px;"
                                                                    imgfield="img1"/>
                                        <div class="t408__title t-name t-name_md" field="title1">
                                            <div style="font-size:22px;color:#231f20;" data-customstyle="yes"><span
                                                    style="font-weight: 500;">Грущу один и хочу<br/>это поменять</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="t408__col t-col t-col_4">
                                    <div class="t408__wrapper"><img data-tu-max-width="400" data-tu-max-height="400"
                                                                    class="t408__img t408__img_circle"
                                                                    src="/assets/landing/images/tild3932-3831-4138-b037-633536396433__2_.png"
                                                                    style="width: 150px; height: 150px;"
                                                                    imgfield="img2"/>
                                        <div class="t408__title t-name t-name_md" field="title2">
                                            <div style="font-size:22px;color:#231f20;" data-customstyle="yes"><span
                                                    style="font-weight: 500;">Думаю, расставаться<br/>или нет</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="t408__col t-col t-col_4">
                                    <div class="t408__wrapper"><img data-tu-max-width="400" data-tu-max-height="400"
                                                                    class="t408__img t408__img_circle"
                                                                    src="/assets/landing/images/tild3031-3834-4838-a532-396630636532__3_.png"
                                                                    style="width: 150px; height: 150px;"
                                                                    imgfield="img3"/>
                                        <div class="t408__title t-name t-name_md" field="title3">
                                            <div style="font-size:22px;color:#231f20;" data-customstyle="yes"><span
                                                    style="font-weight: 500;">Мои отношения не перерастают<br/>во что-то серьезное</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="rec17082710" class="r" style=" " data-animationappear="off" data-record-type="391"><!-- T391 -->
        <!-- cover -->
        <div class="t-cover" id="recorddiv17082710" bgimgfield="img"
             style="height:60vh; background-image:url('/assets/landing/images/tild3234-3835-4362-b035-303235366632__-__resize__20x__bgbase.jpg');">
            <div class="t-cover__carrier" id="coverCarry17082710" data-content-cover-id="17082710"
                 data-content-cover-bg="/assets/landing/images/tild3234-3835-4362-b035-303235366632__bgbase.jpg"
                 data-content-cover-height="60vh" data-content-cover-parallax="fixed"
                 style="background-image:url('');height:60vh; "></div>
            <div class="t-cover__filter"
                 style="height:60vh;background-color:#000;filter: alpha(opacity:0); KHTMLOpacity: 0.0; MozOpacity: 0.0; opacity: 0.0;"></div>
            <div class="t391">
                <div class="t-container">
                    <div class="t391__firstcol t-col t-col_1 ">
                        <div class="t-cover__wrapper t-valign_middle" style="height: 60vh;"></div>
                    </div>
                    <div class="t391__secondcol t-col t-col_8 t-prefix_1">
                        <div class="t-cover__wrapper t-valign_middle" data-hook-content="covercontent"
                             style="height: 60vh;">
                            <div class="t391__textwrapper t-align_center">
                                <div class="t391__title t-title t-title_xs" field="title" style="font-size:46px;">Наш
                                    подход
                                </div>
                                <div class="t391__text t-descr t-descr_sm" field="text"
                                     style="font-size:26px;line-height:1.6;">Программа основана на достижениях
                                    современной психологии и объединяет самые эффективные методики создания и улучшения
                                    отношений. Мы считаем, что человек наиболее полно реализует себя в паре, и помогаем
                                    быстрее прийти к такой самореализации. Задания программы объединены в 3 раздела:
                                    <br/></div>
                                <div class="t391__buttonwrapper"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script type="text/javascript">$(document).ready(function () {
                t391_checkSize('17082710');
            });
            $(window).load(function () {
                t391_checkSize('17082710');
            });
            $(window).resize(function () {
                t391_checkSize('17082710');
            });</script>
    </div>
    <div id="rec17082711" class="r" style=" " data-animationappear="off" data-record-type="391"><!-- T391 -->
        <!-- cover -->
        <div class="t-cover" id="recorddiv17082711" bgimgfield="img"
             style="height:70vh; background-image:url('/assets/landing/images/tild3037-3630-4564-a163-643963316666__-__resize__20x__bgfirst.jpg');">
            <div class="t-cover__carrier" id="coverCarry17082711" data-content-cover-id="17082711"
                 data-content-cover-bg="/assets/landing/images/tild3037-3630-4564-a163-643963316666__bgfirst.jpg"
                 data-content-cover-height="70vh" data-content-cover-parallax="fixed"
                 style="background-image:url('');height:70vh; "></div>
            <div class="t-cover__filter"
                 style="height:70vh;background-color:#000;filter: alpha(opacity:0); KHTMLOpacity: 0.0; MozOpacity: 0.0; opacity: 0.0;"></div>
            <div class="t391">
                <div class="t-container">
                    <div class="t391__firstcol t-col t-col_6 ">
                        <div class="t-cover__wrapper t-valign_middle" style="height: 70vh;"></div>
                    </div>
                    <div class="t391__secondcol t-col t-col_6 ">
                        <div class="t-cover__wrapper t-valign_middle" data-hook-content="covercontent"
                             style="height: 70vh;">
                            <div class="t391__textwrapper t-align_left">
                                <div class="t391__title t-title t-title_xs" field="title" style="font-size:46px;">Моя
                                    ситуация. Диагностика
                                </div>
                                <div class="t391__text t-descr t-descr_sm" field="text"
                                     style="font-size:26px;line-height:1.6;">Определяем, почему тебе некомфортно в
                                    отношениях или без них, диагностируем ситуацию в сравнении с
                                    психологически здоровыми отношениями.
                                </div>
                                <div class="t391__buttonwrapper"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script type="text/javascript">$(document).ready(function () {
                t391_checkSize('17082711');
            });
            $(window).load(function () {
                t391_checkSize('17082711');
            });
            $(window).resize(function () {
                t391_checkSize('17082711');
            });</script>
    </div>
    <div id="rec17082712" class="r" style=" " data-animationappear="off" data-record-type="391"><!-- T391 -->
        <!-- cover -->
        <div class="t-cover" id="recorddiv17082712" bgimgfield="img"
             style="height:70vh; background-image:url('/assets/landing/images/tild3363-3738-4662-b831-373535393533__-__resize__20x__bgsecond.jpg');">
            <div class="t-cover__carrier" id="coverCarry17082712" data-content-cover-id="17082712"
                 data-content-cover-bg="/assets/landing/images/tild3363-3738-4662-b831-373535393533__bgsecond.jpg"
                 data-content-cover-height="70vh" data-content-cover-parallax="fixed"
                 style="background-image:url('');height:70vh; "></div>
            <div class="t-cover__filter"
                 style="height:70vh;background-color:#000;filter: alpha(opacity:0); KHTMLOpacity: 0.0; MozOpacity: 0.0; opacity: 0.0;"></div>
            <div class="t391">
                <div class="t-container">
                    <div class="t391__firstcol t-col t-col_6 ">
                        <div class="t-cover__wrapper t-valign_middle" style="height: 70vh;"></div>
                    </div>
                    <div class="t391__secondcol t-col t-col_6 ">
                        <div class="t-cover__wrapper t-valign_middle" data-hook-content="covercontent"
                             style="height: 70vh;">
                            <div class="t391__textwrapper t-align_left">
                                <div class="t391__title t-title t-title_xs" field="title" style="font-size:46px;">Я и
                                    отношения.<br/>Терапия
                                </div>
                                <div class="t391__text t-descr t-descr_sm" field="text"
                                     style="font-size:26px;line-height:1.6;">Определяем какие паттерны, ситуации, травмы
                                    из прошлого привели тебя к текущим проблемам и корректируем их.
                                </div>
                                <div class="t391__buttonwrapper"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script type="text/javascript">$(document).ready(function () {
                t391_checkSize('17082712');
            });
            $(window).load(function () {
                t391_checkSize('17082712');
            });
            $(window).resize(function () {
                t391_checkSize('17082712');
            });</script>
    </div>
    <div id="rec17082713" class="r" style=" " data-animationappear="off" data-record-type="391"><!-- T391 -->
        <!-- cover -->
        <div class="t-cover" id="recorddiv17082713" bgimgfield="img"
             style="height:70vh; background-image:url('/assets/landing/images/tild3836-3736-4030-a663-316438313963__-__resize__20x__bgthird.jpg');">
            <div class="t-cover__carrier" id="coverCarry17082713" data-content-cover-id="17082713"
                 data-content-cover-bg="/assets/landing/images/tild3836-3736-4030-a663-316438313963__bgthird.jpg"
                 data-content-cover-height="70vh" data-content-cover-parallax="fixed"
                 style="background-image:url('');height:70vh; "></div>
            <div class="t-cover__filter"
                 style="height:70vh;background-color:#000;filter: alpha(opacity:0); KHTMLOpacity: 0.0; MozOpacity: 0.0; opacity: 0.0;"></div>
            <div class="t391">
                <div class="t-container">
                    <div class="t391__firstcol t-col t-col_6 ">
                        <div class="t-cover__wrapper t-valign_middle" style="height: 70vh;"></div>
                    </div>
                    <div class="t391__secondcol t-col t-col_6 ">
                        <div class="t-cover__wrapper t-valign_middle" data-hook-content="covercontent"
                             style="height: 70vh;">
                            <div class="t391__textwrapper t-align_left">
                                <div class="t391__title t-title t-title_xs" field="title" style="font-size:46px;">План
                                    улучшений. Коучинг
                                </div>
                                <div class="t391__text t-descr t-descr_sm" field="text"
                                     style="font-size:26px;line-height:1.6;">Помогаем принять решение о сохранении
                                    (построении) отношений, составляем план улучшения отношений.
                                </div>
                                <div class="t391__buttonwrapper"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script type="text/javascript">$(document).ready(function () {
                t391_checkSize('17082713');
            });
            $(window).load(function () {
                t391_checkSize('17082713');
            });
            $(window).resize(function () {
                t391_checkSize('17082713');
            });</script>
    </div>
    <div id="rec17082715" class="r" style="padding-top:150px;padding-bottom:150px;background-color:#ffffff; "
         data-record-type="490" data-bg-color="#ffffff"><!-- t490 -->
        <div class="t490">
            <div class="t-section__container t-container">
                <div class="t-col t-col_12">
                    <div class="t-section__topwrapper t-align_center">
                        <div class="t-section__title t-title t-title_xs " field="btitle">
                            <div style="font-size:46px;" data-customstyle="yes">Формат</div>
                        </div>
                        <div class="t-section__descr t-descr t-descr_xl t-margin_auto " field="bdescr">
                            <div style="font-size:26px;line-height:40px;" data-customstyle="yes">Программа объединяет 20
                                заданий на понимание своих отношений, их создание<br/> или улучшение.<br/></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="t-container">
                <div class="t490__col t-col t-col_4 t-align_center t-item ">
                    <div class="t490__bgimg t-margin_auto t-bgimg" bgimgfield="li_img__1476968690512"
                         data-original="/assets/landing/images/tild6430-3263-4534-b566-616566643734__1.png"
                         style="width: 150px; height: 150px; background-image: url('/assets/landing/images/tild6430-3263-4534-b566-616566643734__-__resize__20x__1.png');"></div>
                    <div class="t490__wrappercenter ">
                        <div class="t-heading t-heading_md" style="color:#2c2d34;font-size:20px;font-weight:400;"
                             field="li_title__1476968690512">Каждое упражнение занимает<br/>20 минут в день
                        </div>
                    </div>
                </div>
                <div class="t490__col t-col t-col_4 t-align_center t-item ">
                    <div class="t490__bgimg t-margin_auto t-bgimg" bgimgfield="li_img__1476968700508"
                         data-original="/assets/landing/images/tild3933-3261-4137-b837-383564613434__2.png"
                         style="width: 150px; height: 150px; background-image: url('/assets/landing/images/tild3933-3261-4137-b837-383564613434__-__resize__20x__2.png');"></div>
                    <div class="t490__wrappercenter ">
                        <div class="t-heading t-heading_md" style="color:#2c2d34;font-size:20px;font-weight:400;"
                             field="li_title__1476968700508">Упражнения вы выполняете самостоятельно
                        </div>
                    </div>
                </div>
                <div class="t490__col t-col t-col_4 t-align_center t-item ">
                    <div class="t490__bgimg t-margin_auto t-bgimg" bgimgfield="li_img__1476968722790"
                         data-original="/assets/landing/images/tild3437-6138-4539-a632-363335376139__3.png"
                         style="width: 150px; height: 150px; background-image: url('/assets/landing/images/tild3437-6138-4539-a632-363335376139__-__resize__20x__3.png');"></div>
                    <div class="t490__wrappercenter ">
                        <div class="t-heading t-heading_md" style="color:#2c2d34;font-size:20px;font-weight:400;"
                             field="li_title__1476968722790">Куратор сопровождает вас и дает обратную связь по
                            каждому<br/>из 3х разделов программы
                        </div>
                    </div>
                </div>
                <div class="t-clear t490__separator" style="margin-bottom:30px;"></div>
                <div class="t490__col t-col t-col_4 t-align_center t-item ">
                    <div class="t490__bgimg t-margin_auto t-bgimg" bgimgfield="li_img__1488786615681"
                         data-original="/assets/landing/images/tild6262-3733-4435-b637-616534393033__4.png"
                         style="width: 150px; height: 150px; background-image: url('/assets/landing/images/tild6262-3733-4435-b637-616534393033__-__resize__20x__4.png');"></div>
                    <div class="t490__wrappercenter ">
                        <div class="t-heading t-heading_md" style="color:#2c2d34;font-size:20px;font-weight:400;"
                             field="li_title__1488786615681">Задания вы получаете в Telegram на мобильный телефон или
                            компьютер, там же пишете выводы
                        </div>
                    </div>
                </div>
                <div class="t490__col t-col t-col_4 t-align_center t-item ">
                    <div class="t490__bgimg t-margin_auto t-bgimg" bgimgfield="li_img__1488786675510"
                         data-original="/assets/landing/images/tild6634-3338-4337-a331-643766343938__5.png"
                         style="width: 150px; height: 150px; background-image: url('/assets/landing/images/tild6634-3338-4337-a331-643766343938__-__resize__20x__5.png');"></div>
                    <div class="t490__wrappercenter ">
                        <div class="t-heading t-heading_md" style="color:#2c2d34;font-size:20px;font-weight:400;"
                             field="li_title__1488786675510">Ваши выводы сохранятся в личном кабинете на сайте
                            программы.<br/></div>
                    </div>
                </div>
                <div class="t490__col t-col t-col_4 t-align_center t-item ">
                    <div class="t490__bgimg t-margin_auto t-bgimg" bgimgfield="li_img__1488786816517"
                         data-original="/assets/landing/images/tild6631-3963-4736-a563-666434656534__6.png"
                         style="width: 150px; height: 150px; background-image: url('/assets/landing/images/tild6631-3963-4736-a563-666434656534__-__resize__20x__6.png');"></div>
                    <div class="t490__wrappercenter ">
                        <div class="t-heading t-heading_md" style="color:#2c2d34;font-size:20px;font-weight:400;"
                             field="li_title__1488786816517">Использовать Telegram просто, мы всегда подскажем и
                            поможем!
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="rec17084639" class="r" style=" " data-record-type="270">
        <div class="t270"></div>
        <script type="text/javascript"> $(document).ready(function () {
                setTimeout(function () {
                    var $root = $('html, body');
                    $('a[href*=#]:not([href=#],.carousel-control,.t-carousel__control,[href^=#price],[href^=#popup])').click(function () {
                        var target = $(this.hash);
                        if (target.length == 0) {
                            target = $('a[name="' + this.hash.substr(1) + '"]');
                        }
                        if (target.length == 0) {
                            return true;
                        }
                        $root.animate({scrollTop: target.offset().top + 3}, 500);
                        return false;
                    });
                }, 500);
            }); </script>
    </div>
    <div id="rec17082718" class="r" style="padding-top:0px; " data-record-type="587"><!-- t587 --><!-- cover -->
        <div class="t-cover" id="recorddiv17082718" bgimgfield="img"
             style="height:60vh; background-image:url('/assets/landing/images/tild3039-6432-4236-b365-653838613534__-__resize__20x__bgbaseREG.jpg');">
            <div class="t-cover__carrier" id="coverCarry17082718" data-content-cover-id="17082718"
                 data-content-cover-bg="/assets/landing/images/tild3039-6432-4236-b365-653838613534__bgbasereg.jpg"
                 data-content-cover-height="60vh" data-content-cover-parallax="fixed"
                 style="background-image:url('');height:60vh; "></div>
            <div class="t-cover__filter"
                 style="height:60vh;background-image: -moz-linear-gradient(top, rgba(255,255,255,0.0), rgba(255,255,255,0.0));background-image: -webkit-linear-gradient(top, rgba(255,255,255,0.0), rgba(255,255,255,0.0));background-image: -o-linear-gradient(top, rgba(255,255,255,0.0), rgba(255,255,255,0.0));background-image: -ms-linear-gradient(top, rgba(255,255,255,0.0), rgba(255,255,255,0.0));background-image: linear-gradient(top, rgba(255,255,255,0.0), rgba(255,255,255,0.0));filter: progid:DXImageTransform.Microsoft.gradient(startColorStr='#feffffff', endColorstr='#feffffff');"></div>
            <div class="t587">
                <div class="t-container">
                    <div class="t-width t-width_10 t587__mainblock">
                        <div class="t-cover__wrapper t-valign_middle"
                             style="height:60vh; position: relative;z-index: 1;">
                            <div class="t587__mainwrapper" data-hook-content="covercontent">
                                <div class="t587__title t-title t-title_sm t-margin_auto"
                                     style="color:#231f20;font-size:42px;" field="title">Хочу скидку при запуске! <br/>
                                </div>
                                <div class="t587__descr t-descr t-descr_xl t-margin_auto"
                                     style="color:#231f20;font-size:20px;line-height:1.8;max-width:800px;"
                                     field="descr"><span style="color: rgb(235, 73, 97);">♦ </span> Проанализируй
                                    текущую ситуацию в отношениях<br/><span style="color: rgb(235, 73, 97);">♦ </span>
                                    Скорректируй стоп-факторы и проблемы на пути счастливых отношений<br/><span
                                        style="color: rgb(235, 73, 97);">♦ </span>Создай план улучшений и строй
                                    осознанные отношения
                                </div>
                                <form id="form17082718" name="form17082718" class="js-form-proccess" role="form"
                                      action='/chat' method='POST' data-formactiontype="1" data-inputbox=".t587__blockinput">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="price" value="{{ $price }}">
                                    <!-- NO ONE SERVICES CONNECTED -->
                                    <div class="t587__input-container t587__input-container_600px">
                                        <div class="t587__wrapper">
                                            <div class="t587__blockinput"><input type="text" name="email"
                                                                                 class="t587__input t-input js-tilda-rule t-input_bbonly"
                                                                                 value="" placeholder="Твоя эл. почта"
                                                                                 data-tilda-req="1"
                                                                                 data-tilda-rule="email"
                                                                                 style="color:#231f20; border:3px solid #eb4961; ">
                                            </div>
                                            <div class="t587__blockbutton">
                                                <button type="submit" class="t-submit"
                                                        style="color:#eb4961;border:3px solid #eb4961;background-color:#f4ebf2;border-radius:30px; -moz-border-radius:30px; -webkit-border-radius:30px;box-shadow: 0px 0px 20px 0px rgba(0, 0, 0, 0);">
                                                    УЗНАТЬ ПЕРВЫМ
                                                </button>
                                            </div>
                                        </div>
                                        <div class="">
                                            <div class="js-errorbox-all t587__blockinput-errorbox"
                                                 style="display:none;">
                                                <div class="t587__blockinput-errors-text t-descr t-descr_xs"><p
                                                        class="t587__blockinput-errors-item js-rule-error js-rule-error-all"></p>
                                                    <p class="t587__blockinput-errors-item js-rule-error js-rule-error-req">
                                                        Required field</p>
                                                    <p class="t587__blockinput-errors-item js-rule-error js-rule-error-email">
                                                        Please correct e-mail address</p>
                                                    <p class="t587__blockinput-errors-item js-rule-error js-rule-error-name">
                                                        Name Wrong. Correct please</p>
                                                    <p class="t587__blockinput-errors-item js-rule-error js-rule-error-phone">
                                                        Please correct phone number</p>
                                                    <p class="t587__blockinput-errors-item js-rule-error js-rule-error-string">
                                                        Please enter letter, number or punctuation symbols.</p></div>
                                            </div>
                                            <div class="t587__blockinput-success t-text t-text_md js-successbox"
                                                 style="display:none;">
                                                <div class="t587__success-icon">
                                                    <svg width="50px" height="50px" viewBox="0 0 50 50">
                                                        <g stroke="none" stroke-width="1" fill="none"
                                                           fill-rule="evenodd">
                                                            <g fill="#FFFFFF">
                                                                <path d="M25.0982353,49.2829412 C11.5294118,49.2829412 0.490588235,38.2435294 0.490588235,24.6752941 C0.490588235,11.1064706 11.53,0.0670588235 25.0982353,0.0670588235 C38.6664706,0.0670588235 49.7058824,11.1064706 49.7058824,24.6752941 C49.7058824,38.2441176 38.6664706,49.2829412 25.0982353,49.2829412 L25.0982353,49.2829412 Z M25.0982353,1.83176471 C12.5023529,1.83176471 2.25529412,12.0794118 2.25529412,24.6752941 C2.25529412,37.2705882 12.5023529,47.5182353 25.0982353,47.5182353 C37.6941176,47.5182353 47.9411765,37.2705882 47.9411765,24.6752941 C47.9411765,12.0794118 37.6941176,1.83176471 25.0982353,1.83176471 L25.0982353,1.83176471 Z"></path>
                                                                <path d="M22.8435294,30.5305882 L18.3958824,26.0829412 C18.0511765,25.7382353 18.0511765,25.18 18.3958824,24.8352941 C18.7405882,24.4905882 19.2988235,24.4905882 19.6435294,24.8352941 L22.8429412,28.0347059 L31.7282353,19.1488235 C32.0729412,18.8041176 32.6311765,18.8041176 32.9758824,19.1488235 C33.3205882,19.4935294 33.3205882,20.0517647 32.9758824,20.3964706 L22.8435294,30.5305882 L22.8435294,30.5305882 Z"></path>
                                                            </g>
                                                        </g>
                                                    </svg>
                                                </div>
                                                <div class="t587__success-message t-descr t-descr_lg">Данные успешно
                                                    отправлены. Спасибо!
                                                </div>
                                            </div>
                                        </div>
                                        <div class="t587__form-bottom-text t-text t-text_xs" field="text">
                                            <div style="font-size:20px;color:#231f20;" data-customstyle="yes"><span
                                                    style="font-weight: 500;">Цена программы: {{ $price }} руб</span></div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <style>#rec17082718 input::-webkit-input-placeholder {
                    color: #231f20;
                    opacity: 0.5;
                }

                #rec17082718 input::-moz-placeholder {
                    color: #231f20;
                    opacity: 0.5;
                }

                #rec17082718 input:-moz-placeholder {
                    color: #231f20;
                    opacity: 0.5;
                }

                #rec17082718 input:-ms-input-placeholder {
                    color: #231f20;
                    opacity: 0.5;
                }</style>
        </div>
        <style>#rec17082718 .t-submit:hover {
                background-color: #eb4961 !important;
                color: #ffffff !important;
                box-shadow: 0px 0px 10px 0px rgba(0, 0, 0, 0.2) !important;
            }

            #rec17082718 .t-submit {
                -webkit-transition: background-color 0.2s ease-in-out, color 0.2s ease-in-out, border-color 0.2s ease-in-out, box-shadow 0.2s ease-in-out;
                transition: background-color 0.2s ease-in-out, color 0.2s ease-in-out, border-color 0.2s ease-in-out, box-shadow 0.2s ease-in-out;
            }</style>
        <script>
            $('.js-form-proccess').submit(function() {
                window.ga&&ga('send','event','user','add','email', '{{ $price }}');
            });
        </script>
    </div>
    <div id="rec17082719" class="r" style="padding-top:150px;padding-bottom:150px;background-color:#ffffff; "
         data-record-type="531" data-bg-color="#ffffff"><!-- T531 -->
        <div class="t531">
            <div class="t-section__container t-container">
                <div class="t-col t-col_12">
                    <div class="t-section__topwrapper t-align_center">
                        <div class="t-section__title t-title t-title_xs " field="btitle">Методологи программы</div>
                        <div class="t-section__descr t-descr t-descr_xl t-margin_auto " field="bdescr">
                            <div style="font-size:24px;" data-customstyle="yes"><span style="font-weight: 400;">Анна Чернигова и Андрей Рубан — консультанты Lovelify. Мы пригласили их, чтобы программа стала более живой и глубокой.</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="t-container">
                <div class="t531__row" style="">
                    <div class="t-col t-col_4 t-prefix_1 t531__leftcol">
                        <div class="t531__imgwrapper t531__imgwrapper_lg">
                            <div class="t531__blockimg t-bgimg" bgimgfield="li_img__1478078229569"
                                 data-original="/assets/landing/images/tild6464-3366-4233-a162-623931663262__Anna.jpg"
                                 style="background-image:url('/assets/landing/images/tild6464-3366-4233-a162-623931663262__-__resize__20x__Anna.jpg');"
                                 data-image-width="360" data-image-height="300"></div>
                        </div>
                    </div>
                    <div class="t-col t-col_6 t531__rightcol">
                        <div class="t531__textwrapper t-align_left">
                            <div class="t531__content t-valign_top">
                                <div class="t531__box">
                                    <div class="t531__title t-name t-name_xl t531__bottommargin_lg"
                                         field="li_persname__1478078229569" style="color:#2c2d34;">Анна Чернигова
                                    </div>
                                    <div class="t531__text t-text t-text_sm " field="li_text__1478078229569"
                                         style="color:#2c2d34;">Анна — психолог и сертифицированный психотерапевт по
                                        символдраме, телесноориентированной психологии, позитивной психотерапии, РПТ,
                                        психоаналитической психотерапии, биоэнергетической терапии. С 2009 года проводит
                                        терапевтические группы и индивидуальные консультации по вопросам отношений.
                                        Кандидат наук, преподаватель психологии. Эксперт-психолог в программах «Про
                                        Любовь», «Мужское/Женское на Первом Канале.<br/></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="t531__row" style="">
                    <div class="t-col t-col_4 t-prefix_1 t531__leftcol">
                        <div class="t531__imgwrapper t531__imgwrapper_lg">
                            <div class="t531__blockimg t-bgimg" bgimgfield="li_img__1490224855475"
                                 data-original="/assets/landing/images/tild3539-3965-4839-a164-376331393561__Andrey.jpg"
                                 style="background-image:url('/assets/landing/images/tild3539-3965-4839-a164-376331393561__-__resize__20x__Andrey.jpg');"
                                 data-image-width="360" data-image-height="300"></div>
                        </div>
                    </div>
                    <div class="t-col t-col_6 t531__rightcol">
                        <div class="t531__textwrapper t-align_left">
                            <div class="t531__content t-valign_top">
                                <div class="t531__box">
                                    <div class="t531__title t-name t-name_xl t531__bottommargin_lg"
                                         field="li_persname__1490224855475" style="color:#2c2d34;">Андрей Рубан
                                    </div>
                                    <div class="t531__text t-text t-text_sm " field="li_text__1490224855475"
                                         style="color:#2c2d34;">Андрей — психолог, консультирует по семейной,
                                        организационной психологии, обретению баланса во всех сферах жизни;
                                        мотивационный тренер. Окончил факультет психологии НИУ ВШЭ, Международную
                                        академию коучинга, Московский институт Гештальт-терапии и консультирования. С
                                        2002 года провел 400 тренингов и 350 индивидуальных консультаций. Основатель
                                        компании "Точка отсчета", Тренингового Центра Высшей Школы Экономики.
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script type="text/javascript"> var t531_doResize;
            $(window).resize(function () {
                if (t531_doResize) clearTimeout(t531_doResize);
                t531_doResize = setTimeout(function () {
                    t531_setHeight('17082719');
                }, 200);
            });
            $(document).ready(function () {
                t531_setHeight('17082719');
            });</script>
    </div>
    <div id="rec17082720" class="r" style="background-color:#ffffff; " data-animationappear="off" data-record-type="391"
         data-bg-color="#ffffff"><!-- T391 --><!-- cover -->
        <div class="t-cover" id="recorddiv17082720" bgimgfield="img" style="height:20vh; background-image:url('');">
            <div class="t-cover__carrier" id="coverCarry17082720" data-content-cover-id="17082720"
                 data-content-cover-bg="" data-content-cover-height="20vh" data-content-cover-parallax="fixed"
                 style="background-image:url('');height:20vh; "></div>
            <div class="t-cover__filter"
                 style="height:20vh;background-image: -moz-linear-gradient(top, rgba(255,255,255,1), rgba(255,255,255,1));background-image: -webkit-linear-gradient(top, rgba(255,255,255,1), rgba(255,255,255,1));background-image: -o-linear-gradient(top, rgba(255,255,255,1), rgba(255,255,255,1));background-image: -ms-linear-gradient(top, rgba(255,255,255,1), rgba(255,255,255,1));background-image: linear-gradient(top, rgba(255,255,255,1), rgba(255,255,255,1));filter: progid:DXImageTransform.Microsoft.gradient(startColorStr='#00ffffff', endColorstr='#00ffffff');"></div>
            <div class="t391">
                <div class="t-container">
                    <div class="t391__firstcol t-col t-col_1 ">
                        <div class="t-cover__wrapper t-valign_middle" style="height: 20vh;"></div>
                    </div>
                    <div class="t391__secondcol t-col t-col_8 t-prefix_1">
                        <div class="t-cover__wrapper t-valign_middle" data-hook-content="covercontent"
                             style="height: 20vh;">
                            <div class="t391__textwrapper t-align_center">
                                <div class="t391__title t-title t-title_xs" field="title"
                                     style="color:#2c2d34;font-size:46px;">FAQ
                                </div>
                                <div class="t391__buttonwrapper"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script type="text/javascript">$(document).ready(function () {
                t391_checkSize('17082720');
            });
            $(window).load(function () {
                t391_checkSize('17082720');
            });
            $(window).resize(function () {
                t391_checkSize('17082720');
            });</script>
    </div>
    <div id="rec17082721" class="r" style=" " data-record-type="280"><!-- T260-->
        <div class="t260">
            <div class="t-container">
                <div class="t-col t-col_8 t-prefix_2">
                    <div class="t260__accordion" style="border-bottom: 1px solid #d9d9d9;">
                        <div class="t260__wrapper">
                            <div class="t260__header" style="border-top: 1px solid #d9d9d9;">
                                <div class="t260__title t-name t-name_xl" field="title">
                                    <div style="color:#2c2d34;" data-customstyle="yes">Продолжительность программы</div>
                                </div>
                                <div class="t260__icon">
                                    <div class="t260__lines">
                                        <div class="t260__iconline t260__iconline_left"
                                             style="background: #000000"></div>
                                        <div class="t260__iconline t260__iconline_right"
                                             style="background: #000000"></div>
                                    </div>
                                    <div class="t260__circle" style="background: #dedede"></div>
                                </div>
                            </div>
                            <div class="t260__content">
                                <div class="t260__textwrapper">
                                    <div class="t260__text t-descr t-descr_xs" field="descr">
                                        <div style="font-size:20px;color:#2c2d34;" data-customstyle="yes">Программа в
                                            среднем занимает 3 недели, однако доступ мы не закроем. Пользоваться
                                            методиками и личным кабинетом можно бессрочно.
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script type="text/javascript"> $(document).ready(function () {
                t260_init();
            });</script>
    </div>
    <div id="rec17082722" class="r" style=" " data-record-type="280"><!-- T260-->
        <div class="t260">
            <div class="t-container">
                <div class="t-col t-col_8 t-prefix_2">
                    <div class="t260__accordion" style="border-bottom: 1px solid #d9d9d9;">
                        <div class="t260__wrapper">
                            <div class="t260__header" style="border-top: 1px solid #d9d9d9;">
                                <div class="t260__title t-name t-name_xl" field="title">
                                    <div style="color:#2c2d34;" data-customstyle="yes">Когда начало программы</div>
                                </div>
                                <div class="t260__icon">
                                    <div class="t260__lines">
                                        <div class="t260__iconline t260__iconline_left"
                                             style="background: #000000"></div>
                                        <div class="t260__iconline t260__iconline_right"
                                             style="background: #000000"></div>
                                    </div>
                                    <div class="t260__circle" style="background: #dedede"></div>
                                </div>
                            </div>
                            <div class="t260__content">
                                <div class="t260__textwrapper">
                                    <div class="t260__text t-descr t-descr_xs" field="descr">
                                        <div style="font-size:20px;color:#2c2d34;" data-customstyle="yes">Формат
                                            программы дистанционный и не привязан к дате. Но мы заметили, что если люди
                                            не начинают программу в первые 2 дня, то надолго откладывают, а когда
                                            возвращаются — жалеют о зря потраченном времени. Поэтому лучше не
                                            откладывать. Если случится командировка или другие помехи, мы просто
                                            поставим программу на паузу для вас.
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script type="text/javascript"> $(document).ready(function () {
                t260_init();
            });</script>
    </div>
    <div id="rec17082723" class="r" style=" " data-record-type="280"><!-- T260-->
        <div class="t260">
            <div class="t-container">
                <div class="t-col t-col_8 t-prefix_2">
                    <div class="t260__accordion" style="border-bottom: 1px solid #d9d9d9;">
                        <div class="t260__wrapper">
                            <div class="t260__header" style="border-top: 1px solid #d9d9d9;">
                                <div class="t260__title t-name t-name_xl" field="title">
                                    <div style="color:#2c2d34;" data-customstyle="yes">Какова эффективность
                                        программы<br/>и какой будет результат
                                    </div>
                                </div>
                                <div class="t260__icon">
                                    <div class="t260__lines">
                                        <div class="t260__iconline t260__iconline_left"
                                             style="background: #000000"></div>
                                        <div class="t260__iconline t260__iconline_right"
                                             style="background: #000000"></div>
                                    </div>
                                    <div class="t260__circle" style="background: #dedede"></div>
                                </div>
                            </div>
                            <div class="t260__content">
                                <div class="t260__textwrapper">
                                    <div class="t260__text t-descr t-descr_xs" field="descr">
                                        <div style="font-size:20px;color:#2c2d34;" data-customstyle="yes">Мы собрали
                                            самые эффективные методики для анализа и рефлексии. Они используются в
                                            психологии и коучинге и были адаптированы для самостоятельного
                                            использования. Перемены, которые вы ищете, произойдут в точном соответствии
                                            с вашим желанием и приложенными усилиями. А наш куратор поддержит вас на
                                            пути. Подходы, собранные в программе, помогают и не вредят. Мы проверяли на
                                            себе, и доверяем опыту психологов Анны Черниговой и Андрея Рубана, потому
                                            что они настоящие профессионалы и прекрасно дополняли друг друга на этапе
                                            создания программы. <br/> Если проделаете упражнения, и они не помогут,
                                            вернём вам деньги. <br/></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script type="text/javascript"> $(document).ready(function () {
                t260_init();
            });</script>
    </div>
    <div id="rec17082724" class="r" style=" " data-record-type="280"><!-- T260-->
        <div class="t260">
            <div class="t-container">
                <div class="t-col t-col_8 t-prefix_2">
                    <div class="t260__accordion" style="border-bottom: 1px solid #d9d9d9;">
                        <div class="t260__wrapper">
                            <div class="t260__header" style="border-top: 1px solid #d9d9d9;">
                                <div class="t260__title t-name t-name_xl" field="title">
                                    <div style="color:#2c2d34;" data-customstyle="yes">Почему программа стоит так
                                        дешево/дорого
                                    </div>
                                </div>
                                <div class="t260__icon">
                                    <div class="t260__lines">
                                        <div class="t260__iconline t260__iconline_left"
                                             style="background: #000000"></div>
                                        <div class="t260__iconline t260__iconline_right"
                                             style="background: #000000"></div>
                                    </div>
                                    <div class="t260__circle" style="background: #dedede"></div>
                                </div>
                            </div>
                            <div class="t260__content">
                                <div class="t260__textwrapper">
                                    <div class="t260__text t-descr t-descr_xs" field="descr">
                                        <div style="font-size:20px;color:#2c2d34;" data-customstyle="yes">Если вы
                                            сталкивались с психологией или коучингом раньше, то знаете, что обычно они
                                            стоят кратно дороже. Если не сталкивались, цена может казаться высокой :-)
                                            <br/><br/> Правда в том, что нам важно сделать пользу от программы доступной
                                            для многих. Мы делаем это с помощью технологий: берём очень хороший контент
                                            и переводим в упражнения, которые удобно выполнять самостоятельно. Это
                                            позволяет снизить цену.<br/></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script type="text/javascript"> $(document).ready(function () {
                t260_init();
            });</script>
    </div>
    <div id="rec17082725" class="r" style=" " data-record-type="280"><!-- T260-->
        <div class="t260">
            <div class="t-container">
                <div class="t-col t-col_8 t-prefix_2">
                    <div class="t260__accordion" style="border-bottom: 1px solid #d9d9d9;">
                        <div class="t260__wrapper">
                            <div class="t260__header" style="border-top: 1px solid #d9d9d9;">
                                <div class="t260__title t-name t-name_xl" field="title">
                                    <div style="color:#2c2d34;" data-customstyle="yes">Чем мы лучше тренингов
                                        женственности/пикапа
                                    </div>
                                </div>
                                <div class="t260__icon">
                                    <div class="t260__lines">
                                        <div class="t260__iconline t260__iconline_left"
                                             style="background: #000000"></div>
                                        <div class="t260__iconline t260__iconline_right"
                                             style="background: #000000"></div>
                                    </div>
                                    <div class="t260__circle" style="background: #dedede"></div>
                                </div>
                            </div>
                            <div class="t260__content">
                                <div class="t260__textwrapper">
                                    <div class="t260__text t-descr t-descr_xs" field="descr">
                                        <div style="font-size:20px;color:#2c2d34;" data-customstyle="yes">Прежде чем
                                            создать программу мы были удивлены обилию платных и бесплатных материалов на
                                            темы поиска второй половины, женственности, мужественности, пикапа и т.д.
                                            <br/> Наверняка среди них есть и хорошие. Но как их найти? Это первая
                                            сложность. Но главная сложность в том, что эффект от тренингов часто бывает
                                            краткосрочным, а прослушав бесплатный видео-ролик вы все равно не переходите
                                            к действиям. Программа Lovelify планомерно и качественно ведет вас к
                                            построению осознанных отношений. Доступ к программе мы не закроем и в
                                            будущем вы можете также использовать все методики.<br/></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script type="text/javascript"> $(document).ready(function () {
                t260_init();
            });</script>
    </div>
    <div id="rec17082726" class="r" style="padding-bottom:120px; " data-record-type="280"><!-- T260-->
        <div class="t260">
            <div class="t-container">
                <div class="t-col t-col_8 t-prefix_2">
                    <div class="t260__accordion" style="border-bottom: 1px solid #d9d9d9;">
                        <div class="t260__wrapper">
                            <div class="t260__header" style="border-top: 1px solid #d9d9d9;">
                                <div class="t260__title t-name t-name_xl" field="title">
                                    <div style="color:#2c2d34;" data-customstyle="yes">Кто сделал программу?</div>
                                </div>
                                <div class="t260__icon">
                                    <div class="t260__lines">
                                        <div class="t260__iconline t260__iconline_left"
                                             style="background: #000000"></div>
                                        <div class="t260__iconline t260__iconline_right"
                                             style="background: #000000"></div>
                                    </div>
                                    <div class="t260__circle" style="background: #dedede"></div>
                                </div>
                            </div>
                            <div class="t260__content">
                                <div class="t260__textwrapper">
                                    <div class="t260__text t-descr t-descr_xs" field="descr">
                                        <div style="font-size:20px;color:#2c2d34;" data-customstyle="yes">Мы команда
                                            Brainify. В команде проекта семь человек. Кроме Lovelify мы делаем карьерную
                                            программу «<a href="http://brainify.ru/wiw/" target="_blank"
                                                          style="text-decoration: none;border-bottom: 2px solid;box-shadow: inset 0px -1px 0px 0px;-webkit-box-shadow: inset 0px -1px 0px 0px;-moz-box-shadow: inset 0px -1px 0px 0px;">Я
                                                знаю, чего хочу</a>» . При этом мы работаем с сообществом из более 100
                                            экспертов, с которыми советуемся и делаем совместные проекты. <br/> <br/>Нас
                                            больше всего драйвят три вещи: технологии, осознанность и приносить пользу.
                                            В Brainify они удачным образом сочетаются, так что мы занимаемся любимым
                                            делом, и пользователи это чувствуют. <br/><br/> Мы подходим к делу серьёзно,
                                            но не занудно. <br/></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script type="text/javascript"> $(document).ready(function () {
                t260_init();
            });</script>
    </div>
</div><!--/allrecords--><!-- Tilda copyright. Don't remove this line -->
<div class="t-tildalabel t-tildalabel_white" id="tildacopy" data-tilda-sign="5978#746611"><a
        href="https://tilda.cc/?upm=5978" class="t-tildalabel__link">
        <div class="t-tildalabel__wrapper">
            <div class="t-tildalabel__txtleft">Made on</div>
            <div class="t-tildalabel__wrapimg"><img src="/assets/landing/images/tildacopy_black.png" class="t-tildalabel__img"></div>
            <div class="t-tildalabel__txtright">Tilda</div>
        </div>
    </a></div><!-- Stat -->
<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter44324116 = new Ya.Metrika({
                    id:44324116,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true,
                    webvisor:true
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/44324116" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-53525942-4', 'auto');
    ga('send', 'pageview');

</script>
</body>
</html>