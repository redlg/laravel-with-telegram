@extends('layouts.app')

@section('content')
    <div class="container">
        @if(Session::has('message'))
        <div class="alert {{ Session::get('alert-class', 'alert-info') }}" role="alert">{!! Session::get('message') !!}</div>
        @endif
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Бот</div>
                    {{--<div class="panel-body"></div>--}}
                    <table class="table table-striped table-hover">
                        <tr>
                            <td rowspan="7">Webhook Info
                                <a href="{{ route('bot.webhook', ['update' => true]) }}"
                                   class="btn btn-default"
                                   onclick="$.get(this.href).then(function(){window.location.reload()});return false"
                                ><span class="glyphicon glyphicon-refresh"></span></a>
                            </td>
                            <td>url</td>
                            <td>{{ $webhook->get('url') }}</td>
                        </tr>
                        <tr>
                            <td>has_custom_certificate</td>
                            <td>{{ $webhook->get('has_custom_certificate') }}</td>
                        </tr>
                        <tr>
                            <td>pending_update_count</td>
                            <td>{{ $webhook->get('pending_update_count') }}</td>
                        </tr>
                        <tr>
                            <td>last_error_date</td>
                            <td>{{ $webhook->get('last_error_date') }}</td>
                        </tr>
                        <tr>
                            <td>last_error_message</td>
                            <td>{{ $webhook->get('last_error_message') }}</td>
                        </tr>
                        <tr>
                            <td>max_connections</td>
                            <td>{{ $webhook->get('max_connections') }}</td>
                        </tr>
                        <tr>
                            <td>allowed_updates</td>
                            <td>{!! $webhook->get('allowed_updates') ? implode("<br>", $webhook->get('allowed_updates')) : "" !!}</td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                @if(!$webhook->get('url'))
                                    <form action="{{ route('bot.init') }}" method="post">
                                        {{ csrf_field() }}
                                        <button class="btn btn-default">Установить webhook</button>
                                    </form>
                                @endif
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection