@extends("layouts.chat")

@push('scripts')
<script src="/js/chat.js"></script>
@endpush
@push('styles')
<link rel="stylesheet" href="/css/chat.css?1">
@endpush

@section('content')
    <div class="wrap">
        <section class="b-connect">
            <div class="b-connect__inner">
                <form id="chat_form" action="/chat/save" method="post">
                    {{ csrf_field() }}
                    <input type="hidden" name="email" value="{{ $email }}">
                    <input type="hidden" name="price" value="{{ $price }}">
                    <div class="chat">
                        <!-- page_1 -->
                        <div class="chat__item" id="page_1">
                            <div class="chat__item-inner">
                                <div class="chat-question">
                                    <div class="chat-question__person chat-person wow fadeInDown" style="animation-delay:400ms;">
                                        <img class="chat-person__photo" src="/images/ira-large.png" alt="Ира"/>
                                        <p class="chat_person__name">Ира</p>
                                    </div>
                                    <div class="chat-question__message wow fadeInLeft">
                                        Привет! Здорово, что ты стремишься к осознанным отношениям. Давай знакомиться, чтобы мы могли лучше тебе помочь.
                                    </div>
                                </div>
                                <div class="chat-answer">
                                    <div class="chat-email-request">
                                        <input class="chat-text" type="text" name="fio" placeholder="Как тебя зовут?">
                                        <div class="chat-email-request__checkboxes">
                                            Пол:
                                            <div class="chat-email-request__checkbox">
                                                <input id="agree_to_process_m" class="chat-email-request__checkbox__input" type="radio" name="gender" value="M">
                                                <label for="agree_to_process_m">М</label>
                                            </div>
                                            <div class="chat-email-request__checkbox">
                                                <input id="agree_to_process_f" class="chat-email-request__checkbox__input" type="radio" name="gender" value="F">
                                                <label for="agree_to_process_f">Ж</label>
                                            </div>
                                        </div>
                                    </div>
                                    <button type="button" class="chat-button js-next">Продолжить</button>
                                </div>
                            </div>
                        </div>

                        <!-- page_2 -->
                        <div class="chat__item chat__item--hidden" id="page_2">
                            <div class="chat__item-inner">
                                <div class="chat-question">
                                    <div class="chat-question__person chat-person wow fadeInDown" style="animation-delay:400ms;">
                                        <img class="chat-person__photo" src="/images/ira-large.png" alt="Ира"/>
                                        <p class="chat_person__name">Ира</p>
                                    </div>
                                    <div class="chat-question__message wow fadeInLeft">
                                        Какая история похожа на твою?
                                    </div>
                                </div>
                                <div class="chat-answer">
                                    <div class="radio-buttons">
                                        <label class="radio-buttons__button js-next">
                                            <input type="radio" name="state" value="leave_or_not">
                                            <span>Думаю, уходить из отношений или нет</span>
                                        </label>

                                        <label class="radio-buttons__button js-next">
                                            <input type="radio" name="state" value="make_better">
                                            <span>Хочу улучшить текущие отношения</span>
                                        </label>

                                        <label class="radio-buttons__button js-next">
                                            <input type="radio" name="state" value="loooking_for_partner">
                                            <span>Ищу партнёра</span>
                                        </label>

                                        <label class="radio-buttons__button js-next">
                                            <input type="radio" name="state" value="not_understand_what_want">
                                            <span>Не понимаю, кто мне подходит</span>
                                        </label>

                                        <label class="radio-buttons__button js-next">
                                            <input type="radio" name="state" value="want_understand_myself">
                                            <span>Хочу разобраться в себе</span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- page_3 -->
                        <div class="chat__item chat__item--hidden" id="page_3">
                            <div class="chat__item-inner">
                                <div class="chat-question">
                                    <div class="chat-question__person chat-person wow fadeInDown" style="animation-delay:400ms;">
                                        <img class="chat-person__photo" src="/images/ira-large.png" alt="Ира"/>
                                        <p class="chat_person__name">Ира</p>
                                    </div>
                                    <div class="chat-question__message wow fadeInLeft">
                                        Уточни свою ситуацию, чтобы мы смогли лучше тебе помочь.
                                    </div>
                                </div>
                                <div class="chat-answer">
                                    <textarea name="details" class="chat-textarea expand" placeholder="Начни писать здесь"></textarea>
                                    <button type="submit" class="chat-button js-submit">Завершить</button>
                                </div>
                            </div>
                        </div>

                        <!-- page_4 -->
                        <div class="chat__item chat__item--hidden" id="page_4">
                            <div class="chat__item-inner">
                                <div class="chat-question">
                                    <div class="chat-question__person chat-person wow fadeInDown" style="animation-delay:400ms;">
                                        <img class="chat-person__photo" src="/images/ira-large.png" alt="Ира"/>
                                        <p class="chat_person__name">Ира</p>
                                    </div>
                                    <div class="chat-question__message wow fadeInLeft">
                                        Отлично! Запуск летом. Мы тебе напишем.
                                        <br>
                                        По вопросам пиши на <a id="lovely_email"></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </section>
    </div>
@endsection