@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <a href="{{ route('section.assignment.task.index', [$section_id, $assignment->id]) }}" class="btn-link">← К списку</a>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        @if($task->exists)
                            Изменение задания
                        @else
                            Добавление задания
                        @endif
                    </div>
                    <div class="panel-body">
                        <form class="form-horizontal"
                              role="form" method="POST"
                              action="{{ $task->exists ? route('section.assignment.task.update', [$section_id, $assignment->id, $task->id]) : route('section.assignment.task.store', [$section_id, $assignment->id]) }}">
                            {{ csrf_field() }}
                            {{ $task->exists ? method_field('PUT') : "" }}
                            <input type="hidden" name="assignment_id" value="{{ $assignment->id }}">

                            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                <label for="name" class="col-md-4 control-label">Заголовок</label>
                                <div class="col-md-6">
                                    <input type="text" id="name" class="form-control" name="name" value="{{ old('name', $task->name) }}" />
                                    @if ($errors->has('name'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('text') ? ' has-error' : '' }}">
                                <label for="text" class="col-md-4 control-label">Описание в личном кабинете</label>
                                <div class="col-md-6">
                                    <textarea id="text" name="text" class="form-control">{{ old('text', $task->text) }}</textarea>
                                    @if ($errors->has('text'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('text') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('chat_text') ? ' has-error' : '' }}">
                                <label for="text" class="col-md-4 control-label">Описание для чата</label>
                                <div class="col-md-6">
                                    <textarea id="text" name="text" class="form-control">{{ old('chat_text', $task->chat_text) }}</textarea>
                                    @if ($errors->has('chat_text'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('chat_text') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('position') ? ' has-error' : '' }}">
                                <label for="position" class="col-md-4 control-label">Порядок</label>
                                <div class="col-md-6">
                                    {{ \Form::select('position', $select_positions, old('position', $task->position), ['class' => 'form-control']) }}
                                    @if ($errors->has('position'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('position') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-8 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        @if($task->exists)
                                            Измененить
                                        @else
                                            Добавить
                                        @endif
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection