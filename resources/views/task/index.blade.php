@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <a href="{{ route('section.assignment.index', [$section_id]) }}" class="btn-link">← К разделам</a>
                <div class="panel panel-default">
                    <div class="panel-heading">Задания упражнения "{{ $assignment->name }}"</div>
                    {{--<div class="panel-body"></div>--}}
                    <table class="table table-striped table-hover">
                        <tr>
                            <th width="50">ID</th>
                            <th>Заголовок</th>
                            <th>Текст</th>
                            <th>Порядок</th>
                            <th width="150"></th>
                        </tr>
                        @foreach($tasks as $item)
                            <tr id="task_{{ $item->id }}" class="{{ $item->active ? 'active' : '' }}">
                                <td>{{ $item->id }}</td>
                                <td>{{ $item->name }}</td>
                                <td>{{ $item->text }}</td>
                                <td>{{ $item->position }}</td>
                                <td>
                                    <a href="{{ route('section.assignment.task.edit', [$section_id, $assignment->id, $item->id]) }}"
                                       class="btn btn-xs btn-primary">изменить</a>
                                    <a href="{{ route('section.assignment.task.destroy', [$section_id, $assignment->id, $item->id]) }}"
                                       onclick="return postDelete(this, $('#task_{{ $item->id }}'))"
                                       class="btn btn-xs btn-danger">удалить</a>
                                </td>
                            </tr>
                        @endforeach
                        <tr>
                            <td colspan="5">
                                <a href="{{ route('section.assignment.task.create', [$section_id, $assignment->id]) }}" class="btn btn-primary">Добавить новый</a>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <script>
        function postDelete(link, deleteItem) {
            var url = $(link).attr('href');
            if (confirm('Уверены, что хотите удалить?')) {
                $.ajax({
                    url: url,
                    method: 'post',
                    dataType: 'json',
                    data: {
                        _method: 'delete'
                    }
                }).done(function (data) {
                    if (data.error) {
                        toastr.error(data.error);
                    } else {
                        toastr.info(data.message || 'Удалено');
                        if (deleteItem) {
                            if (!deleteItem.jquery) {
                                deleteItem = $(deleteItem);
                            }
                            deleteItem.remove();
                        }
                    }
                }).fail(function(xhr, status, error) {
                    toastr.error(error);
                });
            }
            return false;
        }
    </script>
@endsection