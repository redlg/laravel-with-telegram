<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Lovelify | Настройка бота</title>
    <meta name="format-detection" content="telephone=no"/>
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon"/><!-- Assets -->
    <script src="/components/jquery/dist/jquery.min.js"></script>

    <link rel="stylesheet" href="/components/bootstrap/dist/css/bootstrap.min.css">
    {{--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">--}}
    <script src="/components/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="/assets/landing/js/jquery-1.10.2.min.js"></script>
</head>
@extends('layouts.app')

@section('content')
    <div id="wf-builder" class="wf-builder"></div>
@endsection

@push('scripts')
    {{--<script src="/assets/app.js"></script>--}}
    <script>
        (function(){
            var Love = window.Love = (window.Love || {});
            Love.Workflow = (Love.Workflow || {});
            Love.routes = {
                "section": {
                    "list": '{{  route('section.index') }}',
                },
                "assignment": {
                    "all": '{{  route('assignment.all') }}',
                    "list": '{{  route('section.assignment.index', ['section' => '_section_']) }}',
                    "show": '{{  route('section.assignment.show', ['section' => '_section_', 'assignment' => '_assignment_']) }}',
                },
                "workflow": {
                    "list": '{{  route('workflow.index') }}',
                    "show": '{{  route('workflow.show', $workflow->id) }}',
                    "step_path": '{{  route('workflow.step_path', $workflow->id) }}',
                    "steps": {
                        "list": '{{ route('workflow.steps.index', ['workflow' => $workflow->id]) }}',
                        "create": '{{ route('workflow.steps.store', ['workflow' => $workflow->id]) }}',
                        "update": '{{ route('workflow.steps.update', ['workflow' => $workflow->id, 'step' => '_step_']) }}',
                        "delete": '{{ route('workflow.steps.destroy', ['workflow' => $workflow->id, 'step' => '_step_']) }}',
                        "file": {
                            "upload": '{{  route('workflow.steps.file.upload', ['workflow' => $workflow->id, 'step' => '_step_']) }}',
                            "delete": '{{  route('workflow.steps.file.delete', ['workflow' => $workflow->id, 'step' => '_step_', 'file' => '_file_']) }}',
                        }
                    }
                }
            };
            Love.User = {!! \Auth::user()->toJson() !!};
            Love.Workflow.fields = {!! json_encode($fields) !!};
        })();
    </script>
@endpush

@if(App::environment('local'))
    @push('scripts')
        <script src="http://<?=request()->getHost()?>:8080/assets/app.min.js"></script>
    @endpush
@else
    @push('scripts')
        <script src="/assets/app.min.js"></script>
    @endpush
    @push('styles')
            <link rel="stylesheet" href="/assets/app.min.css">
    @endpush
@endif
