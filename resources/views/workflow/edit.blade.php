@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        @if($workflow->exists)
                            Изменение сценария
                        @else
                            Добавление сценария
                        @endif
                    </div>
                    <div class="panel-body">
                        <form class="form-horizontal"
                              role="form" method="POST"
                              action="{{ $workflow->exists ? route('workflow.update', $workflow->id) : route('workflow.store') }}">
                            {{ csrf_field() }}
                            {{ $workflow->exists ? method_field('PUT') : "" }}

                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <label for="name" class="col-md-4 control-label">Название</label>

                                <div class="col-md-6">
                                    <input id="name" type="text" class="form-control" name="name" value="{{ old('name', $workflow->name) }}" required autofocus>

                                    @if ($errors->has('name'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="main" value="1" {{ old('main', $workflow->main) ? 'checked' : '' }}>
                                            Основной <br>
                                            <span class="help-block">Основным может быть только 1 сценарий</span>
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-8 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        @if($workflow->exists)
                                            Изменить
                                        @else
                                            Добавить
                                        @endif
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection