@echo off
rem запустить все сервисы, необходимые для разработки

set "START1=0"
set "START2=0"
set "START3=0"

IF [%1]==[] (
  Set START1=1
  Set START2=1
  Set START3=1
)
IF "%1"=="1" Set START1=1
IF "%1"=="2" Set START2=1
IF "%1"=="3" Set START3=1

if %START1% EQU 1 start "queue work" php artisan queue:work
if %START2% EQU 1 start "process tg updates" php artisan bot:updates -m0 -s5
if %START3% EQU 1 start "process bot states" php artisan bot:states -es30