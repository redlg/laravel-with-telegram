<?php

return [
    'api_token' => env('SLACK_API_TOKEN', ''),
    'webhook' => env('SLACK_WEBHOOK_URL', ''),
    'default_icon' => env('SLACK_DEFAULT_ICON', ':ghost:'),
    'default_channel' => env('SLACK_DEFAULT_CHANNEL', '#lovelify'),
    'logs_channel' => env('SLACK_LOGS_CHANNEL', '#lovelify_logs'),
];