<?php
/**
 * Created by PhpStorm.
 * User: RedLine
 * Date: 20.09.2016
 * Time: 8:51
 */

return [
    // поле от по-умолчанию
    "default_source" => "Lovelify",

    //Ваш логин для HTTPS-протокола
    "HTTPS_LOGIN" => env('CABINA_PILOTA_LOGIN', ''),

    //Ваш пароль для HTTPS-протокола
    "HTTPS_PASSWORD" => env('CABINA_PILOTA_PASSWORD', ''),

    //HTTPS-Адрес, к которому будут обращаться скрипты. Со слэшем на конце.
    "HTTPS_ADDRESS" => "https://lcab.sms-uslugi.ru/",

    //HTTP-Адрес, к которому будут обращаться скрипты. Со слэшем на конце.
    "HTTP_ADDRESS" => "http://lcab.sms-uslugi.ru/",

    //метод, которым отправляется запрос (curl или file_get_contents)
    "HTTPS_METHOD" => "curl",

    //1 - использовать HTTPS-адрес, 0 - HTTP
    "USE_HTTPS" => 1,

    //Класс попытается автоматически определить кодировку ваших скриптов. 
    //Если вы хотите задать ее сами в параметре HTTPS_CHARSET, то укажите HTTPS_CHARSET_AUTO_DETECT значение FALSE
    "HTTPS_CHARSET_AUTO_DETECT" => false,
    
    //кодировка ваших скриптов. cp1251 - для Windows-1251, либо же utf-8 для, сообственно - utf-8 :)
    "HTTPS_CHARSET" => "utf-8",
];