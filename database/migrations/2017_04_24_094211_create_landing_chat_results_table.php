<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLandingChatResultsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('landing_chat_results', function (Blueprint $table) {
            $table->increments('id');
            $table->string('email');
            $table->string('fio');
            $table->string('gender', 1);
            $table->string('state', 25);
            $table->text('details');
            $table->float('price');
            $table->timestamps();

            $table->index(['email', 'price']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('landing_chat_results');
    }
}
