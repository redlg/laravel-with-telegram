<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddErrorTextToChatStates extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('chat_states', function (Blueprint $table) {
            $table->string('error_text', 1024)->nullable();
        });
        Schema::table('chat_states_histories', function (Blueprint $table) {
            $table->string('error_text', 1024)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('chat_states', function (Blueprint $table) {
            $table->dropColumn('error_text');
        });
        Schema::table('chat_states_histories', function (Blueprint $table) {
            $table->dropColumn('error_text');
        });
    }
}
