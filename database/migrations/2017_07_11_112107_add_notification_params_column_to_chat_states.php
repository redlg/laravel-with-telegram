<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNotificationParamsColumnToChatStates extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('chat_states', function (Blueprint $table) {
            $table->mediumText('next_notification_params')->nullable();
        });
        Schema::table('chat_states_histories', function (Blueprint $table) {
            $table->mediumText('next_notification_params')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('chat_states', function (Blueprint $table) {
            $table->dropColumn('next_notification_params');
        });
        Schema::table('chat_states_histories', function (Blueprint $table) {
            $table->dropColumn('next_notification_params');
        });
    }
}
