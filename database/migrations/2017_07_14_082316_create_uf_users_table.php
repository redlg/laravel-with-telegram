<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUfUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('uf_users', function (Blueprint $table) {
            $table->unsignedInteger('ref_id');
            $table->boolean('in_pair')->nullable();
            $table->string('city')->nullable();
            $table->tinyInteger('gender')->nullable();

            $table->index(['ref_id']);

            $table->foreign('ref_id')
                ->references('id')->on('users')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });

        // create records for all users
        $users = \App\Models\User::all();
        foreach($users as $user) {
            $user->fields()->create([]);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('uf_users');
    }
}
