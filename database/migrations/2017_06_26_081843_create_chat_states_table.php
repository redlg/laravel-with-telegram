<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChatStatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('chat_states', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('chat_id');
            $table->unsignedInteger('workflow_id');
            $table->unsignedInteger('workflow_step_id');
            $table->dateTime('proceed_at');
            $table->string('chat_state')->default('active');
            $table->dateTime('next_notification_at')->nullable();
            $table->string('next_notification_text')->nullable();
            $table->mediumText('chat_data')->nullable();
            $table->timestamps();

            $table->index(['user_id', 'chat_id']);
            $table->index(['chat_id']);
            $table->index(['workflow_id']);
            $table->index(['workflow_step_id']);
            $table->index(['next_notification_at']);

            $table->foreign('user_id')
                ->references('id')->on('users')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->foreign('chat_id')
                ->references('id')->on('chats')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->foreign('workflow_id')
                ->references('id')->on('workflows')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->foreign('workflow_step_id')
                ->references('id')->on('workflow_steps')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('chat_states');
    }
}
