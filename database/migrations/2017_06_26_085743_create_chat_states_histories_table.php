<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChatStatesHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('chat_states_histories', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('chat_id');
            $table->unsignedInteger('workflow_id');
            $table->unsignedInteger('workflow_step_id');
            $table->dateTime('proceed_at');
            $table->string('chat_state')->default('paused');
            $table->dateTime('next_notification_at')->nullable();
            $table->string('next_notification_text')->nullable();
            $table->mediumText('chat_data')->nullable();
            $table->timestamp('created_at')->nullable();
            $table->string('record_type')->default('insert');

            $table->index(['user_id', 'chat_id']);
            $table->index(['chat_id']);
            $table->index(['workflow_id']);
            $table->index(['workflow_step_id']);
            $table->index(['record_type']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('chat_states_histories');
    }
}
