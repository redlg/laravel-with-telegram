<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddVideoColAndFkToSections extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('assignments', function (Blueprint $table) {
            $table->string('video', 512)->nullable();
            $table->smallInteger('position')->default(0);
            $table->unsignedInteger('section_id')->nullable();

            $table->foreign('section_id')->references('id')->on('sections')->onDelete('SET NULL');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('assignments', function (Blueprint $table) {
            $table->dropForeign(['section_id']);
            $table->dropColumn('video');
            $table->dropColumn('position');
            $table->dropColumn('section_id');
        });
    }
}
