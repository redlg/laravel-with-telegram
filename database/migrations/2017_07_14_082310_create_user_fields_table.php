<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserFieldsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_fields', function (Blueprint $table) {
            $table->increments('id');
            $table->string('ref_table');
            $table->string('name')->nullable();
            $table->string('field_name');
            $table->string('field_type');
            $table->mediumText('settings');

            $table->index(['id']);
            $table->index(['ref_table']);
        });

        // create records for uf_users
        $insert_values = [
            [
                'ref_table' => 'users',
                'name' => 'В паре',
                'field_name' => 'in_pair',
                'field_type' => 'boolean',
                'settings' => serialize([
                    'length' => 1
                ]),
            ],
            [
                'ref_table' => 'users',
                'name' => 'Город',
                'field_name' => 'city',
                'field_type' => 'string',
                'settings' => serialize([
                    'length' => 255
                ]),
            ],
            [
                'ref_table' => 'users',
                'name' => 'Пол',
                'field_name' => 'gender',
                'field_type' => 'string',
                'settings' => serialize([
                    'length' => 1
                ]),
            ],
        ];
        $values = [];
        foreach($insert_values as $insert) {
            \App\Models\UserField::create($insert);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_fields');
    }
}
