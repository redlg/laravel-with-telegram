<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChatMessagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('chat_messages', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('update_id');
            $table->unsignedInteger('message_id');
            $table->string('message_type');
            $table->text('text')->nullable();
            $table->bigInteger('from_peer_id');
            $table->boolean('outgoing')->default(0);
            $table->mediumText('message_data')->nullable();

            $table->timestamps();
            $table->unsignedInteger('chat_id')->nullable();
            $table->unsignedInteger('user_id')->nullable();

            $table->index(['chat_id']);

            $table->foreign('chat_id')
                ->references('id')
                ->on('chats')
                ->onUpdate('cascade')
                ->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('chat_messages');
    }
}
