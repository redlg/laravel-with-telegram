<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $groupAdmins = \App\Models\Group::create([
            'code' => 'administrators',
            'name' => 'Администраторы',
            'description' => 'Администраторы сервиса',
        ]);
        $groupUsers = \App\Models\Group::create([
            'code' => 'users',
            'name' => 'Пользователи',
            'description' => 'Обычные пользователи',
        ]);
        \App\Models\User::create([
            "name" => "Alexey",
            "email" => "putalexey@brainify.ru",
            "password" => bcrypt('temp_password123')
        ])->groups()->sync([$groupAdmins->id, $groupUsers->id]);
    }
}
