/**
 * Created by Kris (putalexey@redlg.ru) on 24.03.2017.
 */
var gulp = require('gulp'),
    sass         = require('gulp-sass'), //препроцессор для sass/scss
    concat       = require('gulp-concat'), //конкатенация файлов
    source       = require('gulp-sourcemaps'), //маппинг
    cssnano      = require('gulp-cssnano'), //минификация css
    plumber      = require('gulp-plumber'), //отлавливает ошибки чтобы не прерывался watch
    autoprefixer = require('gulp-autoprefixer'); //автопрефиксер css
    livereload = require('gulp-livereload');

var path = {
    src: {
        styles: {
            love_chat: 'public/css/**/*.scss'
        }
    },
    build: {
        css: {
            love_chat: 'public/css/'
        }
    }
};

gulp.task('scss', function() {
    gulp.src([path.src.styles.love_chat])
        .pipe(plumber())
        .pipe(source.init())
        // .pipe(concat('global.min.css'))
        .pipe(sass())
        .pipe(autoprefixer())
        .pipe(source.write())
        .pipe(plumber.stop())
        .pipe(gulp.dest(path.build.css.love_chat));
});
gulp.task('watch', function() {
    livereload.listen({
        basePath: __dirname + "/public"
    });

    gulp.watch('public/css/**/*.scss', ['scss']);
    gulp.watch('public/css/**/*.css', livereload.changed);
});



