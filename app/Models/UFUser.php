<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\UserField
 *
 * @mixin \Eloquent
 * @property int $ref_id
 * @property bool $in_pair
 * @property string $city
 * @property bool $gender
 * @method static \Illuminate\Database\Query\Builder|\App\Models\UFUser whereCity($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\UFUser whereGender($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\UFUser whereInPair($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\UFUser whereRefId($value)
 */
class UFUser extends Model
{
    protected $table = 'uf_users';

    protected $primaryKey = 'ref_id';

    public $timestamps = false;
    protected $guarded = [];
}
