<?php

namespace App\Models;

use App\Traits\UpdatesPositions;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Assignment
 *
 * @package App\Models
 * @property int $id
 * @property string $name
 * @property string $description
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $video
 * @property int $position
 * @property int $section_id
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Assignment whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Assignment whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Assignment whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Assignment wherePosition($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Assignment whereSectionId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Assignment whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Assignment whereVideo($value)
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Assignment ordered()
 * @property-read \App\Models\Section $section
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Task[] $tasks
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Assignment whereDescription($value)
 */
class Assignment extends Model
{
    use UpdatesPositions;

    protected $attributes = [
        'position' => 0,
    ];

    protected $fillable = [
        'name',
        'video',
        'description',
        'position',
        'section_id',
    ];

    public function scopeOrdered($query) {
        return $query->orderBy('position');
    }

    public function section()
    {
        return $this->belongsTo(Section::class);
    }
    public function tasks()
    {
        return $this->hasMany(Task::class);
    }
}
