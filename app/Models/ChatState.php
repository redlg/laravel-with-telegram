<?php
/**
 * Brainify
 * Author: Kris (putalexey@redlg.ru)
 * Date: 26.06.2017 13:02
 */

namespace App\Models;


use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;

/**
 * App\Models\ChatState
 *
 * @property int $id
 * @property int $user_id
 * @property int $chat_id
 * @property int $workflow_id
 * @property int $workflow_step_id
 * @property string $proceed_at
 * @property string $chat_state
 * @property string $next_notification_at
 * @property string $next_notification_text
 * @property string $next_notification_params
 * @property string $chat_data
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $error_text
 * @property-read \App\Models\Chat $chat
 * @property-read \App\Models\User $user
 * @property-read \App\Models\Workflow $workflow
 * @property-read \App\Models\WorkflowStep $workflow_step
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ChatState whereChatData($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ChatState whereChatId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ChatState whereChatState($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ChatState whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ChatState whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ChatState whereNextNotificationAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ChatState whereNextNotificationText($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ChatState whereProceedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ChatState whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ChatState whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ChatState whereWorkflowId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ChatState whereWorkflowStepId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ChatState whereNextNotificationParams($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ChatState whereErrorText($value)
 * @mixin \Eloquent
 */
class ChatState extends Model
{
    const STATE_ACTIVE = 'active';
    const STATE_POSTPONE = 'postpone';
    const STATE_WAIT = 'wait';
    const STATE_PAUSED = 'paused';
    const STATE_STOPPED = 'stopped';
    const STATE_ERROR = 'error';

    protected $with = [
        'user',
        'chat',
        'workflow',
        'workflow.steps',
        'workflow_step'
    ];

    protected $guarded = ['id'];
    protected $dates = [
        'proceed_at'
    ];

    public function user() {
        return $this->belongsTo(User::class);
    }
    public function chat() {
        return $this->belongsTo(Chat::class);
    }
    public function workflow() {
        return $this->belongsTo(Workflow::class);
    }
    public function workflow_step() {
        return $this->belongsTo(WorkflowStep::class);
    }

    public function getNextSteps()
    {
        return $this->workflow_step->nextItems;
    }

    protected static function boot()
    {
        parent::boot();

        static::created(function($model) {
            static::logHistory($model, 'insert');
        });
        static::updated(function($model) {
            static::logHistory($model, 'update');
        });
    }
    protected static function logHistory($model, $record_type = 'insert')
    {
        /** @var ChatState $model */
        $histModel = new ChatStatesHistory(['record_type' => $record_type]);
        $histModel->fill(Arr::except($model->getAttributes(), ['id', 'updated_at']));
        $histModel->save();
    }

    public function getChatDataAttribute()
    {
        return !is_null($this->attributes['chat_data']) ? unserialize($this->attributes['chat_data']) : null;
    }
    public function setChatDataAttribute($value)
    {
        return $this->attributes['chat_data'] = serialize($value);
    }

    public function getNextNotificationParamsAttribute()
    {
        return !is_null($this->attributes['next_notification_params']) ? unserialize($this->attributes['next_notification_params']) : null;
    }
    public function setNextNotificationParamsAttribute($value)
    {
        return $this->attributes['next_notification_params'] = serialize($value);
    }

    public function updateProceedAt($date = null)
    {
        $this->setProceedAt($date);
        $this->save();
        return $this;
    }

    public function setProceedAt($date = null)
    {
        if ($date === null)
            $date = new Carbon();
        $this->proceed_at = $date;

        return $this;
    }
    
    public function setNotification($text, $time, $params = [])
    {
        $this->next_notification_text = $text;
        $this->next_notification_at = $time;
        $this->next_notification_params = $params;
    }
    public function clearNotification()
    {
        $this->next_notification_text = null;
        $this->next_notification_at = null;
    }
}