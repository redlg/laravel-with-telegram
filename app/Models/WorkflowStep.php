<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\WorkflowStep
 *
 * @property int $id
 * @property string $type
 * @property object $data
 * @property int $workflow_id
 * @property int $parent_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\WorkflowStep[] $nextItems
 * @property-read \App\Models\WorkflowStep $parent
 * @method static \Illuminate\Database\Query\Builder|\App\Models\WorkflowStep whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\WorkflowStep whereData($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\WorkflowStep whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\WorkflowStep whereParentId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\WorkflowStep whereType($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\WorkflowStep whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\WorkflowStep whereWorkflowId($value)
 * @mixin \Eloquent
 */
class WorkflowStep extends Model
{
    const TYPE_START = 'start';
    const TYPE_DELAY = 'delay';
    const TYPE_ACTION = 'action';
    const TYPE_CONDITION = 'condition';
    const TYPE_ANSWER = 'answer';
    const TYPE_GOTO = 'goto';

    const TYPE_MESSAGE_IMAGE = 'image';
    const TYPE_MESSAGE_VIDEO = 'video';
    const TYPE_MESSAGE_TEXT = 'text';
    const TYPE_MESSAGE_STICKER = 'sticker';

    protected $fillable = [
        'workflow_id',
        'data',
        'type',
        'parent_id',
    ];

    public function getDataAttribute($value)
    {
        return json_decode($value);
    }
    public function setDataAttribute($value)
    {
        $this->attributes['data'] = json_encode($value);
    }

    public function parent() {
        return $this->belongsTo(WorkflowStep::class, 'parent_id');
    }

    public function nextItems() {
        return $this->hasMany(WorkflowStep::class, 'parent_id');
    }

    /**
     * @param int $step_id
     * @return WorkflowStep|null
     */
    public function getNextById($step_id = null) {
        $next_item = null;
        if ($step_id !== false && count($this->nextItems)) {
            foreach($this->nextItems as $item) {
                if ($item->id == $step_id || $step_id === null) {
                    $next_item = $item;
                    break;
                }
            }
        }
        return $next_item;
    }

    protected static function boot()
    {
        parent::boot();
        self::deleted(function($step) {
            if ($step->type == self::TYPE_ACTION
                && in_array($step->data->message_type, [self::TYPE_MESSAGE_IMAGE, self::TYPE_MESSAGE_VIDEO])
                && $step->data->file_id) {
                if ($file = File::find($step->data->file_id))
                    $file->delete();
            }
        });
    }
}
