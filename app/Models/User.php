<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Laravel\Passport\HasApiTokens;

/**
 * App\Models\User
 *
 * @property int $id
 * @property string $name
 * @property string $last_name
 * @property string $email
 * @property string $password
 * @property string $remember_token
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Laravel\Passport\Client[] $clients
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Group[] $groups
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read \Illuminate\Database\Eloquent\Collection|\Laravel\Passport\Token[] $tokens
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User whereEmail($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User wherePassword($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User whereRememberToken($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read \App\Models\Chat $chat
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User whereLastName($value)
 * @property-read \App\Models\UFUser $fields
 */
class User extends Authenticatable
{
    use HasApiTokens;
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'last_name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $with = ['fields'];

    protected static function boot()
    {
        parent::boot();
        static::created(function($user) {
            UserField::create(['user_id' => $user->id]);
        });
    }

    public static function findOrRegister($data, $dataOnRegister = []) {
        /** @var User $user */
        $user = self::firstOrNew($data);
        if(!$user->exists) {
            $user->fill($dataOnRegister);
            $user->password = bcrypt(Str::random());
            $user->random_password = true;
            $user->save();
        }
        return $user;
    }

    public function groups() {
        return $this->belongsToMany(Group::class);
    }

    public function chat() {
        return $this->hasOne(Chat::class);
    }

    public function getBotAddingUrl() {
        return 'https://t.me/' . config('telegram.bot_login') . '?start=' . app('hashids')->encode([$this->id]);
    }

    public function fields() {
        return $this->hasOne(UFUser::class, 'ref_id');
    }

    /**
     * @param $peer_id
     * @return User
     */
    public static function getUserByPeer($peer_id)
    {
        return static::whereHas(
            'chat', function($query) use ($peer_id) {
            $query->where([
                'peer_id' => $peer_id,
            ]);
        })->first();
    }

    public function inGroups($groups = [], $all = false)
    {
        $groups = Arr::wrap($groups);
        $groups_intersect = [];
        foreach ($groups as $group) {
            if (is_numeric($group)) {
                $key = 'id';
                $value = intval($group);
            } else {
                $key = 'code';
                $value = $group;
            }
            $in_group = false;

            foreach ($this->groups as $user_group) {
                if ($user_group->{$key} == $value) {
                    $groups_intersect[] = $user_group;
                    break;
                }
            }
        }
        if ($all)
            return count($groups_intersect) == count($groups);
        else
            return count($groups_intersect) > 0;
    }
}
