<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\UserField
 *
 * @property int $id
 * @property string $ref_table
 * @property string $name
 * @property string $field_name
 * @property string $field_type
 * @property string $settings
 * @method static \Illuminate\Database\Query\Builder|\App\Models\UserField whereFieldName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\UserField whereFieldType($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\UserField whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\UserField whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\UserField whereRefTable($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\UserField whereSettings($value)
 * @mixin \Eloquent
 */
class UserField extends Model
{
    public $timestamps = false;
    protected $guarded = [];

    protected $attributes = [
        'settings' => 'a:0:{}'
    ];

    public function getSettingsAttribute()
    {
        return unserialize($this->attributes['settings']);
    }

    public function setSettingsAttribute($value)
    {
        $this->attributes['settings'] = serialize($value);
    }
}
