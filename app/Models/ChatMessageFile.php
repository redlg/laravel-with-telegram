<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ChatMessageFile
 *
 * @package App\Models
 * @property integer $id
 * @property string $file_id
 * @property string $path
 * @property string $type
 * @property string $src
 * @property integer $size
 * @mixin \Eloquent
 */
class ChatMessageFile extends Model
{
    protected $fillable = [
        'path', 'type',
        'src', 'size',
    ];

    public static function getFile($file_id) {
        $file = ChatMessageFile::firstOrNew(['file_id' => $file_id]);
        if(!$file->exists) {
            $contents = self::downloadFile($file_id, $path);

            $src = 'upload/bot_files/' . $path;
            $path = public_path($src);
            self::checkDirExistence(dirname($path));

            file_put_contents($path, $contents);

            $file = new ChatMessageFile();
            $file->path = $path;
            $file->type = mime_content_type($file->path);
            $file->src = '/' . $src;
            $file->size = filesize($file->path);
            $file->save();
        }

        return $file;
    }

    /**
     * @param string $file_id TG file id to download
     * @param string $path tg file path will be written to with variable
     * @return mixed
     */
    private static function downloadFile($file_id, &$path = '')
    {
        $tgFile = \Telegram::bot()->getFile([
            'file_id' => $file_id
        ]);
        $token = \Telegram::bot()->getAccessToken();
        $path = $tgFile->getFilePath();
        return file_get_contents("https://api.telegram.org/file/bot{$token}/" . $tgFile->getFilePath());
    }

    private static function checkDirExistence($dir)
    {
        if(!file_exists($dir))
            \File::makeDirectory($dir, 0755, true);
    }
}
