<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Workflow
 *
 * @property int $id
 * @property string $name
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property bool $main
 * @property-read \App\Models\WorkflowStep $first_step
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\WorkflowStep[] $steps
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Workflow whereMain($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Workflow whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Workflow whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Workflow whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Workflow whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Workflow extends Model
{
    protected $fillable = [
        'main',
        'name',
    ];

    protected $_first_in_tree = null;

    /**
     * @var \Illuminate\Support\Collection
     */
    protected $allSteps = null;

    /**
     * Возвращает первый шаг сценария, а последующие шаги хранится в параметре next предыдущего
     * @return WorkflowStep|null
     */
    public function steps_tree()
    {
        if(!$this->_first_in_tree) {
            $this->allSteps = $this->steps->keyBy('id');
            $this->allSteps->each(function ($step) {
//            dump($step->toArray(), $step->parent_id);
                if ($step->parent_id) {
                    if (!$this->allSteps[$step->parent_id]->next)
                        $this->allSteps[$step->parent_id]->next = collect();
                    $this->allSteps[$step->parent_id]->next[] = $step;
                } else if ($step->type == WorkflowStep::TYPE_START) {
                    $this->_first_in_tree = $step;
                }
            });
        }
        return $this->_first_in_tree;
    }

    public function first_step()
    {
        $relation = $this->hasOne(WorkflowStep::class);
        $relation->getQuery()->where('type', WorkflowStep::TYPE_START);
        return $relation;
    }

    /**
     * @param $id
     * @return WorkflowStep|null
     */
    public function getStepById($id)
    {
        $this->steps_tree();
        return $this->allSteps->get($id, null);
    }

    public function steps() {
        return $this->hasMany(WorkflowStep::class);
    }

    protected static function boot()
    {
        parent::boot();
        static::deleting(function($workflow) {
            /** @var Workflow $workflow */
            $workflow->steps()->get()->each(function($step) {
                $step->delete();
            });
        });
    }
}
