<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\TgFileCache
 *
 * @mixin \Eloquent
 * @property int $file_id
 * @property string $telegram_file_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Models\TgFileCache whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\TgFileCache whereFileId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\TgFileCache whereTelegramFileId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\TgFileCache whereUpdatedAt($value)
 */
class TgFileCache extends Model
{
    protected $primaryKey = 'file_id';

    protected $fillable = [
        'file_id',
        'telegram_file_id'
    ];
}
