<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Chat
 *
 * @package App\Models
 * @property int $id
 * @property int $habit_id
 * @property string $title
 * @property string $type Тип чата [private/group/supergroup/chanel]
 * @property string $peer_id
 * @property string $join_url
 * @property User $user
 * @property ChatMessage[] $chat_messages
 * @mixin \Eloquent
 * @property int $user_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Chat whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Chat whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Chat wherePeerId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Chat whereTitle($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Chat whereType($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Chat whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Chat whereUserId($value)
 */
class Chat extends Model
{
    protected $fillable = [
        'habit_id',
        'peer_id',
        'title',
        'type',
//        'join_url',
    ];

    public function user() {
        return $this->belongsTo(User::class);
    }
    public function chat_messages() {
        return $this->hasMany(ChatMessage::class);
    }

    public static function getChatByPeer($peer_id) {
        return static::where([
            'peer_id' => $peer_id,
        ])->first();
    }

    /**
     * @param \Telegram\Bot\Objects\Chat $tgChat
     * @param null $user_id
     * @return Model
     */
    public static function createOrGet($tgChat, $user_id = null) {
        $chat = static::firstOrNew([
            'peer_id' => $tgChat->getId(),
            'type' => $tgChat->getType(),
        ]);
        if (!$chat->exists || !$chat->user_id) {
            $chat->user_id = $user_id;
            if($chat->type == 'private') {
                $chat->title = sprintf(
                    '%s %s [%d]',
                    $tgChat->getLastName(),
                    $tgChat->getFirstName(),
                    $tgChat->getId()
                );
            } else {
                $chat->title = $tgChat->getTitle();
            }
            $chat->save();
        }
        return $chat;
    }
}
