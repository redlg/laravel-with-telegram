<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;

/**
 * Class ChatMessages
 *
 * @package App\Models
 * @property int $id
 * @property int $update_id
 * @property int $message_id
 * @property string $message_type
 * @property Carbon $date
 * @property int $chat_id
 * @property string $chat_title
 * @property string $message_data
 * @property string $text
 * @property Chat $chat
 * @mixin \Eloquent
 * @property int $from_peer_id
 * @property bool $outgoing
 * @property string $created_at
 * @property string $updated_at
 * @property int $user_id
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ChatMessage whereChatId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ChatMessage whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ChatMessage whereFromPeerId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ChatMessage whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ChatMessage whereMessageData($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ChatMessage whereMessageId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ChatMessage whereMessageType($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ChatMessage whereOutgoing($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ChatMessage whereText($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ChatMessage whereUpdateId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ChatMessage whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ChatMessage whereUserId($value)
 */
class ChatMessage extends Model
{
    public $timestamps = false;

    protected $data = null;
    protected $attributes = [
        'text' => '',
    ];
    protected $fillable = [
        'update_id',
        'message_id',
        'message_type',
        'text',
        'from_peer_id',
        'outgoing',
        'message_data',
        'chat_id',
        'user_id',
        'created_at',
        'updated_at',
    ];

    public function chat()
    {
        return $this->belongsTo(Chat::class);
    }

    public function getData($key)
    {
        if (!$this->data)
            $this->data = $this->message_data ? json_decode($this->message_data, true) : [];

        return Arr::get($this->data, $key);
    }

    /**
     * @param \Telegram\Bot\Objects\Message $tgMessage
     * @param int $chat_id
     * @param $update_id
     * @param bool $bot_message
     * @return static
     */
    public static function saveTgMessage($tgMessage, $chat_id, $update_id = 0, $bot_message = false)
    {
        return ChatMessage::create([
            'update_id' => $update_id,
            'message_id' => $tgMessage->getMessageId(),
            'message_type' => self::getMessageType($tgMessage),
            'text' => $tgMessage->getText() ?: '',
            'from_peer_id' => $tgMessage->getFrom()->getId(),
            'outgoing' => $bot_message,
            'message_data' => json_encode($tgMessage->toArray()),
            'chat_id' => $chat_id,
            'created_at' => Carbon::createFromTimestamp($tgMessage->getDate()),
        ]);
    }


    /**
     * @param \Telegram\Bot\Objects\Message $message
     * @return string
     */
    public static function getMessageType($message)
    {
        $type = 'text_message';
        if ($message->has('new_chat_member')) {
            $type = 'new_member';
        } elseif ($message->has('left_chat_member')) {
            $type = 'left_member';
        } elseif ($message->has('new_chat_title')) {
            $type = 'new_chat_title';
        } elseif ($message->has('new_chat_photo')) {
            $type = 'new_chat_photo';
        } elseif ($message->has('delete_chat_photo')) {
            $type = 'delete_chat_photo';
        } elseif ($message->has('group_chat_created')) {
            $type = 'chat_created';
        } elseif ($message->has('photo')) {
            $type = 'photo';
        } elseif ($message->has('audio')) {
            $type = 'audio';
        } elseif ($message->has('voice')) {
            $type = 'voice';
        } elseif ($message->has('video')) {
            $type = 'video';
        } elseif ($message->has('video_note')) {
            $type = 'video_note';
        } elseif ($message->has('document')) {
            $type = 'document';
        } elseif ($message->has('sticker')) {
            $type = 'sticker';
        } elseif ($message->has('migrate_to_chat_id')) {
            $type = 'chat_migrated_to_supergroup';
        }

        return $type;
    }
}
