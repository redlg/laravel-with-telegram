<?php

namespace App\Console;

use App\Console\Commands\DeployCommandsCommand;
use App\Console\Commands\MakeDeployCommandCommand;
use App\Console\Commands\HandleBotUpdates;
use App\Console\Commands\ProcessChatStatesCommand;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        DeployCommandsCommand::class,
        MakeDeployCommandCommand::class,
        HandleBotUpdates::class,
        ProcessChatStatesCommand::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')
        //          ->hourly();
        $schedule->command(ProcessChatStatesCommand::class)
                 ->everyMinute();
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
