<?php
declare(ticks = 1);

namespace App\Console\Commands;

use App\Jobs\ProcessChatState;
use App\Models\ChatState;
use Carbon\Carbon;
use Illuminate\Console\Command;

class ProcessChatStatesCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'bot:states {--S|single} {--e|endless} {--s|sleep=60}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Handles chat states. Notifications, postponed';

    /**
     * @var bool Continue satates processing
     */
    private $continue = false;

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->continue = $this->input->getOption('endless');
        $delay = $this->input->getOption('sleep');
        $this->registerSignalHandlers();

        do {
            $this->output->writeln("Quering for states to process");

            $states = ChatState::where(function ($query) {
                $query->where('chat_state', ChatState::STATE_POSTPONE)
                    ->where('proceed_at', '<', Carbon::now());
            })->orWhere(function ($query) {
                $query->where('chat_state', ChatState::STATE_WAIT)
                    ->where('next_notification_at', '<', Carbon::now());
            })->orWhere(function ($query) {
                $query->where('chat_state', ChatState::STATE_ACTIVE)
                    ->where('proceed_at', '<', Carbon::now()->subSeconds(30));
            })->get();

            if (count($states))
                $this->output->writeln(count($states) . " to process");

            foreach ($states as $state) {
//            dump($state->getAttributes());
                dispatch(new ProcessChatState($state));
                if ($this->input->getOption('single'))
                    break;
            }
            if ($this->continue) {
                usleep($delay * 1000000);
            }
        } while ($this->continue);

        return true;
    }

    private function handleSignal($signo) {
        if (in_array($signo, [SIGINT, SIGHUP, SIGTERM])) {
            $this->continue = false;
        }
    }

    private function registerSignalHandlers()
    {
        if (function_exists('pcntl_signal')) {
            pcntl_signal(SIGHUP, [$this, 'handleSignal']);
            pcntl_signal(SIGINT, [$this, 'handleSignal']);
            pcntl_signal(SIGTERM, [$this, 'handleSignal']);
        }
    }
}
