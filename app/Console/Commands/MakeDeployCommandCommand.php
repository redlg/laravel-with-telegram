<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Collection;

class MakeDeployCommandCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:deploy_command {name : Name of the file}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create deploy command';
    protected $files;

    /**
     * Create a new command instance.
     *
     * @param Filesystem $files
     */
    public function __construct(Filesystem $files)
    {
        parent::__construct();
        $this->files = $files;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $path = $this->getCommandsPath();
        $this->files->put(
            $path . DIRECTORY_SEPARATOR . date('Y_m_d_His_') . $this->argument('name') . ".cmd",
            'php artisan list'
        );
        return true;
    }

    protected function getCommandsPath() {
        return $this->laravel->databasePath().DIRECTORY_SEPARATOR.'deploy_commands';
    }

}
