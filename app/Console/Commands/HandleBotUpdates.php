<?php
/**
 * Brainify
 * Author: Kris (putalexey@redlg.ru)
 * Date: 07.07.2017 12:06
 */

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Collection;

class HandleBotUpdates extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'bot:updates {--s|sleep=1} {--m|max_execution=50}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Handle bot updates';

    protected $start_time = 0;
    protected $average_call_time = 0;
    protected $sleep_time = 1;
    protected $max_execution_time = 50;

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->sleep_time = floatval($this->input->getOption('sleep'));
        $this->max_execution_time = intval($this->input->getOption('max_execution'));

        $this->start_time = microtime(true);
        $this->average_call_time = 0;
        while($this->continueHandling()) {
            $time_start = microtime(true);
            app()->call("\\App\\Http\\Controllers\\TelegramBotController@getUpdates");
            $delta = microtime(true) - $time_start;

            if ($this->average_call_time)
                $this->average_call_time = ($this->average_call_time + $delta) / 2;
            else
                $this->average_call_time = $delta;

            usleep($this->sleep_time * 1000000);
        }

        return true;
    }

    protected function continueHandling()
    {
        if ($this->max_execution_time)
            return microtime(true) - $this->start_time + $this->average_call_time + $this->sleep_time < $this->max_execution_time;
        return true;
    }

}
