<?php

namespace App\Console\Commands;

use App\Models\User;
use Illuminate\Console\Command;
use Illuminate\Support\Collection;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;

class DeployCommandsCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'deploy_commands';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Call deploy commands from files';

    protected $table = 'deploy_commands';

    /**
     * Create a new command instance.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            $path = $this->getCommandsPath();
            $cmds = $this->getCommands($path);
            $pending = $this->pendingFiles($cmds, $this->getRan());
            $this->runCommands($pending);
        } catch(\Exception $ex) {
            $this->error($ex->getMessage() . "\n" . $ex->getTraceAsString());
            throw $ex;
        }
        return true;
    }

    protected function pendingFiles($cmds, $ran) {
        return Collection::make($cmds)->reject(function($cmd) use ($ran) {
            return in_array($cmd, $ran);
        });
    }

    protected function runCommands($cmds) {
        $batch = $this->dbTable()->max('batch') ?: 0;
        $batch++;
        $command = windows_os() ? "" : "sh ";

        foreach ($cmds as $cmd) {
            $process = new Process($command . $cmd);
            $process->run();

            // executes after the command finishes
            if (!$process->isSuccessful()) {
                throw new ProcessFailedException($process);
            }
            $this->dbTable()->insert([
                'file' => $cmd,
                'batch' => $batch
            ]);

            echo $process->getOutput();
        }
    }

    protected function getRan() {
        return $this->dbTable()
            ->orderBy('batch', 'asc')
            ->orderBy('file', 'asc')
            ->pluck('file')->all();
    }

    /**
     * @return \Illuminate\Database\Query\Builder
     */
    protected function dbTable() {
        return \DB::connection()->table($this->table);
    }
    protected function getCommands($path)
    {
        return Collection::make(glob($path . "/*_*.cmd"))
            ->filter()->sort();
    }

    protected function getCommandsPath() {
        return $this->laravel->databasePath().DIRECTORY_SEPARATOR.'deploy_commands';
    }

}
