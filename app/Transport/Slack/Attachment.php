<?php
/**
 * Brainify
 * Author: Kris (putalexey@redlg.ru)
 * Date: 23.06.2017 11:44
 */

namespace App\Transport\Slack;


use App\Traits\CreatesItself;
use Illuminate\Support\Fluent;

/**
 * Class Attachment
 * @package App\Transport\Slack
 *
 * @property string $fallback A plain-text summary of the attachment
 * @property string $color color-coding messages. Can be 'good', 'warning', 'danger', or any hex color code (eg. '#439FE0')
 * @property string $pretext This is optional text that appears above the message attachment block
 * @property string $author_name Small text used to display the author's name
 * @property string $author_link A valid URL that will hyperlink the author_name text mentioned above. Will only work if author_name is present.
 * @property string $author_icon A valid URL that displays a small 16x16px image to the left of the author_name text. Will only work if author_name is present.
 * @property string $title Larger, bold text near the top of a message attachment
 * @property string $title_link Link to set on $title
 * @property string $text main text in a message attachment, and can contain {@link https://api.slack.com/docs/message-formatting standard message markup}
 * @property AttachmentField[] $fields Fields are defined as an array, and hashes contained within it will be displayed in a table inside the message attachment
 * @property string $image_url URL to an image file that will be displayed inside a message attachment. Slack currently support the following formats: GIF, JPEG, PNG, and BMP.
 * @property string $thumb_url URL to an image file that will be displayed as a thumbnail on the right side of a message attachment
 * @property string $footer Add some brief text to help contextualize and identify an attachment. Limited to 300 characters
 * @property string $footer_icon Url to a small icon beside your footer text (16x16px)
 * @property string $ts Set if attachment relates to something happening at a specific time
 * @method \App\Transport\Slack\Attachment fallback($value)
 * @method \App\Transport\Slack\Attachment color($value)
 * @method \App\Transport\Slack\Attachment pretext($value)
 * @method \App\Transport\Slack\Attachment author_name($value)
 * @method \App\Transport\Slack\Attachment author_link($value)
 * @method \App\Transport\Slack\Attachment author_icon($value)
 * @method \App\Transport\Slack\Attachment title($value)
 * @method \App\Transport\Slack\Attachment title_link($value)
 * @method \App\Transport\Slack\Attachment text($value)
 * @method \App\Transport\Slack\Attachment fields($value)
 * @method \App\Transport\Slack\Attachment image_url($value)
 * @method \App\Transport\Slack\Attachment thumb_url($value)
 * @method \App\Transport\Slack\Attachment footer($value)
 * @method \App\Transport\Slack\Attachment footer_icon($value)
 * @method \App\Transport\Slack\Attachment ts($value)
 */
class Attachment extends Fluent
{
    use CreatesItself;
    /**
     * All of the attributes set on the container.
     *
     * @var array
     */
    protected $attributes = [];
}