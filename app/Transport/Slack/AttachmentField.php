<?php
/**
 * Brainify
 * Author: Kris (putalexey@redlg.ru)
 * Date: 23.06.2017 11:56
 */

namespace App\Transport\Slack;


use App\Traits\CreatesItself;
use Illuminate\Support\Fluent;

/**
 * Class AttachmentField
 * @package App\Transport\Slack
 * @property string $title
 * @property string $value
 * @property string $short
 * @method \App\Transport\Slack\AttachmentField title($value)
 * @method \App\Transport\Slack\AttachmentField value($value)
 * @method \App\Transport\Slack\AttachmentField short($value)
 */
class AttachmentField extends Fluent
{
    use CreatesItself;

    protected $attributes = [
        'title' => '',
        'value' => '',
    ];

    public function __construct($title='', $value='', $short = null)
    {
        $attributes = [
            'title' => $title,
            'value' => $value,
        ];
        if ($short !== null) {
            $attributes['short'] = $short;
        }
        parent::__construct($attributes);
    }
}