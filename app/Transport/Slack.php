<?php
/**
 * Created by PhpStorm.
 * User: RedLine
 * Date: 14.09.2016
 * Time: 4:46
 */

namespace App\Transport;

use Illuminate\Contracts\Foundation\Application;


class Slack
{
    public $default_channel;
    public $default_icon;

    public function __construct(Application $app)
    {
//        app('config')->get('slack');
        /** @var \Config $config */
        $config = $app['config'];

        $this->webhook = $config->get('slack.webhook');
        $this->default_channel = $config->get('slack.default_channel', '#general');
        $this->default_icon = $config->get('slack.default_icon', ':triangular_flag_on_post:');
    }

    public function send($message, $channel = null, $icon = null, $attachments=array())
    {
        $channel = $channel !== null ? $channel : $this->default_channel;
        $icon = $icon !== null ? $icon : $this->default_icon;
        $payload = array(
            "channel" => $channel,
            "text" => $message,
            "icon_emoji" => $icon
        );
        if(count($attachments)) {
            $payload['attachments'] = $attachments;
        }
        $data = "payload=" . urlencode(json_encode($payload));

        // You can get your webhook endpoint from your Slack settings
        $ch = curl_init($this->webhook);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        curl_close($ch);

        // Laravel-specific log writing method
        // Log::info("Sent to Slack: " . $message, array('context' => 'Notifications'));

        return $result;
    }
}