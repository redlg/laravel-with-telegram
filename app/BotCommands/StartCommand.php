<?php
/**
 * Created by PhpStorm.
 * User: RedLine
 * Date: 06.10.2016
 * Time: 5:06
 */

namespace App\BotCommands;

use App\Jobs\ProcessChatState;
use App\Jobs\SendSlack;
use App\Models\Chat;
use App\Models\ChatMessage;
use App\Models\ChatState;
use App\Models\User;
use App\Models\Workflow;
use App\Transport\Slack\Attachment;
use Carbon\Carbon;
use Telegram\Bot\Commands\Command;
use Telegram\Bot\Exceptions\TelegramResponseException;
use Telegram\Bot\Objects\Message;

class StartCommand extends Command
{
    /**
     * @var string Command Name
     */
    protected $name = 'start';

    /**
     * @var string Command Description
     */
    protected $description = 'Подключить бота';

    /**
     * {@inheritdoc}
     */
    public function handle($arguments)
    {
        /**
         * @var USER $user
         */
        \Log::debug('Handle StartCommand with arguments', (array) $arguments);
        $message = $this->getUpdate()->getMessage();
        try {
            $join_hash = trim($arguments);
            if ($join_hash) {
                list($user_id) = app('hashids')->decode($join_hash);
                if ($user_id) {
                    // link user with account
                    $user = User::find($user_id);
                    if (!$user)
                        throw new \Exception("Пользователь с ID [$user_id] не найден");

                    $this->startWorkflow($user, $message);
                    return;
                } else {
                    dispatch(SendSlack::warning("Пользователь добавил бота к себе, но переданный hash не верен...")
                        ->addAttachment(Attachment::make()->title('hash')->text($join_hash))
                        ->addAttachment(Attachment::make()->title('message')->text(var_export($message->toArray(), true)))
                    );
                    \Log::warning(
                        'Пользователь добавил бота к себе, но переданный hash не верен...',
                        [
                            'hash' => $join_hash,
                            'message' => $message->toArray(),
                        ]);
                }
            }
            $user = User::getUserByPeer($message->getFrom()->getId());
            if ($user) {
                $this->startWorkflow($user, $message);
                return;
            }
            \Telegram::sendMessage([
                'chat_id' => $message->getChat()->getId(),
                'text' => 'Привет! Добро пожаловать на Lovelify. Пройди, пожалуйста, регистрацию на http://lovelify.ru' .
                    ' и открой телеграм через ссылку в личном кабинете'
            ]);
        } catch(TelegramResponseException $ex) {
            dispatch(SendSlack::error("Ошибка ответа telegram", $ex));
            \Log::error($ex, $message->toArray());
        } catch(\Exception $ex) {
            dispatch(SendSlack::error("Ошибка обработки telegram комманды /start", $ex));
            \Log::error($ex, $message->toArray());
        }
    }

    public function startWorkflow(User $user, Message $message)
    {
        $from = $message->getFrom();

        /** @var Chat $chat */
        $chat = Chat::createOrGet($message->getChat(), $user->id);

        if (!$user->name && $from->getFirstName()) {
            $user->name = $from->getFirstName();
        }
        if (!$user->last_name && $from->getLastName()) {
            $user->last_name = $from->getLastName();
        }
        $user->save(); // сохраняет только если были изменения

        $workflow = Workflow::where('main', 1)->firstOrFail();
        $workflow_step = $workflow->first_step;

        if (!ChatState::where('chat_id', $chat->id)->exists()) {
            $state = new ChatState();
            $state->chat_id = $chat->id;
            $state->user_id = $user->id;
            $state->workflow_id = $workflow->id;
            $state->workflow_step_id = $workflow_step->id;
            $state->proceed_at = new Carbon;
            $state->chat_state = ChatState::STATE_ACTIVE;
            $state->chat_data = [];

            $state->save();

            dispatch(new ProcessChatState($state));
        }
    }
}
