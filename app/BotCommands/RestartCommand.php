<?php
/**
 * Created by PhpStorm.
 * User: RedLine
 * Date: 06.10.2016
 * Time: 5:06
 */

namespace App\BotCommands;

use App\Jobs\ProcessChatState;
use App\Jobs\SendSlack;
use App\Models\Chat;
use App\Models\ChatMessage;
use App\Models\ChatState;
use App\Models\User;
use App\Models\Workflow;
use App\Transport\Slack\Attachment;
use Carbon\Carbon;
use Telegram\Bot\Commands\Command;
use Telegram\Bot\Exceptions\TelegramResponseException;
use Telegram\Bot\Objects\Message;

class RestartCommand extends Command
{
    /**
     * @var string Command Name
     */
    protected $name = 'restart';

    /**
     * @var string Command Description
     */
    protected $description = 'Заного запустить сценарий';

    /**
     * {@inheritdoc}
     */
    public function handle($arguments)
    {
        /**
         * @var USER $user
         */
        \Log::debug('Handle StartCommand with arguments', (array) $arguments);
        $message = $this->getUpdate()->getMessage();
        try {
            $user = User::getUserByPeer($message->getFrom()->getId());
            if ($user && \Gate::forUser($user)->allows('restart-program')) {
                ChatState::whereChatId($user->chat->id)->get()->each(function($state) { $state->delete(); });
                $this->startWorkflow($user, $message);
                return;
            } else {
                \Telegram::bot()->sendMessage([
                    'chat_id' => $this->getUpdate()->getChat()->getId(),
                    'text' => 'Вы не можете перезапустить программу',
                ]);
            }
        } catch(TelegramResponseException $ex) {
            dispatch(SendSlack::error("Ошибка ответа telegram", $ex));
            \Log::error($ex, $message->toArray());
        } catch(\Exception $ex) {
            dispatch(SendSlack::error("Ошибка обработки telegram комманды /start", $ex));
            \Log::error($ex, $message->toArray());
        }
    }

    public function startWorkflow(User $user, Message $message)
    {
        $from = $message->getFrom();

        /** @var Chat $chat */
        $chat = Chat::createOrGet($message->getChat(), $user->id);

        if (!$user->name && $from->getFirstName()) {
            $user->name = $from->getFirstName();
        }
        if (!$user->last_name && $from->getLastName()) {
            $user->last_name = $from->getLastName();
        }
        $user->save(); // сохраняет только если были изменения

        $workflow = Workflow::where('main', 1)->firstOrFail();
        $workflow_step = $workflow->first_step;

        if (!ChatState::where('chat_id', $chat->id)->exists()) {
            $state = new ChatState();
            $state->chat_id = $chat->id;
            $state->user_id = $user->id;
            $state->workflow_id = $workflow->id;
            $state->workflow_step_id = $workflow_step->id;
            $state->proceed_at = new Carbon;
            $state->chat_state = ChatState::STATE_ACTIVE;
            $state->chat_data = [];

            $state->save();

            dispatch(new ProcessChatState($state));
        }
    }
}
