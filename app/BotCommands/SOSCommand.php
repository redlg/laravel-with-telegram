<?php
/**
 * Created by PhpStorm.
 * User: RedLine
 * Date: 06.10.2016
 * Time: 5:06
 */

namespace App\BotCommands;

use App\Jobs\SendSlack;
use App\Models\Chat;
use App\Models\ChatMessage;
use App\Models\User;
use Illuminate\Support\Str;
use Telegram\Bot\Keyboard\Button;
use Telegram\Bot\Keyboard\Keyboard;
use Telegram\Bot\Commands\Command;

class SOSCommand extends Command
{
    /**
     * @var string Command Name
     */
    protected $name = 'sos';

    /**
     * @var string Command Description
     */
    protected $description = 'Попросить помощи куратора';

    /**
     * {@inheritdoc}
     */
    public function handle($arguments)
    {
        try {
            $update = $this->getUpdate();
            $user = User::getUserByPeer($update->getMessage()->getFrom()->getId());
            $local_chat = Chat::getChatByPeer($update->getMessage()->getChat()->getId());
            if ($user) {
                if (\Gate::forUser($user)->allows('request-help')) {
                    $userFields = [
                        'Имя' => $user->name . ($user->last_name ? " {$user->last_name}" : ""),
                        'Телефон' => $user->phone,
                        'Логин Telegram' => $update->getMessage()->getFrom()->getUsername(),
                    ];
                    dispatch(SendSlack::make()->text("Пользователю требуется помощь\n" . implode("\n", $userFields)));
                    $tgMessage = $this->replyWithMessage([
                        'text' => "Куратор скоро с вами свяжется",
                    ]);
                    $this->loggedMessage = ChatMessage::saveTgMessage($tgMessage, $local_chat ? $local_chat->id : null, $update->getUpdateId());
                } else {
                    $this->replyWithMessage([
                        'text' => "Вы сейчас не можете попросить помощи куратора",
                    ]);
                }
            } else {
                $this->replyWithMessage([
                    'text' => "Вы найдены в базе",
                ]);
            }
        } catch(\Exception $ex) {
            \Log::error($ex->getMessage() . "\n" . $ex->getTraceAsString());
        }
    }
}
