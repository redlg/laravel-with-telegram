<?php
/**
 * Created by PhpStorm.
 * User: RedLine
 * Date: 06.10.2016
 * Time: 5:06
 */

namespace App\BotCommands;

use Telegram\Bot\Keyboard\Button;
use Telegram\Bot\Keyboard\Keyboard;
use Telegram\Bot\Commands\Command;

class TestCommand extends Command
{
    /**
     * @var string Command Name
     */
    protected $name = 'test';

    /**
     * @var string Command Description
     */
    protected $description = 'Тест';

    /**
     * {@inheritdoc}
     */
    public function handle($arguments)
    {return;
        try {
            $keyboard = Keyboard::make();//->inline();
            $keyboard->row(
                Button::make([
                    'text' => env('habits.done_message', 'сделал'),
                ]),
                Button::make([
                    'text' => env('habits.skip_message', 'не сделал'),
                ])
            );
            $keyboard->setOneTimeKeyboard(true);
            $keyboard->setResizeKeyboard(true);

            $this->replyWithMessage([
                'text' => 'try this',
                'reply_markup' => $keyboard,
            ]);
        } catch(\Exception $ex) {
            \Log::error($ex->getMessage() . "\n" . $ex->getTraceAsString());
        }
    }
}
