<?php
/**
 * Created by PhpStorm.
 * User: RedLine
 * Date: 06.10.2016
 * Time: 5:06
 */

namespace App\BotCommands;

use App\Models\Chat;
use App\Models\ChatMessage;
use App\Models\User;
use Illuminate\Support\Str;
use Telegram\Bot\Keyboard\Button;
use Telegram\Bot\Keyboard\Keyboard;
use Telegram\Bot\Commands\Command;

class PasswordCommand extends Command
{
    /**
     * @var string Command Name
     */
    protected $name = 'password';

    /**
     * @var string Command Description
     */
    protected $description = 'Сброс пароля';

    /**
     * {@inheritdoc}
     */
    public function handle($arguments)
    {
        try {
            $update = $this->getUpdate();
            $user = User::getUserByPeer($update->getMessage()->getFrom()->getId());
            if ($user) {
                $new_pass = Str::random(8);
                $user->update(["password" => bcrypt($new_pass)]);
                $tgMessage = $this->replyWithMessage([
                    'text' => "Твой новый пароль: $new_pass",
                ]);
                $local_chat = Chat::getChatByPeer($update->getMessage()->getChat()->getId());
                $this->loggedMessage = ChatMessage::saveTgMessage($tgMessage, $local_chat ? $local_chat->id : null, $update->getUpdateId());
            } else {
                $this->replyWithMessage([
                    'text' => "Вы найдены в базе",
                ]);
            }
        } catch(\Exception $ex) {
            \Log::error($ex->getMessage() . "\n" . $ex->getTraceAsString());
        }
    }
}
