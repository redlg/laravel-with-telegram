<?php
/**
 * Brainify
 * Author: Kris (putalexey@redlg.ru)
 * Date: 19.06.2017 12:22
 */

namespace App\Http;


class JSONReponse
{
    public static function success($data = []) {
        return response($data + ['status' => 'success']);
    }

    public static function error($message, $code = 400) {
        return response([
            'status' => 'error',
            'error' => $message,
            'code' => $code,
        ]);
    }
    public static function redirect($to = null) {
        if (request()->expectsJson()) {
            return response([
                'status' => 'redirect',
                'to' => $to,
            ]);
        } else {
            return redirect($to);
        }
    }
}