<?php

namespace App\Http\Controllers;

use App\Traits\GeneratesSelectPositions;
use App\Models\Assignment;
use App\Models\Section;
use Illuminate\Http\Request;

class AssignmentController extends Controller
{
    use GeneratesSelectPositions;

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function all(Request $request)
    {
        $assignments = Assignment::orderBy('section_id')->ordered()->get();

        return $assignments;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @param Section $section
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, Section $section)
    {
        $assignments = $section->assignments()->ordered()->get();
        if ($request->expectsJson()) {
            return $assignments;
        }
        return view('assignment.index', ['assignments' => $assignments, 'section' => $section]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Section $section)
    {
        $assignment = new Assignment();

        $select_positions = $this->getSelectPositions($section->assignments()->ordered()->get(), $assignment);
        $section->position = $select_positions->keys()->last();
        return view('assignment.edit', [
            'assignment' => $assignment,
            'sections' => Section::ordered()->get(),
            'select_positions' => $select_positions,
            'section' => $section
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param int $section_id
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $section_id)
    {
        $this->validate($request, $this->getValidationRules());
        $assignment = Assignment::create($request->only([
            'name', 'video', 'position', 'section_id', 'description',
        ]));
        Assignment::updatePositions($assignment->section_id);

        return redirect(route('section.assignment.index', [$section_id]));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Assignment  $assignment
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, Section $section, Assignment $assignment)
    {
        if ($request->expectsJson()) {
            return $assignment;
        }
        return redirect(route('section.assignment.edit', [$section->id, $assignment->id]));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Assignment  $assignment
     * @return \Illuminate\Http\Response
     */
    public function edit(Section $section, Assignment $assignment)
    {
        return view('assignment.edit', [
            'assignment' => $assignment,
            'sections' => Section::ordered()->get(),
            'select_positions' => $this->getSelectPositions(
                $section->assignments()->ordered()->get(),
                $assignment
            ),
            'section' => $section
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param int $section_id
     * @param  \App\Models\Assignment $assignment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $section_id, Assignment $assignment)
    {
        $this->validate($request, $this->getValidationRules());
        $assignment->update($request->only([
            'name', 'video', 'position', 'section_id', 'description',
        ]));
        Assignment::updatePositions($section_id);
        return redirect(route('section.assignment.index', [$section_id]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Assignment  $assignment
     * @return \Illuminate\Http\Response
     */
    public function destroy($section_id, Assignment $assignment)
    {
        $assignment->delete();

        return redirect(route('section.assignment.index', [$section_id]));
    }

    protected function getValidationRules() {
        return [
            'name' => 'required|max:255',
            'video' => 'max:512',
            'position' => 'integer',
            'section_id' => 'integer',
        ];
    }
}
