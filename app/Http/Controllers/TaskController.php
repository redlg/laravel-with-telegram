<?php

namespace App\Http\Controllers;

use App\Traits\GeneratesSelectPositions;
use App\Http\JSONReponse;
use App\Models\Assignment;
use App\Models\Section;
use App\Models\Task;
use Illuminate\Http\Request;

class TaskController extends Controller
{
    use GeneratesSelectPositions;

    /**
     * Display a listing of the resource.
     *
     * @param $section_id
     * @param Assignment $assignment
     * @return \Illuminate\Http\Response
     */
    public function index($section_id, Assignment $assignment)
    {
        return view('task.index', [
            'section_id' => $section_id,
            'assignment' => $assignment,
            'tasks' => $assignment->tasks()->ordered()->get(),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param $section_id
     * @param Assignment $assignment
     * @return \Illuminate\Http\Response
     */
    public function create($section_id, Assignment $assignment)
    {
        $task = new Task();
        $select_positions = $this->getSelectPositions($assignment->tasks()->ordered()->get(), $task);
        $task->position = $select_positions->keys()->last();
        return view('task.edit', [
            'section_id' => $section_id,
            'assignment' => $assignment,
            'select_positions' => $select_positions,
            'task' => $task,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param $section_id
     * @param $assignment_id
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $section_id, $assignment_id)
    {
        $task = new Task($request->all());
        $task->assignment_id = $assignment_id;
        $task->save();
        Task::updatePositions();

        return redirect(route('section.assignment.task.index', [$section_id, $assignment_id]));
    }

    /**
     * Display the specified resource.
     *
     * @param $section_id
     * @param $assignment_id
     * @param $task_id
     * @return \Illuminate\Http\Response
     * @internal param Task $task
     */
    public function show($section_id, $assignment_id, $task_id)
    {
        return JSONReponse::redirect(route('section.assignment.task.index', [$section_id, $assignment_id, $task_id]));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $section_id
     * @param Assignment $assignment
     * @param  \App\Models\Task $task
     * @return \Illuminate\Http\Response
     */
    public function edit($section_id, Assignment $assignment, Task $task)
    {
        $select_positions = $this->getSelectPositions($assignment->tasks()->ordered()->get(), $task);
        return view('task.edit', [
            'section_id' => $section_id,
            'assignment' => $assignment,
            'select_positions' => $select_positions,
            'task' => $task,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $section_id, $assignment_id, Task $task)
    {
        $task->update($request->all());
        Task::updatePositions();

        return JSONReponse::redirect(route('section.assignment.task.index', [$section_id, $assignment_id, $task->id]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function destroy($section_id, $assignment_id, Task $task)
    {
        $task->delete();

        return JSONReponse::redirect(route('section.assignment.task.index', [$section_id, $assignment_id, $task->id]));
    }
}
