<?php

namespace App\Http\Controllers;

use App\Jobs\ProcessChatState;
use App\Jobs\SendSlack;
use App\Models\Chat;
use App\Models\ChatMessage;
use App\Models\ChatState;
use App\Models\TaskAnswer;
use App\Telegram\ButtonCallbackData;
use App\Telegram\WebhookInfo;
use App\Transport\Slack\Attachment;
use App\Workflow\StepExecutor;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Telegram;
use App\Telegram\Keyboard;
use App\Telegram\Button;
use Telegram\Bot\Objects\Audio;
use Telegram\Bot\Objects\Document;
use Telegram\Bot\Objects\PhotoSize;
use Telegram\Bot\Objects\Update;
use Telegram\Bot\Objects\Video;
use Telegram\Bot\Objects\Voice;

class TelegramBotController extends Controller
{
    /**
     * @var \App\Models\ChatMessage
     */
    protected $loggedMessage = null;

    public function webhook($token) {
        if($token !== env('TELEGRAM_WEBHOOK_TOKEN', '')) {
            return response('Wrong Token', 403);
        }
        $update = Telegram::bot()->commandsHandler(true);
        if(!$update->get('update_id')) {
            return response('update_id field not found', 422);
        }

        return $this->processUpdates([$update]);
    }

    public function getUpdates()
    {
        /** @var Telegram\Bot\Api $bot */
        $updates = Telegram::bot()->commandsHandler();
//        $updates = Telegram::bot()->getUpdates();
//        if (count($updates)) {
//            foreach($updates as $update) {
//                dump ('Update: ' . $update->getUpdateId() . ($update->hasMessage() ? ' ' . ChatMessage::getMessageType($update->getMessage()) : ''));
//            }
//        }

        return $this->processUpdates($updates);
    }

    /**
     * @param \Telegram\Bot\Objects\Update[] $updates
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    protected function processUpdates($updates)
    {
        foreach($updates as $update) {
            if (!$update->get('update_id')) {
                return response('update_id field not found', 422);
            }

            $this->logUpdate($update);
            try {
                $this->processUpdate($update);
            } catch (\Exception $ex) {
                $this->dispatch(SendSlack::make()
                    ->text("Ошибка обработки сообщения боту [{$ex->getCode()}] {$ex->getMessage()}")
                    ->channel(config('slack.logs_channel'))
                    ->attachments([ Attachment::make()->title('Stack')->text($ex->getTraceAsString()) ])
                );
                \Log::error("Ошибка обработки сообщения боту\n" . (string)$ex);
            }
        }

        return response('ok');
    }

    /**
     * @param $update Update
     */
    public function processUpdate($update) {
        $message = $update->getMessage();
        if($message) {
            $this->processMessageText($message);
        } elseif ($update->has('edited_message')) {
            $this->processMessageUpdate($update->getEditedMessage());
        }
        if ($update->getCallbackQuery()) {
            $this->processCallbackQuery($update);
        }
    }

    /**
     * @param Update $update
     */
    protected function logUpdateToDB($update)
    {
        $message = $update->getMessage();
        if($message) {
            $chat = $message->getChat();

            $local_chat = Chat::createOrGet($chat);

            $this->loggedMessage = ChatMessage::saveTgMessage($message, $local_chat->id, $update->getUpdateId());
        }
    }


    /**
     * @param Update $update
     */
    protected function logUpdate($update)
    {
        try {
            $this->logUpdateToDB($update);
        } catch(\Exception $ex) {
            $this->dispatch(SendSlack::make()
                ->text("Ошибка логирования сообщения боту [{$ex->getCode()}] {$ex->getMessage()}")
                ->channel(config('slack.logs_channel'))
                ->attachments([ Attachment::make()->title('Stack')->text($ex->getTraceAsString()) ])
            );
            \Log::error($ex);
        }
    }

    /**
     * @param \Telegram\Bot\Objects\Message $message
     */
    protected function processMessageUpdate($message) {
        $old_message = ChatMessage::where('message_id', $message->getMessageId())->first();
        if ($old_message) {
            // get saved answer in lk
            /** @var TaskAnswer $answer */
            if($answer = TaskAnswer::where('chat_message_id', $old_message->id)->first()) {
                $answer->answer = $message->getText() ?: "";
                $answer->answer_type = ChatMessage::getMessageType($message);

                // if old file was there
                if ($answer->file) {
                    $answer->file->delete();
                    $answer->file_id = null;
                }

                if (in_array($answer->answer_type, ['photo', 'audio', 'voice', 'video', 'video_note', 'document'])) {
                    $answer->answer = $message->get('caption', $answer->answer);

                    /** @var PhotoSize[]|Video|Voice|Document|Audio $object */
                    $object = $message->get($answer->answer_type);
                    if ($answer->answer_type == 'photo') {
                        $best_photo = Arr::first($object);
                        foreach($object as $obj) {
                            if (!empty($best_photo['file_size'])
                                && !empty($obj['file_size'])
                                && $best_photo['file_size'] < $obj['file_size']) {
                                $best_photo = $obj;
                            }
                        }
                        $object = new PhotoSize($best_photo);
                    }

                    $db_file = \App\Workflow\Steps\Answer::downloadFile($object, $message->get('caption', $answer->answer));
                    if ($db_file)
                        $answer->file_id = $db_file->id;
                }

                $answer->save();
            }
        }
    }

    /**
     * @param \Telegram\Bot\Objects\Message $message
     */
    protected function processMessageText($message) {
        // if message is command then do not process it
        if ($message->has('text') && !empty(Telegram::bot()->getCommandBus()->parseCommand($message->getText()))) {
            return;
        }

//        $text = mb_strtolower($message->getText());
        $state = ChatState::whereHas('chat', function($query) use ($message) {
            $query->where('peer_id', $message->getChat()->getId());
        })->first();
        if ($state) {
            dispatch((new ProcessChatState($state, $message, $this->loggedMessage))->onConnection('sync'));
        }
    }

    protected function processCallbackQuery(Update $update) {
        $cb_query = $update->getCallbackQuery();
        $user_peer_id = $cb_query->getFrom()->getId();
        $chat_peer_id = $cb_query->getMessage()->getChat()->getId();
        /** @var ChatState $state */
        $state = ChatState::whereHas(
            'chat',
            function($query) use ($chat_peer_id) {
                $query->where('peer_id', $chat_peer_id);
            })->first();

        $cb_data = @json_decode($cb_query->getData(), true);
        $cb_answer_params = [
            'callback_query_id' => $cb_query->getId()
        ];
        if($state && $cb_data) {
            $cb_data = new ButtonCallbackData($cb_data);
            switch($cb_data->getAction()) {
                case Button::ACTION_CONTINUE:
                    if ($state->chat_state == ChatState::STATE_POSTPONE) {
                        $state->chat_state = ChatState::STATE_ACTIVE;
                        $state->setProceedAt();
                        $state->save();
                    } else {
                        (new StepExecutor($state))->nextStep();
                        $state->clearNotification();
                        $state->save();
                    }
                    Telegram::bot()->editMessageReplyMarkup([
                        'chat_id' => $cb_query->getMessage()->getChat()->getId(),
                        'message_id' => $cb_query->getMessage()->getMessageId(),
                        'reply_markup' => '',
                    ]);
                    break;
                case Button::ACTION_DELAY:
                    $prev_actions = $cb_data->getPrevActions(Button::ACTION_CONTINUE|Button::ACTION_DELAY);
                    if ($cb_data->getPeriod()) {
                        $state->chat_data = [
                            'prev_chat_id' => $cb_query->getMessage()->getChat()->getId(),
                            'prev_message_id' => $cb_query->getMessage()->getMessageId(),
                        ];
                        if($cb_data->getPeriod() == Button::DELAY_PERIOD_MONTH) {
                            $state->setNotification('Что-то вас давно не видно. Продолжаем?', Carbon::now()->addMonth());
                            $cb_answer_params['text'] = "Увидимся через месяц";
                        } elseif($cb_data->getPeriod() == Button::DELAY_PERIOD_WEEK) {
                            $state->setNotification('Что-то вас давно не видно. Продолжаем?', Carbon::now()->addWeek());
                            $cb_answer_params['text'] = "Увидимся через неделю";
                        } else {
                            $state->setNotification('Продолжаем?', Carbon::now()->addDay());
                            $cb_answer_params['text'] = "Увидимся завтра";
                        }
                        $state->save();
                        $keyboard = Keyboard::make()->inline();
                        $buttons = [];
                        if($prev_actions & Button::ACTION_CONTINUE)
                            $buttons[] = Button::make()
                                ->setText("Продолжить программу")
                                ->setCallbackData(ButtonCallbackData::make()
                                    ->setAction(Button::ACTION_CONTINUE)
                                    ->setPrevActions($prev_actions)
                                );
                        if($prev_actions & Button::ACTION_DELAY)
                            $buttons[] = Button::make()
                                ->setText("Отложить")
                                ->setCallbackData(ButtonCallbackData::make()
                                    ->setAction(Button::ACTION_DELAY)
                                    ->setPrevActions($prev_actions)
                                );
                        $keyboard->rowArray($buttons);
                        $keyboard->setResizeKeyboard(true);

                        Telegram::bot()->editMessageReplyMarkup([
                            'chat_id' => $cb_query->getMessage()->getChat()->getId(),
                            'message_id' => $cb_query->getMessage()->getMessageId(),
                            'reply_markup' => $keyboard,
                        ]);
                    } else {
                        $keyboard = Keyboard::make()->inline();
                        if($prev_actions & Button::ACTION_DELAY)
                            $keyboard->row(
                                Button::make()
                                    ->setText("Отложить на день")
                                    ->setCallbackData(ButtonCallbackData::make()
                                            ->setAction(Button::ACTION_DELAY)
                                            ->setPeriod(Button::DELAY_PERIOD_DAY)
                                            ->setPrevActions($prev_actions)
                                    ),
                                Button::make()
                                    ->setText("Отложить на неделю")
                                    ->setCallbackData(ButtonCallbackData::make()
                                            ->setAction(Button::ACTION_DELAY)
                                            ->setPeriod(Button::DELAY_PERIOD_WEEK)
                                            ->setPrevActions($prev_actions)
                                    )
                            );

                        if($prev_actions & Button::ACTION_CONTINUE)
                            $keyboard->row(
                                Button::make()
                                    ->setText("Продолжить программу")
                                    ->setCallbackData(ButtonCallbackData::make()
                                        ->setAction(Button::ACTION_CONTINUE)
                                        ->setPrevActions($prev_actions)
                                    ));
                        $keyboard->setResizeKeyboard(true);

                        Telegram::bot()->editMessageReplyMarkup([
                            'chat_id' => $cb_query->getMessage()->getChat()->getId(),
                            'message_id' => $cb_query->getMessage()->getMessageId(),
                            'reply_markup' => $keyboard,
                        ]);
                    }
                    break;
            }
        }

        try {
            Telegram::bot()->answerCallbackQuery($cb_answer_params);
        } catch (\Exception $ex) {
            \Log::error($ex);
        }
        if ($state && $state->chat_state === ChatState::STATE_ACTIVE) {
            dispatch(new ProcessChatState($state));
        }
    }

    public function index() {
        $webhook = $this->getWebhook();
//        dd($webhook);
        return view('bot.index', ['webhook' => $webhook]);
    }

    public function getWebhookInfo(Request $request = null) {
        return $this->getWebhook($request->get('update', false));
    }

    protected function getWebhook($force_update = false) {
        $webhook = \Cache::get('telegram-webhookinfo');
        if (!$webhook || $force_update) {
            $webhook = new WebhookInfo(Telegram::bot()->getWebhookInfo([])->getDecodedBody());
            \Cache::put('telegram-webhookinfo', $webhook, 60*24);
        }
        return $webhook;
    }

    /**
     * Sends your cretificate to telegram servers. It needs for communicating with your bot
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     * @throws \Exception if certificate file not found
     */
    public function sendCertificate()
    {
        $webhookParams = [
            'url' => 'https://lovelify.ru/bot/webhook/' . env('TELEGRAM_WEBHOOK_TOKEN', ''),
        ];
        $cert_path = storage_path('/ssl/ru.lovelify.pem');
        if (file_exists($cert_path))
            $webhookParams['certificate'] = $cert_path;

        Telegram::bot()->setWebhook($webhookParams);
        $apiResponse = Telegram::bot()->getLastResponse()->getDecodedBody();

        //update webhook info in cache
        $this->getWebhook(true);
        return back()->with('message', $apiResponse['description']);
    }

    /**
     * Sends getMe command and prints basic information about bot. I use it to check if bot is working
     *
     * @param Request $request
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function ping(Request $request) {
        $response = Telegram::bot()->getMe();

        $botId = $response->getId();
        $firstName = $response->getFirstName();
        $username = $response->getUsername();
        if(($request->ajax() && !$request->pjax()) || $request->wantsJson()) {
            return response()->json([
                "botId" => $botId,
                "firstName" => $firstName,
                "username" => $username,
            ]);
        } else {
            return response("\$botId => {{ $botId }} <br> \$firstName => {{ $firstName }} <br> \$username => {{ $username }}");
        }
    }

    /**
     * Messages
     */

    /**
     * Отправляет приветственное сообщение только что пришедшему пользователю
     * @param $chat
     * @param string $user_name
     */
    public function sendGreetingMessage($chat, $user_name = '')
    {
        $message = "Привет" . ($user_name ? " {$user_name}" : "") . "! ";
        $message .= "Добро пожаловать в группу!\n";
        $tgMessage = Telegram::bot()->sendMessage([
            'chat_id' => $chat->peer_id,
            'text' => $message,
        ]);

        ChatMessage::saveTgMessage($tgMessage, $chat->id, 0, true);
    }
}
