<?php

namespace App\Http\Controllers;

use App\Models\WorkflowStep;
use App\Models\Workflow;
use Illuminate\Http\Request;
use App\Models\File;
use Symfony\Component\HttpFoundation\File\Exception\FileException;

class WorkflowStepsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \App\Models\Workflow $workflow
     * @return \Illuminate\Http\Response
     */
    public function index(Workflow $workflow)
    {
        return response($workflow->steps_tree());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\Workflow $workflow
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Workflow $workflow)
    {
        $step = new WorkflowStep($request->all());
        $step->workflow_id = $workflow->id;
        $step->save();

        return response($step);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\WorkflowStep $workflowStep
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $workflow, WorkflowStep $step)
    {
        $data = $request->get('data');
        $file_to_del = null;
        $file_id = $step->data && property_exists($step->data, 'file_id') ? $step->data->file_id : null;
        if ($file_id && $data && $data['file_id'] != $file_id) {
            $file_to_del = File::find($step->data->file_id);
        }
        if ($step->update($request->except(['workflow_id'])) && $file_to_del) {
            $file_to_del->delete();
        }

        return response($step);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $workflow
     * @param  \App\Models\WorkflowStep $step
     * @return \Illuminate\Http\Response
     */
    public function destroy($workflow, WorkflowStep $step)
    {
        $step->delete();

        return response();
    }

    public function uploadFile(Request $request)
    {
        $uploaded_file = $request->file('file');
        if (!$uploaded_file)
            throw new \Exception("Файл не отправлен");
        if (!$uploaded_file->isValid())
            throw new \Exception("Ошибка загрузки файла: " . $uploaded_file->getErrorMessage());

        $file = $uploaded_file->move(public_path('upload/workflow/files'), $uploaded_file->hashName());
        $relative_path = str_replace(
            DIRECTORY_SEPARATOR,
            '/',
            str_replace(public_path(), '', $file->getPath())
        );

        $sizes = strpos($file->getMimeType(), "image/") === 0 ? getimagesize($file->getPathname()) : [0, 0];
        return File::create([
            'file_name' => $file->getFilename(),
            'dir_path' => $relative_path,
            'original_name' => $uploaded_file->getClientOriginalName(),
            'width' => $sizes[0],
            'height' => $sizes[1],
            'file_size' => $file->getSize(),
            'content_type' => $file->getMimeType(),
        ]);
    }

    public function deleteFile(Request $request, $workflow, WorkflowStep $step, File $file)
    {
        $stepData = $step->data;
        if ($stepData->file_id != $file->id) {
            throw new \Exception('This step file_id is not equals to requests file_id');
        }
        $file->delete();
        unset($stepData->file_id);
        unset($stepData->file_url);

        return [
            "status" => $step->update(['data' => $stepData]) ? "OK" : "Error"
        ];
    }
}
