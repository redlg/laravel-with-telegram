<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\UserField;
use App\Models\Workflow;
use App\Models\WorkflowStep;
use Illuminate\Http\Request;

class WorkflowController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $workflows = Workflow::all();
        if ($request->expectsJson()) {
            return response($workflows);
        }
        return view('workflow.index', [
            'workflows' => $workflows
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('workflow.edit', ['workflow' => new Workflow()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $workflow = new Workflow([
            "name" => $request->name
        ]);
        if ($request->get('main')) {
            Workflow::where('main', '=', '1')->update(['main' => 0]);
            $workflow->main = 1;
        }
        $workflow->save();
        // create initial step
        WorkflowStep::create([
            'workflow_id' => $workflow->id,
            'data' => new \stdClass(),
            'type' => WorkflowStep::TYPE_START,
        ]);

//        return $workflow->fresh();
        return redirect(route('workflow.show', $workflow->id));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Workflow  $workflow
     * @return \Illuminate\Http\Response
     */
    public function show(Workflow $workflow)
    {
        \PageParams::put('body_class', 't-body loading');

//        \App\Models\User::class;
        $fields = [
            'User' => $this->describeUsersTable(),
            'UserFields' => $this->describeUserFields(),
        ];

        return view('workflow.show', ['workflow' => $workflow, 'fields' => $fields]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Workflow  $workflow
     * @return \Illuminate\Http\Response
     */
    public function edit(Workflow $workflow)
    {
        return view('workflow.edit', ['workflow' => $workflow]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Workflow  $workflow
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Workflow $workflow)
    {
        if ($request->get('main') && !$workflow->main) {
            Workflow::where('main', '=', '1')->update(['main' => 0]);
        }
        $workflow->update($request->all());
        return redirect(route('workflow.edit', $workflow->id));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Workflow  $workflow
     * @return \Illuminate\Http\Response
     */
    public function destroy(Workflow $workflow)
    {
        $workflow->delete();
        return response(['message' => 'Сценарий удален']);
    }

    public function step_path(Request $request, Workflow $workflow)
    {
        /** @var WorkflowStep[] $allSteps */
        $allSteps = $workflow->steps->keyBy('id');
        $proceeded_steps = [];
        $step = $request->all();

        $queue = [
            $step
        ];

        while($step = array_shift($queue)) {
            $proceeded_steps[$step['id']] = $step['id'];

            $allSteps[$step['id']]->update($step);
            foreach($step['next'] as $next_step) {
                array_push($queue, $next_step);
            }
        }

        // delete all records not in the path
        $allSteps->whereNotIn('id', $proceeded_steps)->each(function($model) {
            /** @var WorkflowStep $model */
            $model->delete();
        });
    }

    protected function describeUsersTable()
    {
        return [
            'name' => [
                'name' => 'Имя',
                'field_name' => 'name',
                'field_type' => 'string',
                'settings' => [
                    'length' => 255
                ],
            ],
            'last_name' => [
                'name' => 'Фамилия',
                'field_name' => 'last_name',
                'field_type' => 'string',
                'settings' => [
                    'length' => 255
                ],
            ],
            'email' => [
                'name' => 'Email',
                'field_name' => 'email',
                'field_type' => 'string',
                'settings' => [
                    'length' => 255
                ],
            ],
        ];
    }

    protected function describeUserFields()
    {
        return UserField::where('ref_table', 'users')->get()
            ->mapWithKeys(function ($field) {
                return [$field['field_name'] => $field];
            });
    }
}
