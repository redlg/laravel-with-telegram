<?php

namespace App\Jobs;

use App\Traits\CreatesItself;
use App\Transport\Slack\Attachment;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class SendSlack implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels, CreatesItself;

    /**
     * @var Attachment[]
     */
    public $attachments = [];

    /**
     * @var string
     */
    public $text = '';

    /**
     * @var string|null Channel message to send to
     */
    public $channel = null;

    /**
     * @var string|null Message icon
     */
    public $icon = null;

    /**
     * Create a new job instance.
     *
     * @param $text
     * @param null $channel
     * @param array $attachments
     */
    public function __construct($text = '', $channel = null, $attachments = [])
    {
        $this->text = $text;
        $this->channel = $channel;
        $this->attachments = $attachments;
    }

    /**
     * Set `icon` field
     * @param string $value
     * @return $this
     */
    public function icon($value)
    {
        $this->icon = $value;
        return $this;
    }

    /**
     * Set `text` field
     * @param string $value
     * @return $this
     */
    public function text($value)
    {
        $this->text = $value;
        return $this;
    }

    /**
     * Set `channel` field
     * @param string $value
     * @return $this
     */
    public function channel($value)
    {
        $this->channel = $value;
        return $this;
    }

    /**
     * Set `attachments` field
     * @param Attachment[] $value
     * @return $this
     */
    public function attachments($value)
    {
        $this->attachments = $value;
        return $this;
    }

    /**
     * Add Attachment to attachments list
     * @param Attachment $attachment
     * @return $this
     */
    public function addAttachment(Attachment $attachment) {
        $this->attachments[] = $attachment;

        return $this;
    }

    /**
     * @param string $error_text
     * @param \Exception $ex
     * @return $this
     */
    public static function error($error_text, $ex = null)
    {
        if ($ex) {
            $error_text .= "\n[{$ex->getCode()}] {$ex->getMessage()}";
        }
        return static::make()->text($error_text)
            ->channel(config('slack.logs_channel'))
            ->icon(':bomb:')
            ->addAttachment(Attachment::make()->title('Stack')->text($ex->getTraceAsString()));
    }

    /**
     * @param string $warn_text
     * @return $this
     */
    public static function warning($warn_text)
    {
        return static::make()->text($warn_text)
            ->channel(config('slack.logs_channel'))
            ->icon(':warning:');
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        app('slack')->send($this->text, $this->channel, $this->icon, $this->attachments);
    }
}
