<?php

namespace App\Jobs;

use App\Models\ChatMessage;
use App\Models\ChatState;
use App\Models\WorkflowStep;
use App\Workflow\StepExecutor;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Pagination\Paginator;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Telegram\Bot\Exceptions\TelegramResponseException;
use App\Telegram\Keyboard;
use App\Telegram\Button;
use App\Telegram\ButtonCallbackData;

class ProcessChatState implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var ChatState
     */
    public $state;

    public $step_data;
    public $step_chat_message;

    /**
     * Create a new job instance.
     *
     * @param ChatState $state
     * @param mixed $step_data
     * @param mixed $step_chat_message
     */
    public function __construct(ChatState $state, $step_data = null, ChatMessage $step_chat_message = null)
    {
        $this->state = $state;
        $this->step_data = $step_data;
        $this->step_chat_message = $step_chat_message;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if ($this->state->chat_state === ChatState::STATE_POSTPONE) {
            $this->state->chat_state = ChatState::STATE_ACTIVE;
            $this->state->setProceedAt();
            $this->state->save();
        } elseif($this->state->chat_state === ChatState::STATE_WAIT && $this->state->next_notification_at && $this->state->next_notification_at < Carbon::now()) {
            if (!empty($this->state->chat_data["prev_chat_id"]) && !empty($this->state->chat_data["prev_message_id"])) {
                // hide old message keyboard
                try {
                    \Telegram::bot()->editMessageReplyMarkup([
                        'chat_id' => $this->state->chat_data["prev_chat_id"],
                        'message_id' => $this->state->chat_data["prev_message_id"],
                        'reply_markup' => '',
                    ]);
                } catch(TelegramResponseException $ex) {
                    // скорее всего ошибка неправильного chat_id или message_id либо "сообщение" не изменено
                    \Log::error($ex);
                }
            }

            $message_data = [
                'chat_id' => $this->state->chat->peer_id,
                'text' => $this->state->next_notification_text,
            ];

            // если `keys` === false ничего не делаем, кнопки не нужны
            // если ключ `keys` не установлен, то добавляем кнопки "по-умолчанию"
            // если `keys` - массив, то добавляем кнопки согласно настройкам
            if (!$this->state->next_notification_params
                || !isset($this->state->next_notification_params['keys'])
                || $this->state->next_notification_params['keys'] !== false) {

                $keyboard = Keyboard::make()->inline();
                if (!empty($this->state->next_notification_params['keys'])) {
                    $prev_actions = 0;
                    foreach($this->state->next_notification_params['keys'] as $key) {
                        if (!empty($key['action'])) {
                            $prev_actions |= Button::getActionConst($key['action']);
                        }
                    }

                    $keys = [];
                    foreach($this->state->next_notification_params['keys'] as $key) {
                        if (!empty($key['text'])) {
                            $btn = Button::make()->setText($key['text']);
                            if(!empty($key['action'])) {
                                $btn->setCallbackData(
                                    ButtonCallbackData::make()
                                        ->setAction(Button::getActionConst($key['action']))
                                        ->setPrevActions($prev_actions)
                                );
                            }
                            $keys[] = $keyboard->button($btn);
                        }
                    }
                    $keyboard->rowArray($keys);
                } else {
                    $keyboard->row(
                        Button::make()
                            ->setText('Продолжить программу')
                            ->setCallbackData(ButtonCallbackData::make()
                                ->setAction(Button::ACTION_CONTINUE)
                                ->setPrevActions(Button::ACTION_CONTINUE|Button::ACTION_DELAY)
                            ),
                        Button::make()
                            ->setText('Отложить')
                            ->setCallbackData(ButtonCallbackData::make()
                                ->setAction(Button::ACTION_DELAY)
                                ->setPrevActions(Button::ACTION_CONTINUE|Button::ACTION_DELAY)
                            )
                    );
                }
                $keyboard->setResizeKeyboard(true);
                $message_data['reply_markup'] = $keyboard;
            }

            $tg_message = \Telegram::bot()->sendMessage($message_data);
            $this->state->chat_data = [
                'prev_chat_id' => $tg_message->getChat()->getId(),
                'prev_message_id' => $tg_message->getMessageId(),
            ];
            $this->state->clearNotification();
            $this->state->save();
        } else {
            $step_executor = new StepExecutor($this->state);
            $step_executor->execute(function ($step_exec) {
                $step_exec->data = $this->step_data;
                $step_exec->chat_message = $this->step_chat_message;
            });
        }
        // continue dispatching while this chat state is active
        if ($this->state->chat_state == ChatState::STATE_ACTIVE)
            dispatch(new ProcessChatState($this->state));
    }
}
