<?php
/**
 * Brainify
 * Author: Kris (putalexey@redlg.ru)
 * Date: 26.06.2017 16:57
 */

namespace App\Workflow;


use App\Jobs\SendSlack;
use App\Models\ChatState;
use App\Models\WorkflowStep;
use App\Workflow\Steps\StepException;
use Carbon\Carbon;

class StepExecutor
{
    /**
     * @var ChatState
     */
    public $state;

    public function __construct(ChatState $state)
    {
        $this->state = $state;
    }

    public function execute($callback = null)
    {
        try {
            $stepClass = $this->getStepClass($this->state->workflow_step);
            if (!$stepClass) throw new \Exception('Class not found ' . $this->state->workflow_step->type);
            if (is_callable($callback)) {
                call_user_func($callback, $stepClass);
            }
            $stepClass->execute();
        } catch (\Exception $ex) {
            if (!($ex instanceof StepException)) {
                dispatch(SendSlack::make()
                    ->error("StepExecutor error", $ex)
                );
            }
        }
    }

    public function nextStep($callback = null)
    {
        $stepClass = $this->getStepClass($this->state->workflow_step);
        if (is_callable($callback)) {
            call_user_func($callback, $stepClass);
        }
        $stepClass->nextStep();
    }

    /**
     * @param WorkflowStep $step
     * @return Steps\Action|Steps\Base|Steps\GotoWorkflow|null
     */
    public function getStepClass($step)
    {
        $instance = null;

        switch($step->type) {
            case WorkflowStep::TYPE_START:
                return new Steps\Base($this->state);
            case WorkflowStep::TYPE_ACTION:
                return new Steps\Action($this->state);
            case WorkflowStep::TYPE_GOTO:
                return new Steps\GotoWorkflow($this->state);
            case WorkflowStep::TYPE_ANSWER:
                return new Steps\Answer($this->state);
            case WorkflowStep::TYPE_DELAY:
                return new Steps\Delay($this->state);
            case WorkflowStep::TYPE_CONDITION:
                return new Steps\Condition($this->state);
        }

        return $instance;
    }

    public function asd()
    {
        /** @var WorkflowStep $next_step */
        $next_step = $this->state->step;
        if ($next_step) {
            $this->state->workflow_id = $next_step->workflow_id;
            $this->state->workflow_step_id = $next_step->id;
            $this->state->proceed_at = new Carbon;
        }
    }
}