<?php
/**
 * Brainify
 * Author: Kris (putalexey@redlg.ru)
 * Date: 26.06.2017 17:01
 */

namespace App\Workflow\Steps;

use App\Jobs\SendSlack;
use App\Models\ChatMessage;
use App\Models\ChatState;
use App\Models\WorkflowStep;
use App\Transport\Slack\Attachment;
use Carbon\Carbon;
use Illuminate\Support\Arr;

class Base
{
    /**
     * @var ChatState
     */
    public $state;

    /**
     * @var mixed
     */
    public $data;

    /**
     * @var ChatMessage
     */
    public $chat_message;

    /**
     * @var WorkflowStep
     */
    public $step;

    public function __construct(ChatState $state)
    {
        $this->state = $state;
        $this->step = $state->workflow_step;
    }

    public function execute()
    {
        $this->nextStep();
        $this->state->save();
    }

    /**
     * set State params to execute next step ASAP
     * @param int $step_id
     */
    public function nextStep($step_id = null)
    {
        $next_item = $this->step->getNextById($step_id);
        if ($next_item !== null) {
            $this->state->workflow_step_id = $next_item->id;
            $this->state->chat_state = ChatState::STATE_ACTIVE;
            $this->state->setProceedAt();
            $this->state->clearNotification();
        } else
            $this->state->chat_state = ChatState::STATE_STOPPED;
    }
    
    protected function StepUnfinishedError($unfinished_fields = [], $throw=true, $append_defaults = true)
    {
        $this->StepError(
            "Ошибка, шаг {$this->step->id} не заполнен",
            [
                'Незаполненные поля' => Arr::wrap($unfinished_fields),
                'Шаг' => $this->step
            ],
            $throw,
            $append_defaults
        );
    }
    
    protected function StepError($text, $additional_data = [], $throw=true, $append_defaults = true)
    {
        $this->state->chat_state = ChatState::STATE_ERROR;
        $this->state->error_text = $text;
        if($throw) $this->state->save();

        if ($append_defaults && $this->step) {
            $text = "Ошибка в шаге `{$this->step->id}`. " . $text;
            $additional_data['Шаг'] = $this->step;
        }
        \Log::warning($text, $additional_data);

        $attachments = [];
        foreach($additional_data as $key => $value) {
            if (!is_string($value))
                $value = json_encode($value, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE);
            $attachments[] = Attachment::make()->title($key)->text($value);
        }
        dispatch(SendSlack::make()
            ->warning($text)
            ->attachments($attachments)
        );
        if ($throw)
            throw new StepException($text);
    }

    public function isActive() {
        return $this->isState(ChatState::STATE_ACTIVE);
    }

    public function isState($state) {
        return $this->state->chat_state == $state;
    }
}