<?php
/**
 * Brainify
 * Author: Kris (putalexey@redlg.ru)
 * Date: 26.06.2017 17:01
 */

namespace App\Workflow\Steps;


use App\Models\Assignment;
use App\Models\ChatMessage;
use App\Models\ChatState;
use App\Models\File;
use App\Models\Task;
use App\Models\TgFileCache;
use Carbon\Carbon;
use Illuminate\Support\Arr;
use App\Telegram\Keyboard;
use Illuminate\Support\Str;

class Action extends Base
{
    public function execute()
    {
        if ($this->isActive()) {
            if (property_exists($this->step->data, 'type')) {
                switch ($this->step->data->type) {
                    case 'send_message':
                        $this->processSendMessage();
                        break;
                    case 'update_custom_field':
                    case 'send_notification':
                        $this->nextStep();
                        break;
                }
            } else {
                $this->nextStep();
            }
            $this->state->save();
        }
    }

    public function processSendMessage()
    {
        if (property_exists($this->step->data, 'message_type')) {
            switch ($this->step->data->message_type) {
                case 'assignment_field':
                    if (property_exists($this->step->data, 'assignment_id') &&
                        property_exists($this->step->data, 'assignment_field')
                    ) {
                        $assignment = Assignment::find($this->step->data->assignment_id);
                        if (!$assignment) {
                            $this->StepError(
                                "Упражнение `{$this->step->data->assignment_id}` не найдено",
                                [ 'Упражнение' => $assignment ]
                            );
                            break;
                        }
                        if ($this->step->data->assignment_field == "task") {
                            if (!property_exists($this->step->data, 'task_id'))
                                $this->StepUnfinishedError('task_id');
                            $task = Task::find($this->step->data->task_id);
                            if (!$task)
                                $this->StepError("Задание `{$this->step->data->task_id}` не найдено");
                            if (!$task->chat_text)
                                $this->StepError("Текст задания для отправки не задан");

                            $message = $task->chat_text;
                        } else {
                            $field = $this->step->data->assignment_field;
                            if (!array_key_exists($field, $assignment->getAttributes())) {
                                $this->StepError(
                                    "Ошибка в шаге `{$this->step->id}`. Свойство `$field` не найдено в упражнении",
                                    ['Упражнение' => $assignment]
                                );
                                break;
                            }
                            $message = $assignment->getAttribute($field);
                            if(!$message)
                                $this->StepError("Поле `$field` упражнения `{$assignment->id}` пустое");
                        }
                        $message_data = [
                            'chat_id' => $this->state->chat->peer_id,
                            'text' => $message,
                        ];

                        $this->setKeyboard($message_data);

                        $tgMessage = \Telegram::bot()->sendMessage($message_data);
                    } else {
                        $this->StepUnfinishedError();
                    }
                    break;
                case 'text':
                    if (property_exists($this->step->data, 'value')) {
                        $message = $this->step->data->value;
                        $message = $this->processMessageTags($message);
                        $message_data = [
                            'chat_id' => $this->state->chat->peer_id,
                            'text' => $message,
                        ];

                        $this->setKeyboard($message_data);

                        $tgMessage = \Telegram::bot()->sendMessage($message_data);
                    } else {
                        $this->StepUnfinishedError();
                    }
                    break;
                case 'image':
                    if (property_exists($this->step->data, 'file_id')) {
                        $file = File::find($this->step->data->file_id);
                        $cache_image = TgFileCache::find($file->id);

                        $message_data = [
                            'chat_id' => $this->state->chat->peer_id,
                        ];
                        if ($cache_image) {
                            $message_data['photo'] = $cache_image->telegram_file_id;
                        } else {
                            $message_data['photo'] = public_path($file->url);
                        }
                        if (property_exists($this->step->data, 'value') && $this->step->data->value) {
                            $message_data['caption'] = $this->step->data->value;
                        }

                        $this->setKeyboard($message_data);

                        $tgMessage = \Telegram::bot()->sendPhoto($message_data);
                        if (!$cache_image) {
                            $photo = Arr::first($tgMessage->getPhoto());
                            $this->saveCacheFile($file->id, $photo['file_id']);
                        }
                    } else {
                        $this->StepUnfinishedError();
                    }
                    break;
                case 'video':
                    if (property_exists($this->step->data, 'file_id')) {
                        $file = File::find($this->step->data->file_id);
                        $cache_image = TgFileCache::find($file->id);

                        $message_data = [
                            'chat_id' => $this->state->chat->peer_id,
                        ];
                        if ($cache_image) {
                            $message_data['video'] = $cache_image->telegram_file_id;
                        } else {
                            $message_data['video'] = public_path($file->url);
                        }
                        if (property_exists($this->step->data, 'value') && $this->step->data->value) {
                            $message_data['caption'] = $this->step->data->value;
                        }

                        $this->setKeyboard($message_data);

                        $tgMessage = \Telegram::bot()->sendVideo($message_data);
                        if (!$cache_image) {
                            $this->saveCacheFile($file->id, $tgMessage->getVideo()->file_id);
                        }
                    } else {
                        $this->StepUnfinishedError();
                    }
                    break;
                case 'sticker':
                    // TODO доделать
                    $this->StepError("Отправка сообщения типа `{$this->step->data->message_type}` не реализовано");
                    break;
            }
            if (isset($tgMessage)) {
                $this->state->chat_data = collect($this->state->chat_data)->merge([
                    'prev_chat_id' => $tgMessage->getChat()->getId(),
                    'prev_message_id' => $tgMessage->getMessageId()
                ])->all();
                ChatMessage::saveTgMessage($tgMessage, $this->state->chat->id, 0, true);
            }
            if ($this->needNextStep())
                $this->nextStep();
        }
    }

    protected function setKeyboard(&$message_data)
    {
        if ($this->step->data->keyboard_show) {
            $keyboard = Keyboard::make()->inline()->setKeys($this->step->data->keyboard_keys);
            $message_data['reply_markup'] = $keyboard;
            $this->state->chat_state = ChatState::STATE_WAIT;
            $this->state->setNotification("Куда вы подевались?", Carbon::now()->addDay());
        }
    }
    protected function needNextStep()
    {
        return !$this->step->data->keyboard_show;
    }

    protected function saveCacheFile($file_id, $tg_file_id)
    {
        return TgFileCache::create([
            'file_id' => $file_id,
            'telegram_file_id' => $tg_file_id,
        ]);
    }

    public static $message_tags = [
        'user.login' => 'tagUserLogin',
        'user.new_password' => 'tagUserNewPassword',
    ];

    public function processMessageTags($message)
    {
        return preg_replace_callback('/{([^}]*)}/', function($matches) {
            $parts = explode(":", $matches[1], 2);
            $action = Arr::get($parts, 0, '');
            if (!empty($parts[1])) {
                $params = $parts[1] ? explode(":", $parts[1]) : [];
            } else {
                $params = [];
            }

            if (array_key_exists($action, static::$message_tags)) {
                $method = static::$message_tags[$action];
            } else {
                $method = "tag" . studly_case($action);
            }
            if (method_exists($this, $method)) {
                return $this->{$method}($matches[0], $params);
            }
            return $matches[0];
        }, $message);
    }

    protected function tagUserLogin() {
        return $this->state->user->email;
    }

    protected function tagUserNewPassword() {
        $user = $this->state->user;
        $new_password = Str::random(6);
        $user->password = bcrypt($new_password);
        $user->save();
        return $new_password;
    }

    protected function tagUrl($tag, $params) {
        if (count($params)) {
            return url($params[0]);
        }
        return url("");
    }

    protected function tagRoute($tag, $params) {
        if (count($params) > 0) {
            $route = \Route::getRoutes()->getByName($params[0]);
            if ($route)
                return url($route->uri());
        }
        return $tag;
    }
}