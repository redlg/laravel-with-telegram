<?php
/**
 * Brainify
 * Author: Kris (putalexey@redlg.ru)
 * Date: 26.06.2017 17:01
 */

namespace App\Workflow\Steps;


use App\Models\ChatMessage;
use App\Models\ChatState;
use App\Models\File;
use App\Models\Task;
use App\Models\TaskAnswer;
use App\Models\Workflow;
use App\Models\WorkflowStep;
use Carbon\Carbon;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Telegram\Bot\Objects\Audio;
use Telegram\Bot\Objects\Document;
use Telegram\Bot\Objects\PhotoSize;
use Telegram\Bot\Objects\Video;
use Telegram\Bot\Objects\Voice;

class Condition extends Base
{
    public function execute()
    {
        if ($this->isActive()) {
            if (!$this->checkValues()) {
                $this->StepUnfinishedError();
            } else {
                $next_step_yes = property_exists($this->step->data, 'next_yes') && $this->step->data->next_yes ? $this->step->data->next_yes : false;
                $next_step_no = property_exists($this->step->data, 'next_no') && $this->step->data->next_no ? $this->step->data->next_no : false;

                if ($this->checkCondition()) {
                    $this->nextStep($next_step_yes);
                } else {
                    $this->nextStep($next_step_no);
                }
                $this->state->proceed_at = Carbon::now();
            }
            $this->state->save();
        }
    }

    protected function checkCondition()
    {
        if ($this->step->data->type == "user_field") {
            return $this->checkConditionUserField();
        } elseif ($this->step->data->type == "task_answer") {
            return $this->checkConditionTaskAnswer();
        }
        throw new \Exception("Not implemented condition type {$this->step->data->type}");

    }
    protected function checkConditionUserField()
    {
        $user = $this->state->user;
        $value = Arr::get($user, $this->step->data->field_name);
        return $this->checkExpression($value, $this->step->data->condition, $this->step->data->condition_value);
    }

    protected function checkExpression($field_value, $cond, $check_value = null)
    {
        switch($cond) {
            case "eq":
                return $field_value == $check_value;
                break;
            case "lt":
                return $field_value < $check_value;
                break;
            case "gt":
                return $field_value > $check_value;
                break;
            case "lt_eq":
                return $field_value <= $check_value;
                break;
            case "gt_eq":
                return $field_value >= $check_value;
                break;
            case "not_empty":
                return empty($field_value);
                break;
            case "empty":
                return !empty($field_value);
                break;
            case "starts_with":
                return Str::startsWith($field_value, $check_value);
                break;
            case "ends_with":
                return Str::endsWith($field_value, $check_value);
                break;
            case "contains":
                return Str::contains($field_value, $check_value);
                break;
        }
        return false;
    }
    protected function checkConditionTaskAnswer()
    {
        throw new \Exception("Not implemented condition type `task answer`");
        return true;
    }


    protected function checkValues()
    { //TODO Add check on empty fields
        if (empty($this->step->data->type)) {
            return false;
        }
        if ($this->step->data->type == "user_field") {
            if (empty($this->step->data->field_name)) {
                return false;
            }
        } elseif ($this->step->data->type == "task_answer") {
            if (empty($this->step->data->task_id)) {
                return false;
            }
        } else {
            return false;
        }
        if (empty($this->step->data->condition)) {
            return false;
        }
        return true;
    }
    protected function field_empty() {

    }
}