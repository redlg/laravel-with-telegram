<?php
/**
 * Brainify
 * Author: Kris (putalexey@redlg.ru)
 * Date: 26.06.2017 17:01
 */

namespace App\Workflow\Steps;


use App\Jobs\SendSlack;
use App\Models\ChatMessage;
use App\Models\ChatState;
use App\Models\File;
use App\Models\Task;
use App\Models\TaskAnswer;
use Carbon\Carbon;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Telegram\Bot\Objects\Audio;
use Telegram\Bot\Objects\Document;
use Telegram\Bot\Objects\PhotoSize;
use Telegram\Bot\Objects\Video;
use Telegram\Bot\Objects\Voice;

class Answer extends Base
{
    public function execute()
    {
        if ($this->isActive()) {
            $this->state->chat_state = ChatState::STATE_WAIT;
            $this->state->setNotification("Что-то вас давно не видно. Продолжаем?", Carbon::now()->addDay(), [
                'keys' => [
                    [
                        "text" => "Отложить",
                        "action" => "delay",
                    ]
                ]
            ]);
            $this->state->save();
        } elseif($this->isState(ChatState::STATE_WAIT) || $this->isState(ChatState::STATE_POSTPONE)) {
            /** @var \Telegram\Bot\Objects\Message $data */
            $data = $this->data;
            if ($data) {
                if (property_exists($this->step->data, 'task_id') && $this->step->data->task_id) {
                    $task = Task::find($this->step->data->task_id);
                    if (!$task) {
                        $this->StepError("Задание {$this->step->data->task_id} не найдено");
                    } else {
                        $attributes = [
                            'user_id' => $this->state->user_id,
                            'task_id' => $task->id,
                            'answer' => $data->getText() ?: "",
                            'answer_type' => ChatMessage::getMessageType($data),
                        ];

                        if (in_array($attributes['answer_type'], ['photo', 'audio', 'voice', 'video', 'video_note', 'document'])) {
                            $attributes['answer'] = $data->get('caption', $attributes['answer']);

                            /** @var PhotoSize[]|Video|Voice|Document|Audio $object */
                            $object = $data->get($attributes['answer_type']);
                            if ($attributes['answer_type'] == 'photo') {
                                $best_photo = Arr::first($object);
                                foreach($object as $obj) {
                                    if (!empty($best_photo['file_size'])
                                        && !empty($obj['file_size'])
                                        && $best_photo['file_size'] < $obj['file_size']) {
                                        $best_photo = $obj;
                                    }
                                }
                                $object = new PhotoSize($best_photo);
                            }

                            $db_file = static::downloadFile($object, $data->get('caption', $attributes['answer']));
                            if ($db_file)
                                $attributes['file_id'] = $db_file->id;
                        }
                        if ($this->chat_message) {
                            $attributes['chat_message_id'] = $this->chat_message->id;
                        }
                        TaskAnswer::create($attributes);
                    }
                    $this->nextStep();
                    $this->state->clearNotification();
                    $this->state->chat_state = ChatState::STATE_ACTIVE;
                } else {
                    $this->StepUnfinishedError();
                }
            }
            $this->state->save();
        }
    }

    /**
     * @param PhotoSize|Video|Voice|Document|Audio $object
     * @return File|false
     */
    public static function downloadFile($object, $caption = '')
    {
        try {
            $file_info = \Telegram::bot()->getFile(['file_id' => $object->getFileId()]);
            $contents = file_get_contents('https://api.telegram.org/file/bot' . config('telegram.bot_token') . '/' . $file_info->getFilePath());
            $file_name = Str::random(40) . "_" . basename($file_info->getFilePath());

            $dir_path = '/upload/workflow/files/tg';
            $path = public_path($dir_path . '/' . $file_name);
            if (!\File::isDirectory(public_path($dir_path))) {
                \File::makeDirectory(public_path($dir_path), 0755, true, true);
            }
            \File::put($path, $contents);
            return File::create([
                'dir_path' => $dir_path,
                'original_name' => $file_name,
                'file_name' => $file_name,
                'width' => $object->get('width', 0),
                'height' => $object->get('height', 0),
                'file_size' => $file_info->get('file_size', 0),
                'content_type' => mime_content_type($path),
                'description' => $caption,
            ]);
        } catch(\Exception $ex) {
            dispatch(SendSlack::error($ex->getMessage(), $ex));
            return false;
        }
    }
}