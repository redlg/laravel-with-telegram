<?php
/**
 * Brainify
 * Author: Kris (putalexey@redlg.ru)
 * Date: 30.06.2017 14:08
 */

namespace App\Facades;


use Illuminate\Support\Facades\Facade;

class PageParamsFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'page-params';
    }
}