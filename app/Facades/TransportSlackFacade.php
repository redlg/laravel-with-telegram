<?php
/**
 * Created by PhpStorm.
 * User: RedLine
 * Date: 09.09.2016
 * Time: 10:40
 */

namespace App\Facades;

use Illuminate\Support\Facades\Facade;

class TransportSlackFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'slack';
    }
}