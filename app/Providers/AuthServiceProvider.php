<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Laravel\Passport\Passport;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
        Gate::define('request-help', function($user) {
            return true;
        });
        Gate::define('access-program', function($user) {
            return true;
        });
        Gate::define('restart-program', function($user) {
            return $user->inGroups(['administrators']);
        });
        Gate::define('admin-bot', function($user) {
            return $user->inGroups(['administrators']);
        });

        Passport::routes();
    }
}
