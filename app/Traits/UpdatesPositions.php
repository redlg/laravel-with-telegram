<?php
/**
 * Brainify
 * Author: Kris (putalexey@redlg.ru)
 * Date: 20.06.2017 11:23
 */

namespace App\Traits;

trait UpdatesPositions
{
    public static function updatePositions() {
        \DB::transaction(function() {
            $items = static::orderBy('position')->get();
            $position = 1;
            foreach($items as $item) {
                $item->update(['position' => $position]);
                $position+=2;
            }
        });
    }
}