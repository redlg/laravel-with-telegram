<?php
/**
 * Brainify
 * Author: Kris (putalexey@redlg.ru)
 * Date: 23.06.2017 12:38
 */

namespace App\Traits;

/**
 * Creates instance of class with `make` function
 * @package App\Traits
 */
trait CreatesItself
{
    public static function make()
    {
        return new static();
    }
}