<?php
/**
 * Brainify
 * Author: Kris (putalexey@redlg.ru)
 * Date: 20.06.2017 11:36
 */

namespace App\Traits;


trait GeneratesSelectPositions
{
    /**
     * @param \Illuminate\Database\Eloquent\Collection|static[] $items
     * @param \Illuminate\Database\Eloquent\Model $item
     * @return \Illuminate\Database\Eloquent\Collection
     */
    protected function getSelectPositions($items, $item)
    {
        $max_position = -1;
        $select_positions = $items->mapWithKeys(function($value) use ($item, &$max_position) {
            $max_position = max($max_position, $value->position);
            if ($item->id == $value->id)
                return [$value->position => 'Без изменений'];
            else
                return [$value->position + 1 => 'После ' . $value->name];
        });
        $select_positions->prepend('В начало', 0);
        $select_positions[$max_position + 2] = 'В конец';

        return $select_positions;
    }
}