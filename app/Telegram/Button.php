<?php
/**
 * Brainify
 * Author: Kris (putalexey@redlg.ru)
 * Date: 12.07.2017 13:26
 */

namespace App\Telegram;

use \Telegram\Bot\Keyboard\Button as TgButton;

class Button extends TgButton
{
    const ACTION_CONTINUE = 1;
    const ACTION_DELAY = 2;

    const DELAY_PERIOD_DAY = 1;
    const DELAY_PERIOD_WEEK = 2;
    const DELAY_PERIOD_MONTH = 3;

    public static function getActionConst($action)
    {
        switch($action) {
            case 'continue':
                return static::ACTION_CONTINUE;
            case 'delay':
                return static::ACTION_DELAY;
        }
        return $action;
    }

    public function setCallbackData($string)
    {
        if (!is_string($string))
            $string = json_encode($string);
        $this->items['callback_data'] = $string;

        return $this;
    }

    public function getCallbackData()
    {
        return @json_decode($this->get('callback_data', '[]'), true);
    }
}