<?php
/**
 * Brainify
 * Author: Kris (putalexey@redlg.ru)
 * Date: 13.07.2017 12:56
 */

namespace App\Telegram;


use Illuminate\Support\Collection;

/**
 * Class ButtonCallbackData
 * @package App\Telegram
 * @method $this setAction($action)
 * @method $this setPeriod($period)
 * @method $this setPrevActions($prev_periods)
 * @method int getAction($default = null)
 * @method int getPeriod($default = null)
 * @method int getPrevActions($default = null)
 */
class ButtonCallbackData extends Collection
{
    protected $specific_attributes = [
        'Action' => 'a',
        'Period' => 'p',
        'PrevActions' => 'pa',
    ];

    /**
     * Dynamically build params.
     *
     * @param string $method
     * @param array  $args
     *
     * @return $this
     */
    public function __call($method, $args)
    {
        $action = substr($method, 0, 3);

        if ($action === 'set') {
            $property = substr($method, 3);

            if (array_key_exists($property, $this->specific_attributes)) {
                $this->items[$this->specific_attributes[$property]] = $args[0];
            } else {
                $property = snake_case($property);
                $this->items[$property] = $args[0];
            }
            return $this;
        } elseif ($action === 'get') {
            $property = substr($method, 3);

            if (array_key_exists($property, $this->specific_attributes)) {
                $default = isset($args[0]) ? $args[0] : null;
                return $this->get($this->specific_attributes[$property], $default);
            }
        }

        return parent::__call($method, $args);
    }
}