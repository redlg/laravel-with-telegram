<?php
/**
 * Brainify
 * Author: Kris (putalexey@redlg.ru)
 * Date: 12.07.2017 13:24
 */

namespace App\Telegram;

use \Telegram\Bot\Keyboard\Keyboard as TgKeyboard;

class Keyboard extends TgKeyboard
{
    public function rowArray($buttons) {
        $property = 'keyboard';
        if ($this->isInlineKeyboard()) {
            $property = 'inline_keyboard';
        }

        $this->items[$property][] = $buttons;

        return $this;
    }

    public function setKeys($keys, $key_modifier = null) {
        $keyboard_keys = [];
        foreach($keys as $key) {
            $key_data = [
                'text' => $key->text,
                'callback_data' => [
                    'a' => Button::getActionConst($key->action)
                ],
            ];
            if ($key_modifier && is_callable($key_modifier)) {
                call_user_func($key_modifier, $key, $key_data);
            }
            $key_data['callback_data'] = json_encode($key_data['callback_data']);
            $keyboard_keys[] = $this->button($key_data);
        }
        $this->rowArray($keyboard_keys);
        $this->setResizeKeyboard(true);

        return $this;
    }
}