<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use Illuminate\Http\Request;


Route::get('/test2', "TelegramBotController@getUpdates");
Route::get('/test3', function (Request $request) {
    $user = \App\Models\User::where('email', 'putalexey@redlg.ru')->first();
    return response($user->getBotAddingUrl());
//    var_dump($_method->getDocComment());
//    var_dump(Telegram::sendMessage([
//        'chat_id' => 260019765,
//        'text' => 'test sending message'
//    ]));
});
Route::get('/test', function (Request $request) {
    echo "<form method='post'>
        " . csrf_field() . "
        <input type='submit' value='шаг'>
        </form>";

    echo "<div style='display: flex;'>";
        echo "<pre style='max-width: 50%;overflow: auto;'>";
        echo json_encode(
            \App\Models\ChatState::where('user_id', Auth::user()->id)
                ->without(['user', 'workflow.steps'])->first(),
                JSON_PRETTY_PRINT|JSON_UNESCAPED_UNICODE
        );
        echo "</pre>";
        echo "<pre style='max-width: 50%;overflow: auto;'>";
        echo json_encode(
            \App\Models\ChatStatesHistory::where('user_id', Auth::user()->id)
                ->with(['chat', 'workflow', 'workflow_step'])
                ->without(['workflow.steps'])
                ->orderBy('id', 'desc')->limit(1)->offset(1)
                ->without(['workflow.steps'])->first(),
                JSON_PRETTY_PRINT|JSON_UNESCAPED_UNICODE
        );
        echo "</pre>";
    echo "</div>";
});
Route::post('/test', function (Request $request) {
    \Carbon\Carbon::setTestNow(\Carbon\Carbon::now()->addDay());
//    $state = \App\Models\ChatState::where('chat_state', \App\Models\ChatState::STATE_STOPPED)->where('user_id', Auth::user()->id)->first();
    $state = \App\Models\ChatState::where('chat_state', "!=", \App\Models\ChatState::STATE_STOPPED)->where('user_id', Auth::user()->id)->first();
    if ($state)
        dispatch((new \App\Jobs\ProcessChatState($state))->onConnection('sync'));

    return back();
});
Route::get('/', function (Request $request) {
    $price = $request->get("option") == '09ea221d3db11df1f369094ffb4bda7c' ? '16900' : '9900';
    return view('landing', ["price" => $price]);
});
Route::any('/chat/', 'LandingChatController@index');
Route::get('/chat/email/', 'LandingChatController@getOurEmail');
Route::post('/chat/save/', 'LandingChatController@save');

// управление ботом
Route::group(['prefix' => 'bot'], function () {
    Route::post('webhook/{token}', 'TelegramBotController@webhook');
    Route::group(['middleware' => 'can:admin-bot'], function() {
        Route::get('/', 'TelegramBotController@index')->name('bot.index');
        Route::get('webhook', 'TelegramBotController@getWebhookInfo')->name('bot.webhook');
        Route::post('test', 'TelegramBotController@ping')->name('bot.ping');
        Route::post('init', 'TelegramBotController@sendCertificate')->name('bot.init');
    });
});


Auth::routes();
Route::group(["middleware" => 'auth' ], function() {
//    Route::get('/workflows/{workflow_id}/steps', "WorkflowController@steps");
    Route::get('/lk/', function (Request $request) {
        $sections = \App\Models\Section::ordered()
            ->with(['assignments', 'assignments.tasks'])
            ->get();
        $answers = \App\Models\TaskAnswer::where('user_id', Auth::user()->id)->get()->keyBy('task_id');
        return view('cabinet.assignments', [
            "sections" => $sections,
            "answers" => $answers,
        ]);
    })->name('cabinet');
    Route::put('workflow/{workflow}/step_path', "WorkflowController@step_path")->name('workflow.step_path');
    Route::post('workflow/{workflow}/steps/{step}/file', "WorkflowStepsController@uploadFile")->name('workflow.steps.file.upload');
    Route::delete('workflow/{workflow}/steps/{step}/file/{file}', "WorkflowStepsController@deleteFile")->name('workflow.steps.file.delete');

    Route::get('assignment', "AssignmentController@all")->name('assignment.all');
    Route::resource('section', "SectionController");
    Route::resource('section.assignment', "AssignmentController");
    Route::resource('section.assignment.task', "TaskController");

    Route::resource('workflow', "WorkflowController");
    Route::resource('workflow.steps', "WorkflowStepsController", [
        'except' => ['create', 'show', 'edit',]
    ]);
});

Route::get('/home', 'HomeController@index');
