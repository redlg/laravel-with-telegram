const path = require('path');
const webpack = require('webpack');
const srcPath = path.resolve(__dirname, 'resources/assets/js/react');
const buildPath = path.resolve(__dirname, 'public');
const nodeModulesPath = path.resolve(__dirname, 'node_modules');

var config = {
    entry: [
        // activate HMR for React
        'react-hot-loader/patch',

        // bundle the client for webpack-dev-server
        // and connect to the provided endpoint
        'webpack-dev-server/client?http://lovelify.local:8080',

        // bundle the client for hot reloading
        // only- means to only hot reload for successful updates
        'webpack/hot/only-dev-server',

        // app entry point
        './resources/assets/js/react/app.jsx',

        // styles entry point
        './resources/assets/scss/app.scss',
    ],
    devtool: 'inline-source-map',
    output: {
        path: buildPath,
        filename: 'assets/app.min.js',

        publicPath: 'http://localhost:8080/'
        // necessary for HMR to know where to load the hot update chunks
    },
    module: {
        rules: [
            {
                test: /\.jsx?$/,
                // include: [
                //     path.resolve(__dirname, 'node_modules'),
                //     path.resolve(__dirname, "resources/assets/js/react")
                // ],
                exclude: [nodeModulesPath],

                loader: "babel-loader",
                // the loader which should be applied, it'll be resolved relative to the context
                // -loader suffix is no longer optional in webpack2 for clarity reasons
                // see webpack 1 upgrade guide

                options: {
                    presets: [
                        "react",
                        ["es2015", {"modules": false}],
                        "stage-1"
                    ],
                    plugins: [
                        [
                            "transform-decorators-legacy",
                            "react-hot-loader/babel",
                            // "transform-object-assign",
                            "transform-replace-object-assign",
                            "simple-assign",
                        ],
                        [
                            "babel-plugin-root-import",
                            [{
                                "rootPathPrefix": "@",
                                "rootPathSuffix": "resources/assets/js/react"
                            }, {
                                "rootPathPrefix": "#",
                                "rootPathSuffix": "resources/assets/js/react/stores"
                            }]
                        ]
                    ]
                }
                // options for the loader
            }, {
                test: /\.scss$/,
                loader: 'style-loader!css-raw-loader!postcss-loader!sass-loader?sourceMap'
            }
        ]
    },
    plugins: [
        // enable HMR globally
        new webpack.HotModuleReplacementPlugin(),

        // prints more readable module names in the browser console on HMR updates
        new webpack.NamedModulesPlugin(),
        new webpack.ProvidePlugin({
            'Promise': 'es6-promise', // Thanks Aaron (https://gist.github.com/Couto/b29676dd1ab8714a818f#gistcomment-1584602)
            'fetch': 'whatwg-fetch'
        }),

        new webpack.LoaderOptionsPlugin({
            test: /\.scss$/,
            options: {
                postcss: [
                    require('precss'),
                    require('autoprefixer')
                ],
                context: './'
            }
        })
    ],
    resolve: {
        extensions: [".jsx", ".js", ".json", "*"],
        modules: [
            srcPath,
            'node_modules'
        ]
    },
    devServer: {
        headers: { "Access-Control-Allow-Origin": "*" },
        host: 'lovelify.local',
        contentBase: buildPath,
        publicPath: 'http://localhost:8080/',
        hot: true,
        inline: true,
        compress: true
    },
};

module.exports = config;