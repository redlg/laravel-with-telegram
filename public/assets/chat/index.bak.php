<?/**
 * Brainify
 * Author: Kris (putalexey@redlg.ru)
 * Date: 14.04.2017 12:28
 */
$GLOBALS["BUY_PAGE"] = "Y";
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Lovelify — Чат ");

if($_REQUEST["email"]) {
    $userEmail = strtolower(trim($_REQUEST["email"]));
    setcookie('email', $userEmail, time() + 3600*7, '/');
    $_COOKIE["email"] = $userEmail;
    LocalRedirect($APPLICATION->GetCurPage());
} else {
    $userEmail = $_COOKIE["email"];
}
$asset = \Bitrix\Main\Page\Asset::getInstance();
CJSCore::Init(['jquery_expanding']);
$asset->addCss('/changeyourself/assets/css/animate.min.css');
$asset->addCss('/love/chat/assets/css/style.css', true);
$asset->addJs('/changeyourself/assets/js/libs/wow.min.js');
$asset->addJs('/love/chat/assets/script.js');
if(!$userEmail) {
    LocalRedirect("/love/");
}
?>

<section class="b-connect">
    <div class="b-connect__inner">
        <form id="chat_form" action="/love/chat/ajax.php" method="post">
            <?=bitrix_sessid_post()?>
            <input type="hidden" name="email" value="<?=$userEmail?>">
            <input type="hidden" name="email" value="<?=$price?>">
            <input type="hidden" name="action" value="form.save">
            <div class="chat">
                <!-- page_1 -->
                <div class="chat__item" id="page_1">
                    <div class="chat__item-inner">
                        <div class="chat-question">
                            <div class="chat-question__person chat-person wow fadeInDown" style="animation-delay:400ms;">
                                <img class="chat-person__photo" src="/love/chat/assets/img/ira-large.png" alt="Ира"/>
                                <p class="chat_person__name">Ира</p>
                            </div>
                            <div class="chat-question__message wow fadeInLeft">
                                Привет! Здорово, что ты стремишься к осознанным отношениям. Давай знакомиться, чтобы мы могли лучше тебе помочь.
                            </div>
                        </div>
                        <div class="chat-answer">
                            <div class="chat-email-request">
                                <input class="chat-text" type="text" name="fio" placeholder="Как тебя зовут?">
                                <div class="chat-email-request__checkboxes">
                                    Пол:
                                    <div class="chat-email-request__checkbox">
                                        <input id="agree_to_process_m" class="chat-email-request__checkbox__input" type="radio" name="gender" value="M">
                                        <label for="agree_to_process_m">М</label>
                                    </div>
                                    <div class="chat-email-request__checkbox">
                                        <input id="agree_to_process_f" class="chat-email-request__checkbox__input" type="radio" name="gender" value="F">
                                        <label for="agree_to_process_f">Ж</label>
                                    </div>
                                </div>
                            </div>
                            <button type="button" class="chat-button js-next">Продолжить</button>
                        </div>
                    </div>
                </div>

                <!-- page_2 -->
                <div class="chat__item chat__item--hidden" id="page_2">
                    <div class="chat__item-inner">
                        <div class="chat-question">
                            <div class="chat-question__person chat-person wow fadeInDown" style="animation-delay:400ms;">
                                <img class="chat-person__photo" src="/love/chat/assets/img/ira-large.png" alt="Ира"/>
                                <p class="chat_person__name">Ира</p>
                            </div>
                            <div class="chat-question__message wow fadeInLeft">
                                В каких ты сейчас отношениях?
                            </div>
                        </div>
                        <div class="chat-answer">
                            <div class="radio-buttons">
                                <label class="radio-buttons__button js-next">
                                    <input type="radio" name="state" value="free">
                                    <span>Свободен</span>
                                </label>

                                <label class="radio-buttons__button js-next">
                                    <input type="radio" name="state" value="dating">
                                    <span>Встречаюсь</span>
                                </label>

                                <label class="radio-buttons__button js-next">
                                    <input type="radio" name="state" value="mariage">
                                    <span>В браке</span>
                                </label>

                                <label class="radio-buttons__button js-next">
                                    <input type="radio" name="state" value="complicated">
                                    <span>Всё сложно</span>
                                </label>

                                <label class="radio-buttons__button js-next">
                                    <input type="radio" name="state" value="broke_up">
                                    <span>Расстался</span>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- page_3 -->
                <div class="chat__item chat__item--hidden" id="page_3">
                    <div class="chat__item-inner">
                        <div class="chat-question">
                            <div class="chat-question__person chat-person wow fadeInDown" style="animation-delay:400ms;">
                                <img class="chat-person__photo" src="/love/chat/assets/img/ira-large.png" alt="Ира"/>
                                <p class="chat_person__name">Ира</p>
                            </div>
                            <div class="chat-question__message wow fadeInLeft">
                                Уточни свою ситуацию, чтобы мы смогли лучше тебе помочь.
                            </div>
                        </div>
                        <div class="chat-answer">
                            <textarea name="details" class="chat-textarea expanding" placeholder="Начни писать здесь"></textarea>
                            <button type="submit" class="chat-button js-submit">Завершить</button>
                        </div>
                    </div>
                </div>

                <!-- page_4 -->
                <div class="chat__item chat__item--hidden" id="page_4">
                    <div class="chat__item-inner">
                        <div class="chat-question">
                            <div class="chat-question__person chat-person wow fadeInDown" style="animation-delay:400ms;">
                                <img class="chat-person__photo" src="/love/chat/assets/img/ira-large.png" alt="Ира"/>
                                <p class="chat_person__name">Ира</p>
                            </div>
                            <div class="chat-question__message wow fadeInLeft">
                                Отлично! Запуск в мае.
                                <br>
                                А пока мы на связи на <a id="lovely_email"></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</section>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
