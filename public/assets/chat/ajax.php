<?php
/**
 * Brainify
 * Author: Kris (putalexey@redlg.ru)
 * Date: 17.04.2017 15:10
 */
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
class LovelyLanding {
    const ERROR_WRONG_BITRIX_SESSID = 1;
    const ERROR_FUNCTION_NOT_FOUND = 2;
    const ERROR_FORM_FIELD_EMPTY = 4;
    const ERROR_FORM_FIELD_INVALID = 5;
    const ERROR_SAVE_ERROR = 6;

    public function processAjax()
    {
        $result = [];
        try {
            $method = $_SERVER["REQUEST_METHOD"] == "POST" ? 'post' : 'get';
            if (!$_REQUEST["action"] || $_SERVER["REQUEST_METHOD"] == "POST" && !check_bitrix_sessid()) {
                throw new \Exception("Ошибка запроса", static::ERROR_WRONG_BITRIX_SESSID);
            }

            $method .= implode('', array_map(function ($part) {
                return mb_strtoupper(mb_substr($part, 0, 1)) . mb_strtolower(mb_substr($part, 1));
            }, explode('.', $_REQUEST["action"])));

            $result['method'] = $method;
            if (method_exists($this, $method)) {
                $result = call_user_func([$this, $method], $result);
            } else {
                throw new \Exception("Ошибка запроса", static::ERROR_FUNCTION_NOT_FOUND);
            }
        } catch (\Exception $ex) {
            $result['error'] = $ex->getMessage();
            $result['code'] = $ex->getCode();
        }
        \Brainify\Utils::setJSONHeader();
        die(json_encode($result));
    }

    public function getEmail($result) {
        return ['email' => 'love@brainify.ru'];
    }

    public function checkFormSaveOrFail($fields) {
        if (!$fields['FIO']) {
            throw new \Exception('Представься, пожалуйста', static::ERROR_FORM_FIELD_EMPTY);
        }

        if (!$fields['GENDER']) {
            throw new \Exception('Выбери пол', static::ERROR_FORM_FIELD_EMPTY);
        } else if (!in_array($fields['GENDER'], [ 'M', 'F', ])) {
            throw new \Exception('Неверное значение поля пол', static::ERROR_FORM_FIELD_INVALID);
        }

        if (!$fields['STATE']) {
            throw new \Exception('Выбери в каких ты отношениях', static::ERROR_FORM_FIELD_EMPTY);
        } else if (!in_array($fields['STATE'], [
                'free',
                'dating',
                'mariage',
                'complicated',
                'broke_up',
            ])) {
                throw new \Exception('Неверное значение поля отношения', static::ERROR_FORM_FIELD_INVALID);
        }

        return true;
    }
    public function postFormSave($ajax_result)
    {
        $userEmail = $_COOKIE["email"] ? $_COOKIE["email"] : ($_GET["email"] ? $_GET["email"] : '');
        if (!$userEmail) {
            return ['redirect' => "/love/"];
        }
        $fields = [
            'EMAIL' => $_COOKIE["email"],
            'FIO' => filter_var(trim($_POST["fio"]), FILTER_SANITIZE_STRING),
            'GENDER' => trim($_POST["gender"]),
            'STATE' => trim($_POST["state"]),
            'DETAILS' => strip_tags(trim($_POST["details"])),
            'PRICE' => intval($_POST["price"]),
        ];
        $this->checkFormSaveOrFail($fields);

        $result = \Brainify\Lovelify\ChatResultsTable::add($fields);
        if (!$result->isSuccess()) {
            throw new \Exception(implode("\n", $result->getErrorMessages()), static::ERROR_SAVE_ERROR);
        }
        return ['result' => true];
    }
}


(new LovelyLanding)->processAjax();