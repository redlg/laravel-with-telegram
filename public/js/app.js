var Love = window.Love = window.Love || {};

// config toastr
if(!window.toastr) window.toastr = {};
window.toastr.options = {
    closeButton: true,
    debug: false,
    newestOnTop: true,
    progressBar: false,
    positionClass: 'toast-bottom-left',
    preventDuplicates: false,
    onclick: null,
    showDuration: '300',
    hideDuration: '10000',
    timeOut: '2000',
    extendedTimeOut: '1000',
    showEasing: 'swing',
    hideEasing: 'linear',
    showMethod: 'fadeIn',
    hideMethod: 'fadeOut'
};
window.toastr.messages = function(message, title, optionsOverride) {
    return this.notify({
        type: "messages",
        iconClass: 'toast-messages',
        message: message,
        optionsOverride: optionsOverride,
        title: title
    });
};

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

jQuery(function($) {
    $('a.scroll').click(function(e) {
        e.preventDefault();
        var el = $($(this).attr('href'));
        if(el.length) {
            var h = el.offset().top - 50;
            $('html, body').animate({
                scrollTop: h
            }, 300);
        }
    });
});