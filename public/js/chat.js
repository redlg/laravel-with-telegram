$(function () {
    var form;
    
    function doResultActions(data) {
        if (data.error) {
            window.toastr.warning(data.error);
            return true;
        }
        if (data.redirect) {
            window.location.href = data.redirect;
            return true;
        }
        return false;
    }

    function checkPages(pages) {
        var result = true;
        pages.each(function () {
            var page = this;
            if (page.id == 'page_1') {
                if (!form[0].fio.value) {
                    window.toastr.warning('Представься, пожалуйста');
                    result = false;
                } else if (!form[0].gender.value) {
                    window.toastr.warning('Выбери какого ты пола');
                    result = false;
                }
            }
        });
        return result;
    }

    function openPage(chat_item) {
        chat_item.removeClass('chat__item--hidden');
        Love.updateExpanding(chat_item);
        $('window,body').animate({
            scrollTop: chat_item.offset().top
        });
    }

    $.getJSON(
        '/chat/email/',
        function (data) {
            if (!doResultActions(data)) {
                $('#lovely_email')
                    .attr('href', "mailto:" + data.email)
                    .text(data.email);
            }
        }
    );
    $('.js-next').click(function () {
        var chat_item = $(this).closest('.chat__item');
        if (checkPages(chat_item)) {
            openPage(chat_item.next());
        }
    });
    form = $("#chat_form").submit(function (e) {
        e.preventDefault();
        if (checkPages(form.find('.chat__item'))) {
            $.ajax({
                url: form.attr('action'),
                method: 'post',
                data: form.serialize(),
                dataType: 'json',
                success: function (data) {
                    if (!doResultActions(data)) {
                        openPage(form.find('.chat__item').last());
                    }
                }
            });
        }
    });

});