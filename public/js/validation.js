// custom form validation messages
(function() {
    var Love = window.Love = window.Love || {};

    Love.validateForm = function(event) {
        // fetch cross-browser event object and form node
        event = (event ? event : window.event);
        var
            form = (event.target ? event.target : event.srcElement),
            f, field, message_shown = false, formvalid = true;

        // loop all fields
        for (f = 0; f < form.elements.length; f++) {

            // get field
            field = form.elements[f];

            // ignore buttons, fieldsets, etc.
            if (field.nodeName !== "INPUT" && field.nodeName !== "TEXTAREA" && field.nodeName !== "SELECT") continue;

            // is native browser validation available?
            if (typeof field.willValidate !== "undefined") {

                // native validation available
                if (field.nodeName === "INPUT" && field.type !== field.getAttribute("type")) {

                    // input type not supported! Use legacy JavaScript validation
                    field.setCustomValidity(legacyValidation(field) ? "" : "error");

                }

                // native browser check
                field.checkValidity();

            }
            else {

                // native validation not available
                field.validity = field.validity || {};

                // set to result of validation function
                field.validity.valid = legacyValidation(field);

                // if "invalid" events are required, trigger it here

            }

            hideErrorMessages($(field));
            if (field.validity.valid) {
                // remove error styles and messages
                $(field).removeClass('check-error')
                    .addClass('check-valid')
                    .trigger('field-valid');

            }
            else {
                // style field, show error, etc.
                $(field).addClass('check-error')
                    .removeClass('check-valid')
                    .trigger('field-invalid');
                if (!message_shown) {
                    var message = $(field).data('error-text') || "Сначала заполни это поле :-)",
                        $element;
                    if ($(field).data('error-element')) {
                        $element = $($(field).data('error-element'));
                    }
                    if (!$element || !$element.length) {
                        $element = $(field);
                    }
                    // show error popover
                    showErrorMessage($element, message);
                    message_shown = true;
                }

                // form is invalid
                formvalid = false;
            }

        }

        // cancel form submit if validation fails
        if (!formvalid) {
            if (event.preventDefault) event.preventDefault();
        }
        return formvalid;
    };

    function hideErrorMessages($fields) {
        $fields.each(function () {
            var $field = $(this);
            if ($field.not(':visible')) {
                $field = $field.closest(':visible');
            }
            var timer = $field.data('$error_message_box_timer'),
                $message_box = $field.data('$error_message_box');
            if (timer) {
                clearTimeout(timer);
                $field.removeData('$error_message_box_timer');
            }

            if ($message_box && $message_box.length) {
                $message_box.fadeOut('fast', function () {
                    $message_box.remove();
                });
                $field.removeData('$error_message_box');
            }
        });
    }

    function showErrorMessage($field, message) {
        if ($field.not(':visible')) {
            $field = $field.closest(':visible');
        }
        var fieldOffset = $field.offset();
        var fieldSizes = {
            width: $field.width(),
            height: $field.outerHeight()
        };
        var $message_box = $('<div class="error-message"></div>').html(message).css({
            position: 'absolute',
        }).appendTo($(document.body));
        $message_box.css({
            top: fieldOffset.top + fieldSizes.height + 12,
            left: fieldOffset.left + Math.floor(fieldSizes.width / 2) - ($message_box.width() / 2),
        }).hide().fadeIn('fast');

        hideErrorMessages($field);

        $field.data('$error_message_box', $message_box);
        $field.data('$error_message_box_timer', setTimeout(hideErrorMessages.bind(undefined, $field), 5000));
    }

// basic legacy validation checking
    function legacyValidation(field) {

        var
            valid = true,
            val = field.value,
            type = field.getAttribute("type"),
            chkbox = (type === "checkbox" || type === "radio"),
            required = field.getAttribute("required"),
            minlength = field.getAttribute("minlength"),
            maxlength = field.getAttribute("maxlength"),
            pattern = field.getAttribute("pattern");

        // disabled fields should not be validated
        if (field.disabled) return valid;

        // value required?
        valid = valid && (!required ||
                (chkbox && field.checked) ||
                (!chkbox && val !== "")
            );

        // minlength or maxlength set?
        valid = valid && (chkbox || (
                (!minlength || val.length >= minlength) &&
                (!maxlength || val.length <= maxlength)
            ));

        // test pattern
        if (valid && pattern) {
            pattern = new RegExp(pattern);
            valid = pattern.test(val);
        }

        return valid;
    }
})();