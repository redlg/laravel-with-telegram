// ImageLoader. Preload images in background with callback on finish
(function() {
    var Love = window.Love = window.Love || {};

    function ImageLoader(images, cb) {
        this.images = [];
        this.loadingImages = [];
        this.callbacks = [];
        this.loadedImages = 0;

        if(images) {
            if(BX.util.isString(images)) {
                images = [images];
            }

            this.images = images;
            if(cb)
                this.callbacks.push(cb);
            this.start();
        }
    }
    ImageLoader.prototype.start = function() {
        var self = this;
        setTimeout(function() {
            self._start();
        }, 1);
    };
    ImageLoader.prototype._start = function() {
        var self = this;
        for(var i in this.images) {
            if(!this.loadingImages[i]) {
                this.loadingImages[i] = true;
                var img = new Image();
                img.src = this.images[i];
                img.onload = function () {
                    self.handleImageLoaded();
                };
            }
        }
    };
    ImageLoader.prototype.handleImageLoaded = function() {
        this.loadedImages++;
        if(this.loadedImages == this.images.length) {
            // call all cb only once
            for(var i in this.callbacks) {
                this.callbacks[i]();
            }
            this.callbacks = [];
        }
    };
    ImageLoader.prototype.addImage = function(image, cb) {
        this.images.push(image);
        if(cb) this.callbacks.push(cb);
        this.start();
    };

    Love.preloadImages = function(images, cb) {
        return new ImageLoader(images, cb);
    };
})();