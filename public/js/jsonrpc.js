(function(){
    var Love = window.Love = window.Love || {};

    /* OPTIONS:
     * class: класс,
     * listeners (optional): {
     *   success,
     *   complete,
     *   error
     * },
     * url (optional)
     * */
    window.JSONRPC = function(options) {
        this.cl = "";
        this.listeners = {
            success: undefined,
            error: undefined,
            complete: undefined
        };
        $.extend(this, options);
        this.call_id = 1;
    };

    JSONRPC.prototype.url = "/api/ajax.php";

    JSONRPC.prototype.abort = function(){
        if(this.last_xhr && this.last_xhr.__abortable)
            this.last_xhr.abort();
        return this;
    };
    JSONRPC.prototype.send = function(method, arguments, listeners, callback_data, abortable){
        var self = this,
            funcs,
            callParams = {
                jsonrpc: "2.0",
                method: "",
                params: arguments || {},
                id: this.call_id++,
                callback_data: callback_data,
                abortable: true
            };

        if(typeof abortable == "undefined"){
            abortable = true;
        }
        if(typeof method == "object"){
            if(this.cl)
                method.method = this.cl + "." + method.method;
            funcs = $.extend({}, this.listeners, method.listeners);
            delete method.listeners;
            $.extend(callParams, method);
        }else{
            if(this.cl)
                callParams.method = this.cl + ".";
            callParams.method += method;
            callParams.abortable = abortable;

            funcs = $.extend({}, this.listeners, listeners);
        }
        callParams.params["bitrix_sessid"] = BX.bitrix_sessid();
        callParams = JSON.stringify(callParams);
        var url = self.url;
        // remove F**KING #
        if(url.indexOf('#') >= 0)
            url = url.substr(0, url.indexOf('#'));
        url += (url.indexOf('?') < 0 ? '?' : '&') + 'jsonrpc=Y';
        this.last_xhr = $.ajax({
            type: 'POST',
            url: url,
            dataType: "json",
            data: callParams,
            success: function(data){
                //if(data.bitrix_sessid)
                //    phpVars.bitrix_sessid = data.bitrix_sessid;
                if (data.error){
                    if(funcs.error)
                        funcs.error(data.error, callback_data);
                }else{
                    if(funcs.success)
                        funcs.success(data.result, callback_data);
                }
            },
            error: function(data, status, error) {
                if(status !== 'abort') {
                    if (funcs.error) {
                        funcs.error(error, callback_data);
                    }
                }
            },
            complete: function(xhr, status){
                if (funcs.complete){
                    funcs.complete(xhr, status, callback_data);
                }
            }
        });
        this.last_xhr.__abortable = abortable;
    };
})();