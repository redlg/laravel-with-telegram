<?php
ob_start();
$send_errors = true;
echo "\n\n";
echo date("c")."\n";
try {
$data = [];
if (php_sapi_name() == "cli") {
    $data = array(
        "repository" => array(
            "name" => "lovelify"
        ),
    );
    
} else {
    $payload = file_get_contents('php://input');
    if ($payload && $_GET["key"] == "ar1rL1Sz") {
        $data = json_decode($payload, true);
    }
}
if($data && $data["repository"]["name"] === "lovelify") {
    // get current branch and it's remote tracking branch
    exec("git branch -vv|grep \"*\" 2>&1", $output, $result);
    $line = $output[0];
    $errorSubject = 'Lovelify merge error';

    if (preg_match("/\\[origin\\/[^\\]]*\\]/", $line)) {
        // if current branch tracks remote and "git pull" will work

        // echo shell_exec("/usr/bin/git pull --ff-only 2>&1");
	$output = [];
        exec("git pull --ff-only 2>&1", $output, $result);
        echo implode("\n", $output);
        echo "\npull result:$result\n";
        if ($result != 0) {
            if ($send_errors) {
                // var_dump($result);
                mail('putalexey@redlg.ru', $errorSubject, implode("\n", $output), "From: putalexey@redlg.ru");

                // send error to slack
                $data = array(
                    "text" => $errorSubject . "\n" . implode("\n", $output),
                    "icon_emoji" => ":ghost:",
                );

                $hookUrl = 'https://hooks.slack.com/services/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';
                $curl = curl_init();
                curl_setopt($curl, CURLOPT_URL, $hookUrl);
                curl_setopt($curl, CURLOPT_POST, true);
                curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data));
                $out = curl_exec($curl);
                curl_close($curl);
            }
        } else {
            $output = [];
            exec("git submodule foreach git pull 2>&1", $output, $result);
            exec("./console/migrate.php migrate", $output, $result);
            if ($result != 0)
                mail('putalexey@redlg.ru', $errorSubject, implode("\n", $output). "\nresult code: $result", "From: putalexey@redlg.ru");
            echo implode("\n", $output);
            echo "\nmigrate result:$result\n";

            echo "Updating composer dependencies\n";
            $output = [];
            exec("env HOME=/home/bitrix/ php composer install --no-progress -n 2>&1", $output, $result);
            chdir($olddir);
            echo implode("\n", $output);
            echo "\nComposer dependencies updated\n";
        }
    } else {
        echo "current branch doesn't track remote: do nothing\n";
    }
} else {
    die();
}
} catch (\Exception $ex) {
	echo 'ERROR: ' . $ex->getMessage();
}
//echo date("c")."\n";
file_put_contents(__DIR__ . "/../storage/logs/pull.update.log", ob_get_clean(), FILE_APPEND);
echo 'OK';

