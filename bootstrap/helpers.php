<?php


if (!function_exists('menu_link')) {
    function menu_link($link, $title, $attributes = []) {
        $link_attrs = \Illuminate\Support\Arr::pull($attributes, '_link', []);
        if (\Illuminate\Support\Str::startsWith(request()->url(), $link)) {
            $attributes['class'] = (!empty($attributes['class']) ? ' ' : '') . 'active';
        }
        return Html::tag('li', (string)Html::link($link, $title, $link_attrs), $attributes);
    }
}
