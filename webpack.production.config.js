const path = require('path');
const webpack = require('webpack');
const srcPath = path.resolve(__dirname, 'resources/assets/js/react');
const buildPath = path.resolve(__dirname, 'public');
const nodeModulesPath = path.resolve(__dirname, 'node_modules');
const ExtractTextPlugin = require("extract-text-webpack-plugin");


var config = {
    entry: [
        './resources/assets/js/react/app.jsx',
        './resources/assets/scss/app.scss',
    ],
    devtool: 'inline-source-map',
    output: {
        path: buildPath,
        filename: 'assets/app.min.js',
    },
    module: {
        rules: [
            {
                test: /\.jsx?$/,
                // include: [
                //     path.resolve(__dirname, 'node_modules'),
                //     path.resolve(__dirname, "resources/assets/js/react")
                // ],
                exclude: [nodeModulesPath],

                loader: "babel-loader",
                // the loader which should be applied, it'll be resolved relative to the context
                // -loader suffix is no longer optional in webpack2 for clarity reasons
                // see webpack 1 upgrade guide

                options: {
                    presets: [
                        "react",
                        ["es2015", {"modules": false}],
                        "stage-1"
                    ],
                    plugins: [
                        [
                            "transform-decorators-legacy",
                            "transform-replace-object-assign",
                            // "transform-object-assign",
                            "simple-assign"
                        ],
                        [
                            "babel-plugin-root-import",
                            [{
                                "rootPathPrefix": "@",
                                "rootPathSuffix": "resources/assets/js/react"
                            }, {
                                "rootPathPrefix": "#",
                                "rootPathSuffix": "resources/assets/js/react/stores"
                            }]
                        ]
                    ]
                }
                // options for the loader
            }, {
                test: /\.scss$/,
                use: ExtractTextPlugin.extract({
                    fallback: 'style-loader',
                    use: 'css-raw-loader!postcss-loader!sass-loader?sourceMap'
                })
            }
        ]
    },
    plugins: [
        new webpack.DefinePlugin({
            "process.env": {
                'NODE_ENV': JSON.stringify('production')
            }
        }),
        new webpack.optimize.UglifyJsPlugin({
            comments: false,
            beautify: false,
            mangle: {
                except: ['$super', '$', 'exports', 'require']
            }
        }),
        new webpack.LoaderOptionsPlugin({
            test: /\.scss$/,
            minimize: true,
            comments: false,
            options: {
                postcss: [
                    require('precss'),
                    require('autoprefixer')
                ]
            }
        }),
        new ExtractTextPlugin({
            filename: './assets/app.min.css',
            allChunks: true
        })
    ],
    resolve: {
        extensions: [".jsx", ".js", ".json", "*"],
        modules: [
            srcPath,
            'node_modules'
        ]
    }
};

module.exports = config;